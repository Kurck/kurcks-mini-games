package darkPackage;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;
import com.jme3.ui.Picture;

/**
 * @author Kurck Gustavson
 */
public class ScoreBoard extends Node {

	private BitmapFont guiFont;
	private BitmapText deathText;
	private BitmapText totalText;
	private BitmapText pathText;
	private BitmapText endText;
	private BitmapText hiddenText;
	private int noPoints = 0;
	private int noPathPoints = 0;
	private int noEndPoints = 0;
	private int deaths = 0;
	private int totalFound = 0;
	private int pathFound = 0;
	private int endPointsFound = 0;
	private int hiddenFound = 0;

	public ScoreBoard(AssetManager assetManager, int total, int paths, int ends) {
		noPoints = total;
		noPathPoints = paths;
		noEndPoints = ends;

		// Create the score board background
		Quad q = new Quad(85f, 110f);
		Geometry g = new Geometry("quad", q);
		g.setLocalTranslation(10f, 10f, 0f);
		Material transparentBlack = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		transparentBlack.setColor("Color", new ColorRGBA(0f, 0f, 0f, 0.2f));
		transparentBlack.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		g.setMaterial(transparentBlack);
		attachChild(g);

		// Show the icons and texts
		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		int ix = 15, tx = 40, y = -5, h = 20;
		hiddenText = createText(tx, y += h, "");
		endText = createText(tx, y += h, "0 / " + noEndPoints);
		createIcon(ix, y + 1, "End points", "Interface/endIcon.png", assetManager);
		pathText = createText(tx, y += h, "0 / " + noPathPoints);
		createIcon(ix, y + 1, "Path points", "Interface/pathIcon.png", assetManager);
		totalText = createText(tx, y += h, "0 / " + noPoints);
		createIcon(ix, y + 1, "All points", "Interface/allIcon.png", assetManager);
		deathText = createText(tx, y += h, "0");
		createIcon(ix, y + 1, "Deaths", "Interface/deathIcon.png", assetManager);
	}

	private BitmapText createText(int x, int y, String value) {
		BitmapText text = new BitmapText(guiFont, false);
		text.setSize(guiFont.getCharSet().getRenderedSize());
		text.setText(value);
		text.setLocalTranslation(x, (float) y + text.getLineHeight(), 0.0F);
		attachChild(text);
		return text;
	}

	private void createIcon(int x, int y, String name, String path, AssetManager assetManager) {
		Picture icon = new Picture(name);
		icon.setImage(assetManager, path, true);
		icon.setWidth(18);
		icon.setHeight(18);
		icon.setPosition(x, y);
		attachChild(icon);
	}

	// Yes, very repetitive, but not enough time to clean it up :)
	public void hiddenFound() {
		hiddenFound++;
		hiddenText.setText(String.valueOf(hiddenFound));
		addToTotal();
	}

	public void endFound() {
		endPointsFound++;
		endText.setText(endPointsFound + " / " + noEndPoints);
		addToTotal();
	}

	public void pathFound() {
		pathFound++;
		pathText.setText(pathFound + " / " + noPathPoints);
		addToTotal();
	}

	public void addToTotal() {
		totalFound++;
		totalText.setText(totalFound + " / " + noPoints);
	}

	public void thatSuckerDiedAgain() {
		deaths++;
		deathText.setText(String.valueOf(deaths));
	}

	public void reset() {
		// Ugly hack, but no time :)
		hiddenFound = -1;
		endPointsFound = -1;
		pathFound = -1;
		totalFound = -3;

		hiddenFound();
		endFound();
		pathFound();
	}
}
