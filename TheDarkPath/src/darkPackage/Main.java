package darkPackage;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FadeFilter;
import com.jme3.post.filters.FogFilter;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.system.AppSettings;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.geomipmap.lodcalc.DistanceLodCalculator;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;
import com.jme3.ui.Picture;
import java.util.Base64;

public class Main extends SimpleApplication implements ActionListener {

	private enum GamePhase {
		Intro, Game, Dead, EndScene, Outro
	}

	private static final float DEATH_DEPTH = 100f;
	private static final String[] END_SCENE_PHRASES = new String[]{"V2h5IGFtIEkgaGVyZT8=", "V2hvIGFtIEk/", "V2hhdCBkbyBJIHdhbnQ/", "V2lsbCBhbnlvbmUgcGxheSBteSBnYW1lcz8=", "V2lsbCBhbnlvbmUgd2F0Y2ggbXkgdmlkZW9zPw==", "QW0gSSBwdXR0aW5nIGluIGVmZm9ydCBmb3Igbm90aGluZz8=", "V2h5IGRvIG90aGVycyBrZWVwIGFza2luZyB3aGV0aGVyIEkgZm91bmQgc29tZW9uZT8=", "V2lsbCBJIGV2ZXIgZmluZCByZXN0Pw==", "V2h5IGRvIHBlb3BsZSBiZWxpZXZlIHlvdXIgc291bCBhbmQgYnJhaW5zIGFyZSBzZXBhcmF0ZT8=", "V2h5IGRvIHBlb3BsZSBwdXQgc28gbXVjaCBtb25leSBpbiBraWxsaW5nIHBlb3BsZT8=", "RGlkIEkgbWFrZSB0aGUgcmlnaHQgY2hvaWNlcz8=", "V2h5IGRvIHBlb3BsZSBsaWtlIHRyYXZlbGxpbmcgdGhhdCBtdWNoPw==", "V2h5IHdvdWxkIEkgcmF0aGVyIHZpc2l0IHBsYWNlcyBvdXRzaWRlIEVhcnRoPw==", "V2lsbCBJIGZvcmdpdmUgbXlzZWxmIGluIDUwIHllYXJzIHRoYXQgSSBkaWQgbm90IGNoYXNlIGdpcmxzIGFzIG11Y2ggYXMgb3RoZXIgZ3V5cyBkaWQ/", "V2lsbCBJIGV2ZXIgZmluaXNoIHRoZSBnYW1lcyBJIHN0YXJ0ZWQgYnVpbGRpbmcgYmFjayBpbiAyMDA3Pw==", "V2lsbCBJIGZpbmQgb3V0IHdoYXQgSSByZWFsbHkgd2FudD8=", "RG8gSSB3YW50IHRvIGRvIG5vdGhpbmcgYnV0IGJ1aWxkaW5nIGFuZCBwbGF5aW5nIGdhbWVzIG9jY2FzaW9uYWxseT8=", "QW0gSSB0aGUgb25seSBvbmUgc2Nyb2xsaW5nIHRocm91Z2ggb3RoZXIgcGVvcGxl4oCZcyBGYWNlYm9vayBwYWdlcz8=", "SG93IG1hbnkgcGVvcGxlIHZpc2l0IG15IHByb2ZpbGU/", "RG8gdGhlIGdpcmxzIHRoYXQgSSBsaWtlLCBsaWtlIG1lPw==", "V2lsbCBJIGV2ZXIgZmVlbCBsb3ZlIGFnYWluPw==", "QXJlIHRoZSBtZWRpYSBvYmplY3RpdmU/", "RG9lcyBpdCBtYXR0ZXI/", "QW0gSSBhbm5veWluZyBteSBuZWlnaGJvdXJzPw==", "SG93IG1hbnkgeWVhcnMgd2lsbCBpdCB0YWtlIHRvIHNldHRsZT8=", "SG93IHdpbGwgSSByZWFjdCB3aGVuIG15IHBhcmVudHMgZGllPw==", "SSBhbSB3b3JyaWVkIGFib3V0IHBvbGFyaXNhdGlvbi4=", "V2hlbiB3aWxsIHRoZSBFYXJ0aCBiZSB0aHJlYXRlbmVkIGJ5IGEgY2l2aWxpc2F0aW9uIGtpbGxpbmcgcm9jaz8=", "V2lsbCBodW1hbml0eSBkZXN0cm95IGl0c2VsZiBiZWZvcmUganVtcGluZyB0byB0aGUgbmV4dCBzdGVwcGluZyBzdG9uZT8=", "V2lsbCBJIGV2ZXIgdmlzaXQgTWFycyBvciBUaXRhbj8=", "V2h5IGRvIHNvIG1hbnkgYmVsaWV2ZSBpbiBhIGdvZCBvciDigJhzb21ldGhpbmfigJk/", "SG93IHdvdWxkIG91ciB3b3JsZCBsb29rIGxpa2UgaWYgaHVtYW5zIHdlcmUgYWxsIGFsdHJ1aXN0aWM/", "V291bGQgaHVtYW5zIHN0aWxsIGJlIGFsaXZlPw==", "V2lsbCB3ZSBydW4gb3V0IG9mIG1lZGljYXRpb24/", "V2lsbCB0aGUgd29ybGQgcmVhbGx5IGNoYW5nZSBhbmQgc3RvcCB1c2luZyBwb2xsdXRpbmcgcmVzb3VyY2VzPw==", "V2hhdCBkb2VzIGVhdGluZyDigJhoZWFsdGh54oCZIHJlYWxseSBtZWFuPw==", "V2h5IGRvIHBlb3BsZSBjbGluZyB0byBoYWJpdHM/", "V2h5IGlzIGNoYW5nZSBzY2FyeT8=", "QXJlIHRoZSBBbWVyaWNhbiBwcmVzaWRlbnRpYWwgZWxlY3Rpb25zIHJlYWxseSB0aGF0IGltcG9ydGFudCB0aGF0IGl0IHNob3VsZCBiZSBjb3ZlcmVkIGRhaWx5IGZvciBtb250aHMgb24gRHV0Y2ggdGVsZXZpc2lvbj8=", "V2h5IGRvIHRoZSBtZWRpYSBnaXZlIHRoZSBpbXByZXNzaW9uIHRoYXQgbWFueSBwZW9wbGUgbGlrZSB0aGUgVVM/", "QXJlIHdlIGFsb25lPw==", "V2h5IGRvIEkgY2FyZSBzbyBtdWNoIGFib3V0IG90aGVyIHBlb3BsZeKAmXMgb3BpbmlvbnM/", "V2lsbCBJIGV2ZXIgZmluZCBhIGdpcmxmcmllbmQgYWdhaW4/", "RG8gSSBuZWVkIGEgZ2lybGZyaWVuZD8=", "SSBjb3VsZCBoYXZlIHNlbnQgaGVyIGEgdmFsZW50aW5l4oCZcyBjYXJkLg==", "SSBjb3VsZCBoYXZlIHdyaXR0ZW4gaGVyIGEgc3dlZXQgcG9lbS4=", "SSBjb3VsZCBoYXZlIHRvbGQgaGVyIGFib3V0IGhlciBiZWF1dGlmdWwgZXllcy4=", "SSBjb3VsZCBoYXZlIHN0YWxrZWQgaGVyIGF0IHdvcmsu", "SSBzaG91bGRu4oCZdCBoYXZlIHN0YWxrZWQgaGVyLg==", "SSBzaG91bGRu4oCZdCBoYXZlIGJlZW4gcHVzaGluZy4=", "V2UgY291bGQgaGF2ZSBiZWVuIGEgbmljZSBraXRjaGVuIHdyZWNraW5nIGNvdXBsZS4="};
	private GamePhase gamePhase = GamePhase.Intro;
	private TerrainQuad terrain;
	private Material terrainTexturing;
	private BulletAppState bulletAppState;
	private CharacterControl player;
	private Vector3f walkDirection = new Vector3f();
	private boolean left = false, right = false, up = false, down = false;
	private TourGuide guide;
	private AudioNode deathSound;
	private AudioNode backgroundNoises;
	private AudioNode walkingSound;
	private int endSceneCounter = 0;
	private int endSceneRounds = 0;
	private float endSceneTimer = 0f;
	private float endSceneTimerMax = 1f;
	private float introTimer = 3f;
	private FadeFilter fade;
	private boolean fadeRunning = false;
	private Picture theDarkPath;

	public static void main(String[] args) {
		Main app = new Main();
		AppSettings cfg = new AppSettings(true);
		cfg.setTitle("The Dark Path");
		cfg.setSettingsDialogImage("Interface/theDarkPath.jpg");
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		cfg.setFrameRate(60);
		cfg.setVSync(true);
		cfg.setFrequency(60);
		cfg.setResolution(1280, 960);
		cfg.setFullscreen(false);
		cfg.setSamples(2);
		app.setSettings(cfg);
		app.setShowSettings(true);
		app.start();
	}
	private Picture deathBanner;

	@Override
	public void simpleInitApp() {
		// Set-up physics and terrain.
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		loadTerrain();

		// Add the player and make it collide with the terrain.
		terrain.addControl(new RigidBodyControl(0));
		CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 6f, 1);
		// TODO: next game with BetterCharacterControl
		player = new CharacterControl(capsuleShape, 0.05f);
		player.setJumpSpeed(10);
		player.setFallSpeed(70);
		player.setGravity(30);
		player.setMaxSlope(0.1f);
		player.setPhysicsLocation(new Vector3f(0, 142, -100));
		setUpKeys();
		bulletAppState.getPhysicsSpace().add(terrain);
		bulletAppState.getPhysicsSpace().add(player);

		// Add the guide that will tell interesting facts.
		guide = new TourGuide(terrain, assetManager, guiNode);
		rootNode.attachChild(guide);

		// Load sounds and 'music'
		String soundLocation = String.format("Sounds/ploep%d.ogg", FastMath.nextRandomInt(0, 5));
		deathSound = new AudioNode(assetManager, soundLocation, false);
		deathSound.setLooping(false);
		deathSound.setDirectional(false);
		rootNode.attachChild(deathSound);
		backgroundNoises = new AudioNode(assetManager, "Sounds/ambient.ogg", false);
		backgroundNoises.setLooping(true);
		backgroundNoises.setDirectional(false);
		backgroundNoises.setPositional(false);
		rootNode.attachChild(backgroundNoises);
		backgroundNoises.play();
		walkingSound = new AudioNode(assetManager, "Sounds/footsteps.ogg", false);
		walkingSound.setLooping(true);
		walkingSound.setPositional(false);
		walkingSound.setDirectional(false);
		walkingSound.setVolume(0.5f);
		rootNode.attachChild(walkingSound);

		// Add some fog
		FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
		FogFilter fog = new FogFilter();
		fog.setFogColor(ColorRGBA.Black);
		fog.setFogDistance(180F);
		fog.setFogDensity(1.5F);
		fpp.addFilter(fog);

		// Add fade-in
		fade = new FadeFilter(4);
		fade.setValue(0);
		fpp.addFilter(fade);
		viewPort.addProcessor(fpp);

		// Load used overlay images.
		deathBanner = loadBanner("Loose", "Interface/YouDied.png");
		theDarkPath = loadLogo();
		guiNode.attachChild(theDarkPath);
	}

	private void setUpKeys() {
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
		inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
		inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
		inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
		inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_R));
		inputManager.addMapping("Quit", new KeyTrigger(KeyInput.KEY_Q));
		inputManager.addListener(this, "Left", "Right", "Up", "Down", "Jump", "Reset", "Quit");
	}

	@Override
	public void onAction(String binding, boolean value, float tpf) {
		switch (binding) {
			case "Left":
				left = value;
				break;
			case "Right":
				right = value;
				break;
			case "Up":
				up = value;
				break;
			case "Down":
				down = value;
				break;
			case "Jump":
				player.jump();
				break;
			case "Reset":
				if (value) {
					// No longer reset scores :)
					//guide.reset();
					player.setPhysicsLocation(new Vector3f(0, 150, -100));
					guiNode.detachChild(deathBanner);
					gamePhase = GamePhase.Game;
				}
				break;
			case "Quit":
				if (gamePhase == GamePhase.Dead) {
					stopTheGame();
				}
				break;
		}
	}

	@Override
	public void simpleUpdate(float tpf) {
		switch (gamePhase) {
			case Intro:
				if (introTimer > 0f) {
					introTimer -= tpf;
				} else {
					theDarkPath.removeFromParent();
					fade.fadeIn();
					gamePhase = GamePhase.Game;
				}
				break;
			case Game:
				playGame();
				break;
			case EndScene:
				endSceneTimer += tpf;
				if (endSceneTimer > endSceneTimerMax) {
					endSceneTimer = 0f;
					endSceneTimerMax /= 2;
					endScene();
				}
				if (endSceneRounds == 10 && !fadeRunning) {
					guide.hideScoreBoard();
					fadeRunning = true;
					fade.fadeOut();
					guiNode.attachChild(theDarkPath);
				}
				break;
			case Outro:
				// Hacking all the way
				introTimer -= tpf;
				if (introTimer <= 0f) {
					BitmapText text = new BitmapText(guiFont, false);
					text.setColor(ColorRGBA.White);
					text.setSize(guiFont.getCharSet().getRenderedSize());
					text.setText("A game by Kurck Gustavson, 2016");
					text.setLocalTranslation((settings.getWidth() - text.getLineWidth()) / 2, (settings.getHeight() - 330) / 2, 1f);
					guiNode.attachChild(text);
					introTimer = 4f;
				} else if (introTimer < 1f) {
					stopTheGame();
				}
				break;
			default:
				break;
		}
	}

	private void stopTheGame() {
		fade.setEnabled(false);
		backgroundNoises.stop();
		stop();
	}

	private Picture loadBanner(String name, String path) {
		Picture banner = new Picture(name);
		banner.setImage(assetManager, path, true);
		banner.setWidth(547);
		banner.setHeight(135);
		banner.setPosition((settings.getWidth() - 547) / 2, settings.getHeight() / 6);
		return banner;
	}

	private Picture loadLogo() {
		Picture logo = new Picture("Logo");
		logo.setImage(assetManager, "Interface/theDarkPath.jpg", false);
		logo.setWidth(640);
		logo.setHeight(300);
		logo.setPosition((settings.getWidth() - 640) / 2, (settings.getHeight() - 300) / 2);
		return logo;
	}

	/**
	 * Loads the heightmap of the terrain and applies textures to it.
	 */
	private void loadTerrain() {
		// Terrain texture loading
		terrainTexturing = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");
		terrainTexturing.setBoolean("useTriPlanarMapping", false);
		//matRock.setReceivesShadows(true);
		terrainTexturing.setTexture("Alpha", assetManager.loadTexture("Textures/maptexture.png"));
		loadTexture("Tex1", "Textures/rocky.jpg", 128);
		loadTexture("Tex2", "Textures/hotRock.jpg", 128);
		loadTexture("Tex3", "Textures/pool.jpg", 64);

		// Terrain heightmap
		Texture heightMapImage = assetManager.loadTexture("Scenes/ThePathMap.png");
		AbstractHeightMap heightmap = new ImageBasedHeightMap(heightMapImage.getImage(), 1f);
		heightmap.load();
		terrain = new TerrainQuad("terrain", 65, 1025, heightmap.getHeightMap());
		TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
		control.setLodCalculator(new DistanceLodCalculator(65, 2.7f));
		terrain.addControl(control);
		terrain.setMaterial(terrainTexturing);
		terrain.setLocalScale(2f, 1f, 2f);
		rootNode.attachChild(terrain);

		// Load hidden terrain map
		Quad q = new Quad(8, 8);
		Geometry g = new Geometry("TerrainMap", q);
		Vector2f mapLocation = new Vector2f(-68, -688);
		g.setLocalTranslation(mapLocation.x, terrain.getHeight(mapLocation), mapLocation.y);
		Material terrainMapMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		terrainMapMat.setTexture("ColorMap", heightMapImage);
		g.setMaterial(terrainMapMat);
		rootNode.attachChild(g);
	}

	/**
	 * Applies a texture to the terrain.
	 *
	 * @param textureId the id of the texture (Tex1, Tex2, etc.)
	 * @param texturePath the path to the asset to use as texture.
	 * @param scale the scale factor of the texture.
	 */
	private void loadTexture(String textureId, String texturePath, float scale) {
		Texture texture = assetManager.loadTexture(texturePath);
		texture.setWrap(Texture.WrapMode.Repeat);
		terrainTexturing.setTexture(textureId, texture);
		terrainTexturing.setFloat(textureId + "Scale", scale);
	}

	private void playGame() {
		boolean walking = left || right || up || down;
		AudioSource.Status audioStatus = walkingSound.getStatus();
		if (walking && (audioStatus == AudioSource.Status.Paused || audioStatus == AudioSource.Status.Stopped)) {
			walkingSound.play();
		} else if (!walking && audioStatus == AudioSource.Status.Playing) {
			walkingSound.pause();
		}

		Vector3f camDir = cam.getDirection().clone().multLocal(0.6f, 0f, 0.6f);
		Vector3f camLeft = cam.getLeft().clone().multLocal(0.4f);
		walkDirection.set(0, 0, 0);
		if (left) {
			walkDirection.addLocal(camLeft);
		}
		if (right) {
			walkDirection.addLocal(camLeft.negate());
		}
		if (up) {
			walkDirection.addLocal(camDir);
		}
		if (down) {
			walkDirection.addLocal(camDir.negate());
		}
		player.setWalkDirection(walkDirection);
		cam.setLocation(player.getPhysicsLocation());
		float verticalTranslation = player.getPhysicsLocation().getY();
		if (verticalTranslation < DEATH_DEPTH) {
			gamePhase = GamePhase.Dead;
			deathSound.playInstance();
			guiNode.attachChild(deathBanner);
			guide.itDied();
		} else {
			boolean reachedTheEnd = guide.update(player.getPhysicsLocation(), cam.getDirection());
			if (reachedTheEnd) {
				gamePhase = GamePhase.EndScene;
			}
		}
	}

	private void endScene() {
		if (endSceneCounter < END_SCENE_PHRASES.length) {
			String phrase = END_SCENE_PHRASES[endSceneCounter];
			BitmapText text = new BitmapText(guiFont, false);
			text.setColor(ColorRGBA.Black);
			text.setSize(FastMath.nextRandomFloat() * 30f);
			text.setLocalTranslation(FastMath.nextRandomFloat() * settings.getWidth() - 50f, FastMath.nextRandomFloat() * settings.getHeight() - 80f, -1f);
			byte[] decodedMessage = Base64.getDecoder().decode(phrase);
			text.setText(new String(decodedMessage));
			text.setLocalRotation(new Quaternion().fromAngleAxis(FastMath.nextRandomFloat() * 2, new Vector3f(0,0,1)));
			guiNode.attachChild(text);
			endSceneCounter++;
		} else if (endSceneRounds < 12) {
			endSceneCounter = 0;
			endSceneRounds++;
		} else {
			gamePhase = GamePhase.Outro;
		}
	}
}
