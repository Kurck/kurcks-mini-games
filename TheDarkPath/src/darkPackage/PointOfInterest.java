/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkPackage;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.util.Base64;

/**
 *
 * @author gustav
 */
public class PointOfInterest extends Node {

	public enum Type {
		End, Path, Hidden, FinalEnd
	}

	private static final boolean SHOW_SPHERES = false;
	private Type type;
	private BitmapText text;
	private Material mat;
	private boolean reached;
	private AudioNode activationSound;

	public PointOfInterest(Type pointType, int mapX, int mapY, TerrainQuad heightMap, String message, AssetManager assetManager) {
		type = pointType;

		// Define location.
		Vector2f mapped = new Vector2f((mapX - 512) * 2, (mapY - 512) * 2);
		float newY = heightMap.getHeight(mapped) + 10;
		Vector3f newLocation = new Vector3f(mapped.x, newY, mapped.y);
		setLocalTranslation(newLocation);

		// If enabled, a sphere will be rendered.
		if (SHOW_SPHERES) {
			Sphere s = new Sphere(8, 8, 30f);
			Geometry geom = new Geometry("Sphere", s);
			mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
			mat.getAdditionalRenderState().setWireframe(true);
			mat.setColor("Color", ColorRGBA.Black);
			geom.setMaterial(mat);
			attachChild(geom);
		}

		// Load displayed text
        BitmapFont fnt = assetManager.loadFont("Interface/Fonts/Default.fnt");
        text = new BitmapText(fnt, false);
        text.setBox(new Rectangle(-8, -5, 16, 5));
        text.setQueueBucket(RenderQueue.Bucket.Transparent);
        text.setSize(0.6f);
		byte[] decodedMessage = Base64.getDecoder().decode(message);
		text.setText(new String(decodedMessage));

		// Attach a sound node
		String soundLocation = String.format("Sounds/ploep%d.ogg", FastMath.nextRandomInt(0, 5));
		activationSound = new AudioNode(assetManager, soundLocation, false);
		activationSound.setLooping(false);
	}

	public Type getType() {
		return type;
	}

	public boolean isReached(Vector3f playerLocation) {
		if (reached) {
			// Billboarding
			text.lookAt(playerLocation, new Vector3f(0, 1, 0));
			return false;
		}

		if (getLocalTranslation().distanceSquared(playerLocation) > 700f) {
			return false;
		}

		// Newly reached!
		if (SHOW_SPHERES) {
			mat.setColor("Color", ColorRGBA.Green);
		}
        attachChild(text);
		reached = true;
		attachChild(activationSound);
		activationSound.play();
		return true;
	}

	public void reset() {
		reached = false;
		if (SHOW_SPHERES) {
			mat.setColor("Color", ColorRGBA.Black);
		}
		detachChild(activationSound);
	}
	
}
