package darkPackage;

import com.jme3.asset.AssetManager;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.util.ArrayList;
import java.util.List;
import darkPackage.PointOfInterest.Type;
import static darkPackage.PointOfInterest.Type.FinalEnd;
import static darkPackage.PointOfInterest.Type.Path;

/**
 * Manages all interesting locations and notifies the player about events.
 *
 * @author Kurck Gustavson
 */
public class TourGuide extends Node {

	private int noPoints = 0;
	private int noPathPoints = 0;
	private int noEndPoints = 0;
	private ScoreBoard scores;

	private List<PointOfInterest> interestingPoints = new ArrayList<>();

	public TourGuide(TerrainQuad heightMap, AssetManager assetManager, Node guiNode) {
		// Paths
		createInterestingPoint(Type.Path, 506, 452, heightMap, "Q29taW5nIGhvbWU=", assetManager);
		createInterestingPoint(Type.Path, 567, 418, heightMap, "U3BhY2UgZXhwbG9yYXRpb24=", assetManager);
		createInterestingPoint(Type.Path, 669, 394, heightMap, "TGlzdGVuaW5nIHRvIG9sZCBmb3Jnb3R0ZW4gbXVzaWM=", assetManager);
		createInterestingPoint(Type.Path, 630, 290, heightMap, "RnJpZW5kcw==", assetManager);
		createInterestingPoint(Type.Path, 651, 255, heightMap, "UGFuY2FrZXM=", assetManager);
		createInterestingPoint(Type.Path, 632, 28, heightMap, "U2NlbnQgb2Ygd29vZA==", assetManager);
		createInterestingPoint(Type.Path, 891, 156, heightMap, "QmFieSBNZXRhbA==", assetManager);
		createInterestingPoint(Type.Path, 788, 290, heightMap, "RmVlbGluZyBubyBwcmVzc3VyZQ==", assetManager);
		createInterestingPoint(Type.Path, 981, 156, heightMap, "V2V0IGdyYXNz", assetManager);
		createInterestingPoint(Type.Path, 524, 186, heightMap, "TGl0dGxlIEJpZyBBZHZlbnR1cmUgMg==", assetManager);
		createInterestingPoint(Type.Path, 453, 234, heightMap, "QmVlcg==", assetManager);
		createInterestingPoint(Type.Path, 204, 276, heightMap, "TWFraW5nIHBlb3BsZSBiZWxpZXZlIEkgYW0gc3R1cGlkIGJ1dCBhdCB0aGUgZW5kIGl0IHR1cm5zIG91dCB0aGF0IEkgd2FzIHJpZ2h0", assetManager);
		createInterestingPoint(Type.Path, 112, 130, heightMap, "TWluY2VkIG1lYXQgc2F1c2FnZXM=", assetManager);
		createInterestingPoint(Type.Path, 250, 68, heightMap, "TWVtb3JpZXMgb2YgYnVpbGRpbmcgc2hhY2tz", assetManager);
		createInterestingPoint(Type.Path, 52, 394, heightMap, "QmVpbmcgbG92ZWQ=", assetManager);
		createInterestingPoint(Type.Path, 292, 366, heightMap, "QnV5aW5nIGV4dHJlbWVseSBiaWcgYnV0IGxpZ2h0IHN0dWZmIGFuZCBsZXQgdGhlbSBkZWxpdmVyIGl0IGF0IG15IG5laWdoYm91cnMgaG91c2U=", assetManager);
		createInterestingPoint(Type.Path, 82, 576, heightMap, "RmVlbGluZyBubyBwYWlu", assetManager);
		createInterestingPoint(Type.Path, 198, 660, heightMap, "QmVpbmcgZGlmZmVyZW50", assetManager);
		createInterestingPoint(Type.Path, 304, 621, heightMap, "Tm90IGJlaW5nIHRpZWQgdG8gc3BlY2lmaWMgdGltZXMgZm9yIHNsZWVwaW5nIG9yIGJlaW5nIGF3YWtl", assetManager);
		createInterestingPoint(Type.Path, 86, 988, heightMap, "U25vdw==", assetManager);
		createInterestingPoint(Type.Path, 368, 621, heightMap, "QmVpbmcgYWJsZSB0byBkbyB0aGluZ3Mgb3RoZXJzIGNhbid0", assetManager);
		createInterestingPoint(Type.Path, 417, 812, heightMap, "QWxsb3dlZCB0byBmYWls", assetManager);
		createInterestingPoint(Type.Path, 507, 878, heightMap, "SW1wcmVzc2luZyBwZW9wbGU=", assetManager);
		createInterestingPoint(Type.Path, 453, 478, heightMap, "Um9sbGVyQ29hc3RlciBUeWNvb24=", assetManager);
		createInterestingPoint(Type.Path, 639, 614, heightMap, "UmVjZWl2aW5nIGEgZ2lmdCBjYXJk", assetManager);
		createInterestingPoint(Type.Path, 468, 666, heightMap, "Tm90IGJlaW5nIGFibGUgdG8ga2VlcCB1cCB3aXRoIHRoZSBvdGhlciBzbm93Ym9hcmRlcnM=", assetManager);
		createInterestingPoint(Type.Path, 532, 789, heightMap, "RXJyb3JzIG9mIG90aGVycyB0aGF0IHByaW1hcmlseSBhZmZlY3QgbWU=", assetManager);
		createInterestingPoint(Type.Path, 537, 976, heightMap, "RmVlbGluZyBwb3dlcmxlc3M=", assetManager);
		createInterestingPoint(Type.Path, 604, 858, heightMap, "J0ZyaWVuZHMnIGtpY2tpbmcgbWUgb3V0IG9mIHRoZSBncm91cA==", assetManager);
		createInterestingPoint(Type.Path, 849, 846, heightMap, "QSBzaG91dGluZyB0ZWFjaGVyIGFuZCBjcnlpbmcgaW4gZnJvbnQgb2YgY2xhc3NtYXRlcw==", assetManager);
		// End-points
		createInterestingPoint(Type.End, 532, 46, heightMap, "QmFyZSBmZWV0IG9uIGNhcnBldA==", assetManager);
		createInterestingPoint(Type.End, 388, 104, heightMap, "QmlydGhkYXkgZ3JlZXRpbmcgY2FyZHM=", assetManager);
		createInterestingPoint(Type.End, 396, 192, heightMap, "U2lsZW5jZQ==", assetManager);
		createInterestingPoint(Type.End, 294, 422, heightMap, "VGFsa2luZyB3aXRoIG5pY2UgcGVvcGxl", assetManager);
		createInterestingPoint(Type.End, 406, 376, heightMap, "VGhlIGdhbWVzIHBhZ2Ugb24gbXkgYmxvZw==", assetManager);
		createInterestingPoint(Type.End, 640, 448, heightMap, "TGVhcm5pbmcgc2xpZ2h0bHkgbmV3IHRoaW5ncw==", assetManager);
		createInterestingPoint(Type.End, 370, 908, heightMap, "QSBwcmV0dHkgbGFkeSB3aG8gZ2l2ZXMgbWUgYSBzbWlsZQ==", assetManager);
		createInterestingPoint(Type.End, 636, 680, heightMap, "VGhlIHNjZW50IG9mIHZhbmlsbGE=", assetManager);
		createInterestingPoint(Type.End, 770, 602, heightMap, "TXkgc2VsZi1idWlsdCBjb3VjaGVz", assetManager);
		createInterestingPoint(Type.End, 176, 730, heightMap, "QnJvdGhlciBhbmQgc2lzdGVycyBhbmQgdGhlaXIgc3RyYW5nZSBzZW5zZSBvZiBodW1vdXI=", assetManager);
		createInterestingPoint(Type.FinalEnd, 932, 814, heightMap, "RmF0aGVyIHRlbGxpbmcgbWUgdGhhdCBoZSB3aXNoZWQgSSB3YXNuJ3QgYm9ybiBoZXJl", assetManager);
		// Hidden text
		createInterestingPoint(Type.Hidden, 852, 438, heightMap, "V2hlbiBzdHJlc3MgbGV2ZWxzIHJpc2UsIG15IGRyZWFtcyBjaGFuZ2UgYW5kIGV2ZW50dWFsbHkgdmFuaXNoLiBJIHN0YXJ0IGxpc3RlbmluZyBtb3JlIHRvIG9ic2N1cmUgbXVzaWMuIEkgYmVjb21lIGFuZ3J5IGFuZCByYWdlIG92ZXIgdGhlIHNtYWxsZXN0IHRoaW5ncy4gVGhlIGhlYWQgaXMgdG9vIGJ1c3kgdG8gcHJvY2VzcyBpbmZvcm1hdGlvbi4gSXQgdXN1YWxseSB0YWtlcyB5ZWFycyB0byByZWNvdmVyLiBMdWNraWx5IEknbSBvbiB0aGUgd2F5IHVwIGFnYWluIDop", assetManager);
		createInterestingPoint(Type.Hidden, 48, 768, heightMap, "S2FzcGVyLCBSaWNvICYgQXJ0aHVy", assetManager);
		createInterestingPoint(Type.Hidden, 365, 343, heightMap, "TWVnYW4gJiBJbHNl", assetManager);
		createInterestingPoint(Type.Hidden, 861, 639, heightMap, "UGV0cmEgJiBCZXR0eQ==", assetManager);
		createInterestingPoint(Type.Hidden, 759, 114, heightMap, "SWYgeW91IHNlZSB0aGlzIHRleHQgaG92ZXJpbmcgb3ZlciB0aGUgdGVycmFpbiwgc2VuZCBtZSBhIHNjcmVlbnNob3Qgb2YgaXQgYW5kIEkgd2lsbCBidXkgeW91IGJlZXIgOik=", assetManager);

		// Add scores
		scores = new ScoreBoard(assetManager, noPoints, noPathPoints, noEndPoints);
		guiNode.attachChild(scores);
	}

	private void createInterestingPoint(Type pointType, int mapX, int mapY, TerrainQuad heightMap, String text, AssetManager assetManager) {
		PointOfInterest poi = new PointOfInterest(pointType, mapX, mapY, heightMap, text, assetManager);
		attachChild(poi);
		interestingPoints.add(poi);
		noPoints++;
		switch (pointType) {
			case Path: noPathPoints++; break;
			case End: case FinalEnd: noEndPoints++; break;
		}
	}

	public boolean update(Vector3f playerLocation, Vector3f direction) {
		boolean reachedTheEnd = false;
		for (PointOfInterest poi : interestingPoints) {
			if (poi.isReached(playerLocation)) {
				switch (poi.getType()) {
					case Path:
						scores.pathFound();
					break;
					case FinalEnd:
						reachedTheEnd = true;
					case End:
						scores.endFound();
					break;
					case Hidden:
						scores.hiddenFound();
					break;
				}
			}
		}
		return reachedTheEnd;
	}

	public void reset() {
		// Reset counters
		scores.reset();

		// Reset nodes
		for (PointOfInterest poi : interestingPoints) {
			poi.reset();
		}
	}

	public void itDied() {
		scores.thatSuckerDiedAgain();
	}

	public void hideScoreBoard() {
		scores.removeFromParent();
	}
}
