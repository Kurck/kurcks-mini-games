package mygame;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.elements.render.PanelRenderer;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * Controls normal game screens, showing a title, text and buttons.
 *
 * @author Kurck Gustavson
 */
public class GameScreenController extends AbstractAppState implements ScreenController {

	private enum FADING { IN, OUT, STABLE };

	private static final Logger LOGGER = Logger.getLogger(GameScreenController.class.getName());
	private static final String BACKGROUND_ELEMENT_NAME = "backgroundImage";
	private static final String TITLE_ELEMENT_NAME = "title";
	private static final String TEXT_ELEMENT_NAME = "text";
	private static final String[] BUTTON_NAMES = {"button0", "button1", "button2", "button3", "button4"};
	private static final String PAGE_INFORMATION_PATH = "Game/graph.xml";
	private static final int FADING_MULTIPLIER = 1;

	private final AssetManager assetManager;
	private Node rootNode;
	private Nifty nifty;
	private Application app;
	private Screen screen;
	private Page page;
	private Map<String, Page> pages = new HashMap<String, Page>();
	private FADING fadeState = FADING.IN;
	private float fadeValue = 1f;
	private Color fadeColour = new Color(0f, 0f, 0f, fadeValue);
	private Map<String, AudioNode> sounds;
	private String activeSound = null;

	public GameScreenController(AssetManager assetManagert, Node node) {
		super();
		// Add node to attach the audio to.
		this.rootNode = node;
		this.assetManager = assetManagert;
		loadSounds();
	}

	private void loadSounds() {
		// Load audio.
		sounds = new HashMap<String, AudioNode>();
		sounds.put("explosion", loadSound("Sounds/Explosion.ogg", false));
		sounds.put("smash", loadSound("Sounds/GruntSmash.ogg", false));
		sounds.put("phone", loadSound("Sounds/ringingPhone.ogg", true));
		sounds.put("splash", loadSound("Sounds/Splash.wav", false));
		sounds.put("gunshot", loadSound("Sounds/LayeredGunshot3.wav", false));
		loadSound("Sounds/ambient.ogg", true).play();
	}

	private AudioNode loadSound(String path, boolean looping) {
		AudioNode sound = new AudioNode(assetManager, path, false);
		sound.setPositional(false);
		sound.setLooping(looping);
		sound.setVolume(1.0F);
		rootNode.attachChild(sound);
		return sound;
	}

	@Override
	public void bind(Nifty newNifty, Screen newScreen) {
		this.nifty = newNifty;
		this.screen = newScreen;
	}

	@Override
	public void onStartScreen() {
		LOGGER.setLevel(Level.ALL);
		String firstPageId = loadPages();
		toPage(firstPageId);
	}

	@Override
	public void onEndScreen() {
	}

	@Override
	public void initialize(AppStateManager stateManager, Application newApp) {
		this.app = newApp;
	}

	private String loadPages() {
		// Load the XML file containing page information.
		Document doc = (Document) assetManager.loadAsset(PAGE_INFORMATION_PATH);

		// Parse the information from the file.
		String firstPage = null;
		NodeList pageList = doc.getElementsByTagName("page");
		for (int i = 0; i < pageList.getLength(); i++) {
			org.w3c.dom.Node node = pageList.item(i);

			// Skip non-element nodes.
			if (node.getNodeType() != org.w3c.dom.Node.ELEMENT_NODE) {
				continue;
			}

			// Add the page to the collection.
			Page newPage = new Page((org.w3c.dom.Element) node);
			pages.put(newPage.getId(), newPage);

			// Save the first id.
			if (firstPage == null) {
				firstPage = newPage.getId();
			}
		}
		return firstPage;
	}

	@Override
	public void update(float tpf) {
		// Fade if needed. The limiter at the end is to skip loading times.
		if (fadeState != FADING.STABLE && tpf < 0.1f) {
			fade(tpf);
		}
	}

	/**
	 * Fade in and out, based on the current state.
	 * @param tpf time per frame.
	 */
	private void fade(float tpf) {
		// TODO: this should probably done using effects, but for now (due to time constraints) I will do it like this.
		// Update the fade value (alpha channel of the used colour).
		float fadeSpeed = tpf * FADING_MULTIPLIER;
		if (fadeState == FADING.IN && "0a".equals(page.getId())) {
			// Fade slower on the first page.
			fadeSpeed /= 4;
		}
		fadeValue += fadeState == FADING.IN ? -fadeSpeed : fadeSpeed;
		if (fadeValue < 0f) {
			fadeValue = 0f;
			fadeState = FADING.STABLE;
			//System.out.println("End fade in.");
		} else if (fadeValue > 1f) {
			fadeValue = 1f;
			fadeState = FADING.STABLE;
			// End of fade out, load page (a hack to make page loading wait for the fade out to finish).
			loadPage();
		}

		// Debug logging.
		if (LOGGER.isLoggable(Level.FINEST)) {
			Object[] logParams = new Object[]{ fadeValue, fadeSpeed, fadeState, tpf};
			LOGGER.log(Level.FINEST, "Fade value {0}, diff: {1}, in state: {2}. TPF: {3}", logParams);
		}

		// Update the fader panel.
		//System.out.println("Fading: " + fadeValue);
		fadeColour.setAlpha(fadeValue);
		Element fader = screen.findElementByName("fader");
		fader.getRenderer(PanelRenderer.class).setBackgroundColor(fadeColour);
	}

	/**
	 * Switches pages and updates the page components.
	 *
	 * @param buttonId	the id of pressed button.
	 */
	public void toPage(String buttonId) {
		// Get the page id for the page to load.
		String pageId = buttonId;
		if (page != null) {
			pageId = page.getButton(Integer.parseInt(buttonId)).getTarget();
		}

		// Stop the running sound.
		if (activeSound != null) {
			sounds.get(activeSound).stop();
		}

		// Quit the game if requested.
		if ("quit".equals(pageId)) {
			triggerFadeOut();
			app.stop();
			return;
		}

		// Update the current page.
		page = pages.get(pageId);
		if (page == null) {
			LOGGER.log(Level.WARNING, "No page found for id {0}!", pageId);
			return;
		}

		triggerFadeOut();
	}

	private void triggerFadeIn() {
		fadeValue = 1.0f;
		fadeState = FADING.IN;
		//System.out.println("Start fade in.");
	}

	private void triggerFadeOut() {
		fadeValue = 0.0f;
		fadeState = FADING.OUT;
		//System.out.println("Start fade out.");
	}

	private void loadPage() {
		// Update title, text, and buttons.
		updateBackground();
		updateTextElement(TITLE_ELEMENT_NAME, page.getTitle());
		updateTextElement(TEXT_ELEMENT_NAME, page.getText());
		updateButtons();
		triggerFadeIn();
		// Start page related sounds.
		String soundId = page.getSound();
		AudioNode audio = sounds.get(soundId);
		if (audio != null) {
			audio.play();
			activeSound = soundId;
		}
	}

	private void updateBackground() {
		Element background = screen.findElementByName(BACKGROUND_ELEMENT_NAME);
		ImageRenderer renderer = background.getRenderer(ImageRenderer.class);
		String backgroundPath = "Interface/backgrounds/" + page.getBackground();
		NiftyImage newImage = nifty.createImage(backgroundPath, false);
		renderer.setImage(newImage);
	}

	private void updateTextElement(String elementId, String text) {
		Element element = screen.findElementByName(elementId);
		element.getRenderer(TextRenderer.class).setText(text);
	}

	private void updateButtons() {
		boolean firstButton = true;
		for (int i = 0; i < BUTTON_NAMES.length; i++) {
			Element button = screen.findElementByName(BUTTON_NAMES[i]);
			Button buttonInfo = page.getButton(i);

			// Hide the button if it is not used.
			if (buttonInfo == null) {
				button.setVisible(false);
				LOGGER.log(Level.FINEST, "Hiding button #{0}.", i);
				continue;
			} else {
				button.setVisible(true);
			}

			// Update the button's label.
			Element buttonText = screen.findElementByName(BUTTON_NAMES[i] + "#text");
			LOGGER.log(Level.FINER, "Got label {0}.", buttonInfo.getLabel());
			buttonText.getRenderer(TextRenderer.class).setText(buttonInfo.getLabel());

			// Disable the button, if needed.
			if (buttonInfo.isActive()) {
				button.enable();
			} else {
				button.disable();
				LOGGER.log(Level.FINEST, "Disabling button #{0}.", i);
				continue;
			}

			//https://www.scribd.com/doc/238611148/nifty-gui-the-manual-v1-0-pdf
			// Highlight the first button (if visible and not disabled).
			if (firstButton) {
				button.setFocus();
				LOGGER.log(Level.FINEST, "Setting focus to button #{0}.", i);
				firstButton = false;
			}
		}
	}
}
