package mygame;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Kurck Gustavson
 */
public class Page {

	private String id;
	private String title;
	private String text;
	private String background;
	private String sound;
	private List<Button> buttons;

	public Page(Element element) {
		this.id = element.getAttribute("id");
		this.title = getTextFromFirstNode(element, "title");
		this.text = getTextFromFirstNode(element, "text");
		this.background = getTextFromFirstNode(element, "background");
		this.sound = getTextFromFirstNode(element, "sound");
		this.buttons = loadButtons(element);
	}

	/**
	 * Reads the text content of the first (sub)child node having the given name.
	 * @param e	the parent/root element.
	 * @param nodeName	the name of the node to find.
	 * @return	the text content of found node.
	 */
	private String getTextFromFirstNode(Element e, String nodeName) {
		NodeList nodes = e.getElementsByTagName(nodeName);
		if (nodes.getLength() == 0) return null;
		Node firstNode = nodes.item(0);
		if (firstNode == null) return null;
		return firstNode.getTextContent();
	}

	/**
	 * Reads through all button elements and creates a list of buttons.
	 * @param element	the parent/root element.
	 * @return	a list of buttons.
	 */
	private List<Button> loadButtons(Element element) {
		NodeList buttonList = element.getElementsByTagName("button");
		List<Button> buttonCollection = new ArrayList<Button>(buttonList.getLength());
		for (int i = 0; i < buttonList.getLength(); i++) {
			// Skip non-element nodes.
			Node node = buttonList.item(i);
			if (node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			// Create and add the button.
			Button button = new Button((Element) node);
			buttonCollection.add(button);
		}
		return buttonCollection;
	}

	public String getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public String getText() {
		return this.text;
	}

	public String getBackground() {
		return this.background;
	}

	public String getSound() {
		return this.sound;
	}

	public Button getButton(int buttonNumber) {
		if (buttonNumber < buttons.size()) {
			return buttons.get(buttonNumber);
		} else {
			return null;
		}
	}

}
