package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.system.AppSettings;
import de.lessvoid.nifty.Nifty;

/**
 * A game of love. *cough*
 * @author Kurck Gustavson
 */
public class Main extends SimpleApplication {

	public static void main(String[] args) {
		Main app = new Main();
		AppSettings cfg = new AppSettings(true);
		cfg.setTitle("Perfection Cannot Be Caged");
		cfg.setSettingsDialogImage("Interface/perfectionCannotBeCaged.jpg");
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		cfg.setFrameRate(60);
		cfg.setVSync(true);
		cfg.setFrequency(60);
		cfg.setResolution(1280, 1024);
		cfg.setFullscreen(false);
		cfg.setSamples(2);
		app.setSettings(cfg);
		app.start();
	}

	@Override
	public void simpleInitApp() {
		// Apply basic settings.
		setDisplayFps(false);
		setDisplayStatView(false);
		flyCam.setDragToRotate(true);

		// Add an extra loader to the assetManager for reading XML.
		assetManager.registerLoader(XmlLoader.class, "xml");

		// Activate the Nifty-JME integration.
		GameScreenController screenController = new GameScreenController(assetManager, rootNode);
		stateManager.attach(screenController);
		NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
		Nifty nifty = niftyDisplay.getNifty();
		//nifty.setDebugOptionPanelColors(true);
		guiViewPort.addProcessor(niftyDisplay);
		nifty.fromXml("Interface/GameScreen.xml", "gameScreen", screenController);
	}

}
