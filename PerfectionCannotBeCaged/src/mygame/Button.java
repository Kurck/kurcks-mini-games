package mygame;

import org.w3c.dom.Element;

/**
 *
 * @author kurck
 */
public class Button {

	private String label;
	private String target;

	public Button(Element element) {
		this.label = element.getAttribute("label");
		this.target = element.getAttribute("target");
	}

	public String getLabel() {
		return this.label;
	}

	public String getTarget() {
		return this.target;
	}

	boolean isActive() {
		// TODO
		return true;
	}

}