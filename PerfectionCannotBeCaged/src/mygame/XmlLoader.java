package mygame;

import com.jme3.asset.AssetInfo;
import com.jme3.asset.AssetLoader;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Opens the XML file and returns the normalised Document.
 * @author Kurck Gustavson
 */
public class XmlLoader implements AssetLoader {

	private static Logger logger = Logger.getLogger(XmlLoader.class.getName());

	public Object load(AssetInfo assetInfo) throws IOException {
		Document doc = null;
		InputStream stream = null;
		try {
			stream = assetInfo.openStream();
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder;
			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.parse(stream);
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException ex) {
			logger.log(Level.SEVERE, "Bad parse configuration.", ex);
		} catch (SAXException ex) {
			logger.log(Level.SEVERE, "Unable to parse the XML file.", ex);
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException ioe) {
					logger.log(Level.SEVERE, "Unable to close the stream.", ioe);
				}
			}
		}
		return doc;
	}

}
