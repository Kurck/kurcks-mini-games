document.addEventListener("DOMContentLoaded", () => {
	const cards = [
		{ name: "artsy", image: "images/artsy.png" },
		{ name: "dragon", image: "images/dragon.png" },
		{ name: "fish", image: "images/fish.png" },
		{ name: "ghosts", image: "images/ghosts.png" },
		{ name: "pinkpop", image: "images/pinkpop.png" },
		{ name: "star", image: "images/star.png" },
		{ name: "sugababes", image: "images/sugababes.png" },
		{ name: "terschelling", image: "images/terschelling.png" },
		{ name: "artsy", image: "images/artsy.png" },
		{ name: "dragon", image: "images/dragon.png" },
		{ name: "fish", image: "images/fish.png" },
		{ name: "ghosts", image: "images/ghosts.png" },
		{ name: "pinkpop", image: "images/pinkpop.png" },
		{ name: "star", image: "images/star.png" },
		{ name: "sugababes", image: "images/sugababes.png" },
		{ name: "terschelling", image: "images/terschelling.png" }
	];
	cards.sort(() => 0.5 - Math.random());

	const grid = document.getElementById("grid");
	const scoreSpan = document.getElementById("score");
	var score = 0;
	var chosenCardA = null;
	var chosenCardB = null;
	var cardsWon = [];

	function createBoard() {
		for (let i = 0; i < cards.length; i++) {
			var card = document.createElement("img");
			card.id = i;
			card.className = "card";
			card.setAttribute("src", "images/unknown.png");
			card.setAttribute("alt", "Unknown");
			card.addEventListener("click", flipCard);
			grid.appendChild(card);
		}
	}

	function checkForMatch() {
		var cardA = cards[chosenCardA.id];
		var cardB = cards[chosenCardB.id];

		if (cardA.name === cardB.name) {
			score++;
			chosenCardA.setAttribute("src", "images/empty.png");
			chosenCardA.className = "empty";
			cardsWon.push(cardA);
			chosenCardB.setAttribute("src", "images/empty.png");
			chosenCardB.className = "empty";
			cardsWon.push(cardB);
		} else {
			score--;
			chosenCardA.setAttribute("src", "images/unknown.png");
			chosenCardB.setAttribute("src", "images/unknown.png");
		}

		scoreSpan.innerText = score;
		chosenCardA = null;
		chosenCardB = null;

		if (cardsWon.length === cards.length) {
			alert("Congratulations! You have completed the game ^_^");
		}
	}

	function flipCard() {
		if (!chosenCardB) {
			this.setAttribute("src", cards[this.id].image);
			if (chosenCardA && !chosenCardB) {
				chosenCardB = this;
				setTimeout(checkForMatch, 500);
			} else if (!chosenCardA) {
				chosenCardA = this;
			}
		}
	}

	createBoard();
});