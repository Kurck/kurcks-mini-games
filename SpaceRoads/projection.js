console.clear();

// Get the canvas element from the DOM
const canvas = document.querySelector('#scene');
canvas.width = canvas.clientWidth;
canvas.height = canvas.clientHeight;
// Store the 2D context
const ctx = canvas.getContext('2d');

if (window.devicePixelRatio > 1) {
	canvas.width = canvas.clientWidth * 2;
	canvas.height = canvas.clientHeight * 2;
	ctx.scale(2, 2);
}

/* ====================== */
/* ====== VARIABLES ===== */
/* ====================== */
let width = canvas.clientWidth; // Width of the canvas
let height = canvas.clientHeight; // Height of the canvas
let previousTime = 0;

/* ====================== */
/* ====== CONSTANTS ===== */
/* ====================== */
/* Some of those constants may change if the user resizes their screen but I still strongly believe they belong to the Constants part of the variables */
let GLOBE_RADIUS = width * 0.7; // Radius of the globe
let GLOBE_CENTER_Z = -GLOBE_RADIUS; // Z value of the globe center
let PROJECTION_CENTER_X = width / 2; // X center of the canvas HTML
let PROJECTION_CENTER_Y = height / 2; // Y center of the canvas HTML
let FIELD_OF_VIEW = width * 0.9;
const HEX = "0123456789ABCDEF";

class Vector3D {
	constructor(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	add(dx, dy, dz) {
		return new Vector3D(this.x + dx, this.y + dy, this.z + dz);
	}

	plus(other) {
		return new Vector3D(this.x + other.x, this.y + other.y, this.z + other.z);
	}

	multiplyBy(factor) {
		return new Vector3D(this.x * factor, this.y * factor, this.z * factor);
	}

	project() {
		let size = FIELD_OF_VIEW / (FIELD_OF_VIEW - this.z);
		return {
			x: PROJECTION_CENTER_X + this.x * size,
			y: PROJECTION_CENTER_Y - this.y * size,
			size: size
		};
	}

	toString() {
		return `(${this.x}, ${this.y}, ${this.z})`;
	}
}

const BOX_SIZE = 16;

class Box {
	constructor(position) {
		this.position = position;
		this.speed = new Vector3D(0, 0, 96);
		this.isActive = true;
	}

	update(timePassed) {
		this.position = this.position.plus(this.speed.multiplyBy(timePassed));
	}

	draw() {
		if (!this.isActive) {
			return;
		}

		let blt = this.position.add(-BOX_SIZE, BOX_SIZE, -3 * BOX_SIZE).project();
		if (blt.y > height) {
			this.isActive = false;
			return;
		}

		let flb = this.position.add(-BOX_SIZE, 0, BOX_SIZE).project();
		let flt = this.position.add(-BOX_SIZE, BOX_SIZE, BOX_SIZE).project();
		let frt = this.position.add(BOX_SIZE, BOX_SIZE, BOX_SIZE).project();
		let frb = this.position.add(BOX_SIZE, 0, BOX_SIZE).project();
		let brt = this.position.add(BOX_SIZE, BOX_SIZE, -3 * BOX_SIZE).project();

		this.drawQuad("#0B0", [ blt, flt, frt, brt ]);
		if (blt.x < flt.x) {
			let blb = this.position.add(-BOX_SIZE, 0, -3 * BOX_SIZE).project();
			this.drawQuad("#BB0", [ blb, blt, flt, flb ]);
			this.drawVertex(blb, "blb");
		}
		if (brt.x > frt.x) {
			let brb = this.position.add(BOX_SIZE, 0, -3 * BOX_SIZE).project();
			this.drawQuad("#B0B", [ brt, frt, frb, brb ]);
			this.drawVertex(brb, "brb");
		}
		this.drawQuad("#00B", [ flt, frt, frb, flb ]);

		this.drawVertex(flb, "flb");
		this.drawVertex(flt, "flt");
		this.drawVertex(frt, "frt");
		this.drawVertex(frb, "frb");
		this.drawVertex(blt, "blt");
		this.drawVertex(brt, "brt");

		this.drawText(frb.x + BOX_SIZE + 8, frb.y, this.position.toString());
	}

	drawQuad(colour, points) {
		ctx.fillStyle = colour;
		ctx.beginPath();
		for (let i = 0; i < points.length; i++) {
			if (i === 0) {
				ctx.moveTo(points[i].x, points[i].y);
			} else {
				ctx.lineTo(points[i].x, points[i].y);
			}
		}
		ctx.closePath();
		ctx.fill();
	}

	drawVertex(vertex, name) {
		this.drawText(vertex.x + 5, vertex.y + 5, name);
		ctx.beginPath();
		ctx.arc(vertex.x, vertex.y, Math.max(1, vertex.size), 0, Math.PI * 2);
		ctx.closePath();
		ctx.fill();
	}

	drawText(x, y, text) {
		ctx.fillStyle = "#000";
		ctx.font = "10px Comic Sans MS";
		ctx.fillText(text, x, y); 
	}
}

let boxes = [];

/* ====================== */
/* ======== RENDER ====== */
/* ====================== */
function render(timeStamp) {
	ctx.clearRect(0, 0, width, height);

	let timePassed = (timeStamp - previousTime) / 1000;
	previousTime = timeStamp;
	boxes.forEach(box => {
		box.update(timePassed);
		box.draw();
	})
	boxes = boxes.filter(box => box.isActive);

	window.requestAnimationFrame(render);
}

// Function called after the user resized its screen
function afterResize() {
	width = canvas.offsetWidth;
	height = canvas.offsetHeight;
	if (window.devicePixelRatio > 1) {
		canvas.width = canvas.clientWidth * 2;
		canvas.height = canvas.clientHeight * 2;
		ctx.scale(2, 2);
	} else {
		canvas.width = width;
		canvas.height = height;
	}
	GLOBE_RADIUS = width * 0.7;
	GLOBE_CENTER_Z = -GLOBE_RADIUS;
	PROJECTION_CENTER_X = width / 2;
	PROJECTION_CENTER_Y = height / 2;
	FIELD_OF_VIEW = width * 0.8;
}

// Variable used to store a timeout when user resized its screen
let resizeTimeout;
// Function called right after user resized its screen
function onResize() {
	// Clear the timeout variable
	resizeTimeout = window.clearTimeout(resizeTimeout);
	// Store a new timeout to avoid calling afterResize for every resize event
	resizeTimeout = window.setTimeout(afterResize, 500);
}
window.addEventListener('resize', onResize);

// Populate the dots array with random blocks
boxes.push(new Box(new Vector3D(0, -128, 100)));
boxes.push(new Box(new Vector3D(32, -128, 200)));
boxes.push(new Box(new Vector3D(-32, -128, 300)));
boxes.push(new Box(new Vector3D(-64, -128, 400)));
boxes.push(new Box(new Vector3D(0, -128, 500)));

// Render the scene
window.requestAnimationFrame(render);