document.addEventListener("DOMContentLoaded", () => {

	const HEIGHT = 600;
	const WIDTH = 800;

	const DISPLAY = document.getElementById("display");
	const SCORE_SPAN = document.getElementById("score");
	const LEVEL_SPAN = document.getElementById("level");

	class Vector2D {
		constructor(x, y) {
			this.x = x;
			this.y = y;
		}

		squaredLength() {
			return this.x * this.x + this.y * this.y;
		}

		length() {
			return Math.sqrt(this.squaredLength());
		}

		normalize() {
			return this.devideBy(this.length());
		}

		normalizeSquared() {
			return this.devideBy(this.squaredLength());
		}

		squaredDistanceTo(other) {
			return (this.x - other.x) * (this.x - other.x)
				+ (this.y - other.y) * (this.y - other.y);
		}

		distanceTo(other) {
			return Math.sqrt(this.squaredDistanceTo(other));
		}

		minus(other) {
			return new Vector2D(this.x - other.x, this.y - other.y);
		}

		plus(other) {
			return new Vector2D(this.x + other.x, this.y + other.y);
		}

		multiplyBy(factor) {
			return new Vector2D(this.x * factor, this.y * factor);
		}

		devideBy(factor) {
			return new Vector2D(this.x / factor, this.y / factor);
		}
	}

	class Vector3D {
		constructor(x, y, z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		squaredLength() {
			return this.x * this.x + this.y * this.y + this.z * this.z;
		}

		length() {
			return Math.sqrt(this.squaredLength());
		}

		normalize() {
			return this.devideBy(this.length());
		}

		normalizeSquared() {
			return this.devideBy(this.squaredLength());
		}

		squaredDistanceTo(other) {
			return (this.x - other.x) * (this.x - other.x)
				+ (this.y - other.y) * (this.y - other.y)
				+ (this.z - other.z) * (this.z - other.z);
		}

		distanceTo(other) {
			return Math.sqrt(this.squaredDistanceTo(other));
		}

		minus(other) {
			return new Vector3D(this.x - other.x, this.y - other.y, this.z - other.z);
		}

		plus(other) {
			return new Vector3D(this.x + other.x, this.y + other.y, this.z + other.z);
		}

		multiplyBy(factor) {
			return new Vector3D(this.x * factor, this.y * factor, this.z * factor);
		}

		devideBy(factor) {
			return new Vector3D(this.x / factor, this.y / factor, this.z / factor);
		}
	}

	class Spacecraft {
		constructor() {
			let geometry = new THREE.IcosahedronGeometry();
			let material = new THREE.MeshLambertMaterial({ color: 0xffff44 });
			this.mesh = new THREE.Mesh(geometry, material);
			this.reset();
		}

		reset() {
			this.mesh.position.x = 0;
			this.mesh.position.y = 2;
			this.mesh.position.z = 0;

			this.speedingUp = false;
			this.slowingDown = false;
			this.movingLeft = false;
			this.movingRight = false;
			this.jumping = false;
			this.onPlatform = false;
			this.position = new Vector2D(WIDTH / 2, HEIGHT - 96);
			this.speed = new THREE.Vector3();
			this.direction = new Vector2D(0, 0);
		}

		update(timePassed, level) {
			if (this.speedingUp) {
				this.speed.z = Math.max(-5, this.speed.z - 0.5 * timePassed);
			}
			if (this.slowingDown) {
				this.speed.z = Math.min(0, this.speed.z + 0.5 * timePassed);
			}

			if (this.movingLeft) {
				this.speed.x -= 1 * timePassed;
			}
			if (this.movingRight) {
				this.speed.x += 1 * timePassed;
			}

			if (this.onPlatform) {
				if (!this.movingLeft && !this.movingRight) {
					this.speed.x -= 2 * this.speed.x * timePassed;
				}
				if (this.jumping) {
					this.speed.y -= 2;
					this.onPlatform = false;
				}
			} else {
				this.speed.y -= level.gravity * timePassed;
			}
			this.mesh.position.add(this.speed);

			// TODO: collission detection
			// Find the block at current mesh position: round off the position, devide it and then get the value of the block in the level
			// Does it collide? -> stop the spacecraft from moving

			if (this.position.x < 0 || this.position.x > WIDTH || this.position.y > HEIGHT) {
				// TODO: lost spacecraft
			}
		}
	}

	const BLOCK_TYPES = [
		{ name: "ROAD_A", chance: 0.2, material: new THREE.MeshLambertMaterial({ color: 0x4444cc }) },
		{ name: "ROAD_B", chance: 0.75, material: new THREE.MeshLambertMaterial({ color: 0x222278 }) },
		{ name: "ROAD_C", chance: 0.91, material: new THREE.MeshLambertMaterial({ color: 0x1a1f2a }) },
		{ name: "LAUNCHER", chance: 0.94, material: new THREE.MeshLambertMaterial({ color: 0xaaffaa }) },
		{ name: "BREAKER", chance: 0.97, material: new THREE.MeshLambertMaterial({ color: 0x1111ee }) },
		{ name: "MELTER", chance: 1.0, material: new THREE.MeshLambertMaterial({ color: 0xff0000 }) },

		//{ name: "FUEL_STATION", material: new THREE.MeshLambertMaterial({ color: 0x00aa00 }) },
		//{ name: "FINISH", material: new THREE.MeshLambertMaterial({ color: 0xaaaaaa }) }
	];

	const BLOCK_GEOMETRY = new THREE.BoxGeometry(1.5, 0.5, 8);

	class Level {
		constructor(number) {
			this.group = new THREE.Group();
			this.number = number;
			this.speed = 0;
			this.gravity = 0.5;
			this.generate();
			this.reset();
		}

		generate() {
			let maxZ = 128 * Math.log(this.number / BLOCK_GEOMETRY.parameters.depth + 2);
			this.blocks = [];
			for (let i = 0, z = 0; z < maxZ; z += BLOCK_GEOMETRY.parameters.depth, i++) {
				this.blocks[i] = [];
				// TODO: last block
				for (let j = 0; j < 4; j++) {
					if ((j == 0 && i == 0) || Math.random() < 0.5) {
						// TODO: let type & y depend on the previous block
						let blockType = i == 0 && j == 0 ? BLOCK_TYPES[0] : this.selectBlockType();
						let y = BLOCK_GEOMETRY.parameters.height * Math.floor(Math.random() * 2);
						this.createBlock(blockType, BLOCK_GEOMETRY.parameters.width*j, y, z);
						if (j > 0) {
							this.createBlock(blockType, -BLOCK_GEOMETRY.parameters.width*j, y, z);
						}
						this.blocks[i][j] = y;
					} else {
						this.blocks[i][j] = -1;
					}
				}
			}
		}

		selectBlockType() {
			let materialSelector = Math.random();
			for (let i = 0; i < BLOCK_TYPES.length; i++) {
				if (materialSelector < BLOCK_TYPES[i].chance) {
					return BLOCK_TYPES[i];
				}
			}
			return BLOCK_TYPES[0];
		}

		createBlock(blockType, x, y, z) {
			let block = new THREE.Mesh(BLOCK_GEOMETRY, blockType.material);
			block.position.x = x;
			block.position.y = y;
			block.position.z = -z;
			this.group.add(block);
		}

		reset() {
			this.progress = 0;
		}

		update(timePassed, spacecraft) {
			//this.progress += spacecraft.speed * timePassed;
			this.group.position.z += timePassed * 5;
			// TODO: remove/dispose blocks from scene when out of sight
		}
	}

	let scene;
	let renderer;
	let camera;
	let focussed = true;
	let previousTime = 0;

	let score;
	let level;

	let spacecraft;

	initialize();

	function initialize() {
		initializeScene();
		score = 0;
		spacecraft = new Spacecraft();
		scene.add(spacecraft.mesh);
		level = new Level(0);
		scene.add(level.group);
		window.onblur = () => focussed = false;
		window.onfocus = () => focussed = true;
		document.addEventListener("keydown", handleKeyDown);
		document.addEventListener("keyup", handleKeyUp);
		window.requestAnimationFrame(update);
	}

	function initializeScene() {
		scene = new THREE.Scene();

		const light = new THREE.HemisphereLight(0xffffbb, 0x080820, 1);
		scene.add( light );

		camera = new THREE.PerspectiveCamera(75, DISPLAY.width / DISPLAY.height, 0.1, 80);
		camera.position.x = 0;
		camera.position.y = 4;
		camera.position.z = 10;

		renderer = new THREE.WebGLRenderer({ canvas: DISPLAY, antialias: true });
		renderer.setSize(DISPLAY.width, DISPLAY.height);
	}

	function handleKeyDown(event) {
		if (event.code === "KeyW" || event.code === "ArrowUp") {
			spacecraft.speedingUp = true;
		} else if (event.code === "KeyS" || event.code === "ArrowDown") {
			spacecraft.slowingDown = true;
		} else if (event.code === "KeyA" || event.code === "ArrowLeft") {
			spacecraft.movingLeft = true;
			spacecraft.speed.x -= 0.2;
		} else if (event.code === "KeyD" || event.code === "ArrowRight") {
			spacecraft.movingRight = true;
			spacecraft.speed.x += 0.2;
		} else if (event.code === "Space") {
			spacecraft.jumping = true;
		}
	}

	function handleKeyUp(event) {
		if (event.code === "KeyW" || event.code === "ArrowUp") {
			spacecraft.speedingUp = false;
		} else if (event.code === "KeyS" || event.code === "ArrowDown") {
			spacecraft.slowingDown = false;
		} else if (event.code === "KeyA" || event.code === "ArrowLeft") {
			spacecraft.movingLeft = false;
		} else if (event.code === "KeyD" || event.code === "ArrowRight") {
			spacecraft.movingRight = false;
		} else if (event.code === "Space") {
			spacecraft.jumping = false;
		}
	}

	function update(timeStamp) {
		if (focussed) {
			let timePassed = (timeStamp - previousTime) / 1000;
			spacecraft.update(timePassed, level);
			camera.position.z = spacecraft.mesh.position.z + 10;
			renderer.render(scene, camera);
		}
		previousTime = timeStamp;
		window.requestAnimationFrame(update);
	}

});