document.addEventListener("DOMContentLoaded", () => {

	const HEIGHT = 600;
	const WIDTH = 400;
	const GRID = document.getElementById("grid");
	const SCORE_SPAN = document.getElementById("score");

	class Doodler {
		constructor(x, y) {
			this.x = x;
			this.y = y;
			this.hSpeed = 0.0;
			this.vSpeed = 0.0;
			this.isAlive = true;
			this.onPlatform = false;

			this.visual = document.createElement("div");
			this.visual.id = "doodler";
			this.visual.style.left = this.x + "px";
			this.visual.style.bottom = this.y + "px";
			GRID.appendChild(this.visual);
		}

		jump = function() {
			if (this.isAlive && this.onPlatform) {
				this.onPlatform = false;
				this.vSpeed += 24.0;
			}
		}

		move = function(amount) {
			if (this.isAlive && Math.abs(this.hSpeed) < 12.0) {
				this.hSpeed += amount;
			}
		}

		moveDown = function(amount) {
			this.y -= amount;
			this.visual.style.bottom = Math.floor(this.y) + "px";
			this.aliveCheck();
		}

		update = function(platforms) {
			this.updateHorizontalMovement();
			this.updateVerticalMovement(platforms);
			this.aliveCheck();
		}

		aliveCheck = function() {
			if (this.y < 0) {
				this.isAlive = false;
			}
		}

		updateHorizontalMovement = function() {
			this.x += this.hSpeed;
			if (this.x < 0) {
				this.x = 0;
				this.hSpeed = 0.0;
			} else if (this.x > WIDTH - 60) {
				this.x = WIDTH - 60;
				this.hSpeed = 0.0;
			}
			this.visual.style.left = Math.floor(this.x) + "px";
			this.hSpeed *= 0.9;
		}

		updateVerticalMovement = function(platforms) {
			if (this.vSpeed < 0.0) {
				this.y = this.checkCollisions(this.y + this.vSpeed, platforms);
			} else {
				this.y += this.vSpeed;
				this.vSpeed--;
			}
			this.visual.style.bottom = Math.floor(this.y) + "px";
		}

		checkCollisions = function(newY, platforms) {
			for (let i = 0; i < platforms.length; i++) {
				let top = platforms[i].y + 13;
				if (((newY > platforms[i].y && newY <= top) || (this.y >= platforms[i].y && newY <= top))
						&& this.x > platforms[i].x - 50 && this.x < platforms[i].x + 75) {
					this.vSpeed = 0.0;
					this.onPlatform = true;
					return top;
				}
			}
			this.onPlatform = false;
			this.vSpeed--;
			return newY;
		}

		destruct = function() {
			GRID.removeChild(this.visual);
		}
	}

	class Platform {
		constructor(y) {
			this.x = Math.floor(Math.random() * (WIDTH - 85));
			this.y = y;
			this.isAlive = true;

			this.visual = document.createElement("div");
			this.visual.classList.add("platform");
			this.visual.style.left = this.x + "px";
			this.visual.style.bottom = this.y + "px";
			GRID.appendChild(this.visual);
		}

		moveDown = function(amount) {
			this.y -= amount;
			if (this.y < 0) {
				this.destruct();
			} else {
				this.visual.style.bottom = this.y + "px";
			}
		}

		destruct = function() {
			GRID.removeChild(this.visual);
			this.isAlive = false;
		}
	}

	let doodler;
	let platforms = [];
	let updateTimerId;
	let platformGap;
	let score;

	function start() {
		if (updateTimerId) {
			clearInterval(updateTimerId);
		}
		GRID.style.width = WIDTH + "px";
		GRID.style.height = HEIGHT + "px";
		createPlatforms();
		createDoodler();
		updateTimerId = setInterval(update, 20);
	}

	function createPlatforms() {
		platforms.forEach(platform => platform.destruct());
		platformGap = Math.floor(HEIGHT / 5);
		platforms = [];
		for (let i = 0; i < 5; i++) {
			let y = 100 + i * platformGap;
			platforms.push(new Platform(y));
		}
	}

	function createDoodler() {
		if (doodler) {
			doodler.destruct();
		}
		score = 0;
		let firstPlatform = platforms[0];
		doodler = new Doodler(firstPlatform.x + 13, firstPlatform.y + 20);
	}

	function update() {
		if (doodler.isAlive) {
			doodler.update(platforms);
			if (doodler.y > 300) {
				platforms.forEach(platform => { platform.moveDown(4); });
				doodler.moveDown(4);
				if (!platforms[0].isAlive) {
					platforms.shift();
					let topPlatform = platforms[platforms.length - 1];
					platforms.push(new Platform(topPlatform.y + platformGap));
					score += platformGap;
					SCORE_SPAN.innerText = score;
					platformGap++;
				}

			}
		} else {
			clearInterval(updateTimerId);
			alert("GAME OVER! The doodler died :(");
		}
	}

	const LEFT = 37;
	const UP = 38;
	const RIGHT = 39;
	const R = 82;
	const ENTER = 13;
	
	function control(event) {
		if (event.keyCode === LEFT) {
			doodler.move(-8);
		} else if (event.keyCode === UP) {
			doodler.jump();
		} else if (event.keyCode === RIGHT) {
			doodler.move(8);
		} else if (event.keyCode === R || event.keyCode === ENTER) {
			start();
		}
	}
	document.addEventListener("keydown", control);

	start();

});