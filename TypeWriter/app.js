document.addEventListener("DOMContentLoaded", () => {

	const DISPLAY = document.getElementById("display");
	const PHRASES = [
		"ANJALI BISARIA\n\nI met my soulmate. She didn't.",
		"KARTIK GHODASARA\n\n\"Wrong number\", said a familiar voice.",
		"ANJALI BISARIA\n\nBrought roses home. Keys didn't fit.",
		"ANNOYMOUS\n\nI put away the razors again.",
		"SONALI JAIN\n\nHe lies there with no heart, but heartless is the last thing you'll call him.",
		"ANNOYMOUS\n\nIntroduced myself to mother again today."
	];

	let currentPhraseIndex = 0;
	let currentCharacterIndex = 0;
	let currentText = [];
	let typing = true;
	let timeOut = 100;

	function writePhrases() {
		if (typing) {
			writeCharacter();
		} else if (currentText.length === 0) {
			switchToNextPhrase();
		} else {
			removeLastCharacter();
		}
		setTimeout(writePhrases, timeOut + Math.random() * timeOut);
	}

	function writeCharacter() {
		let phrase = PHRASES[currentPhraseIndex];
		let currentChar = phrase[currentCharacterIndex]
		currentText.push(currentChar);
		DISPLAY.innerText = currentText.join("");
		currentCharacterIndex++;
		if (currentCharacterIndex >= phrase.length) {
			timeOut = 1000;
			typing = false;
		} else {
			timeOut = currentChar === "." ? 300 : currentChar === " " ? 60 : 50;
		}
	}

	function switchToNextPhrase() {
		currentCharacterIndex = 0;
		currentPhraseIndex = (currentPhraseIndex + 1) % PHRASES.length;
		typing = true;
		timeOut = 300;
	}

	function removeLastCharacter() {
		currentText.pop();
		DISPLAY.innerText = currentText.join("");
		timeOut = 30;
	}

	writePhrases();

});