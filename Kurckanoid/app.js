document.addEventListener("DOMContentLoaded", () => {

	const BRICK_WIDTH = 42;
	const BRICK_HEIGHT = 40;
	const BRICK_PADDING = 2;

	const HEIGHT = 500;
	const WIDTH = BRICK_PADDING + (BRICK_PADDING + BRICK_WIDTH) * 16;

	const PADDLE_WIDTH = 100;
	const PADDLE_HEIGHT = 20;

	const BALL_RADIUS = 5;
	const BALL_BOX = {
		left: BALL_RADIUS,
		right: WIDTH - BALL_RADIUS,
		top: HEIGHT - BALL_RADIUS,
		bottom: BALL_RADIUS
	}

	const GRID = document.getElementById("grid");
	const LEVEL_SPAN = document.getElementById("level");
	const LIVES_SPAN = document.getElementById("balls");
	const SCORE_SPAN = document.getElementById("score");

	class Brick {
		constructor(x, y, hardness) {
			this.boundingBox = {
				left: x - BALL_RADIUS,
				right: x + BRICK_WIDTH + BALL_RADIUS,
				top: y + BRICK_HEIGHT + BALL_RADIUS,
				bottom: y - BALL_RADIUS
			}
			this.isAlive = true;
			this.hardness = hardness;
			this.value = Math.floor(Math.random() * 20);

			this.visual = document.createElement("div");
			this.visual.className = "brick " + (hardness === 2 ? "primary" : "secondary");
			this.visual.style.left = x + "px";
			this.visual.style.bottom = y + "px";
			this.visual.style.width = BRICK_WIDTH + "px";
			this.visual.style.height = BRICK_HEIGHT + "px";
			GRID.appendChild(this.visual);
		}

		contains(x, y) {
			return this.containsX(x) && this.containsY(y);
		}

		containsX(x) {
			return x >= this.boundingBox.left && x <= this.boundingBox.right;
		}

		containsY(y) {
			return y >= this.boundingBox.bottom && y <= this.boundingBox.top;
		}

		destroy() {
			if (this.hardness <= 1) {
				this.isAlive = false;
				GRID.removeChild(this.visual);
				updateScore(score + this.value);
			} else {
				this.hardness--;
				this.visual.classList.remove("primary");
				this.visual.classList.add("secondary");
				if (Math.random() < 0.05) {
					balls.push(new Ball());
				}
			}
		}
	}

	class Paddle {
		constructor() {
			this.x = (WIDTH - PADDLE_WIDTH) / 2;
			this.y = 4;
			this.reset();

			this.visual = document.createElement("div");
			this.visual.id = "paddle";
			this.visual.style.width = PADDLE_WIDTH + "px";
			this.visual.style.height = PADDLE_HEIGHT + "px";
			this.visual.style.left = this.x + "px";
			this.visual.style.bottom = this.y + "px";
			GRID.appendChild(this.visual);
		}

		contains(x, y) {
			return this.containsX(x) && this.containsY(y);
		}

		containsX(x) {
			return x >= this.x && x <= this.x + PADDLE_WIDTH;
		}

		containsY(y) {
			return y >= this.y && y <= this.y + PADDLE_HEIGHT;
		}

		distanceToMiddle(x) {
			let maxDistance = PADDLE_WIDTH / 2;
			let middle = this.x + maxDistance;
			return (x - middle) / maxDistance;
		}

		reset() {
			this.movingLeft = false;
			this.movingRight = false;
			this.speed = 0.0;
			this.isAlive = true;
		}

		update() {
			if (this.movingLeft && this.speed > -16) {
				this.speed -= 4;
			}
			if (this.movingRight && this.speed < 16) {
				this.speed += 4;
			}
			if (!this.movingLeft && !this.movingRight) {
				this.speed *= 0.75;
			}
			this.x = Math.max(Math.min(this.x + this.speed, WIDTH - PADDLE_WIDTH - 4), 4);
			this.visual.style.left = this.x + "px";
		}
	}

	class Ball {
		constructor() {
			this.x = 50;
			this.y = 50;
			this.hSpeed = 3.0 + level;
			this.vSpeed = 3.0 + level;
			this.isAlive = true;

			this.visual = document.createElement("div");
			this.visual.className = "ball";
			this.visual.style.left = (this.x - BALL_RADIUS) + "px";
			this.visual.style.bottom = (this.y - BALL_RADIUS) + "px";
			this.visual.style.width = (BALL_RADIUS * 2) + "px";
			this.visual.style.height = (BALL_RADIUS * 2) + "px";
			GRID.appendChild(this.visual);
		}

		update(bricks, paddle) {
			if (this.isAlive) {
				let prevX = this.x;
				let prevY = this.y;
				this.x += this.hSpeed;
				this.y += this.vSpeed;

				this.checkCollisionsWithBricks(prevX, prevY, bricks);
				this.checkCollissionWithPaddle(prevX, prevY, paddle);
				this.checkCollisionsWithGrid();

				this.visual.style.left = (this.x - BALL_RADIUS) + "px";
				this.visual.style.bottom = (this.y - BALL_RADIUS) + "px";
			}
		}

		checkCollisionsWithBricks(prevX, prevY, bricks) {
			for (let i = 0; i < bricks.length; i++) {
				let brick = bricks[i];
				if (brick.contains(this.x, this.y)) {
					let containsPrevX = brick.containsX(prevX);
					let containsPrevY = brick.containsY(prevY);
					if (containsPrevX && !containsPrevY) {
						this.vSpeed = -this.vSpeed;
					} else if (containsPrevY && !containsPrevX) {
						this.hSpeed = -this.hSpeed;
					} else {
						this.hSpeed = -this.hSpeed;
						this.vSpeed = -this.vSpeed;
					}
					brick.destroy();
					break;
				}
			}
		}

		checkCollissionWithPaddle(prevX, prevY, paddle) {
			if (paddle.contains(this.x, this.y - BALL_RADIUS)) {
				this.vSpeed = -this.vSpeed;
				let containsPrevX = paddle.containsX(prevX);
				let containsPrevY = paddle.containsY(prevY - BALL_RADIUS);
				if (containsPrevX && !containsPrevY) {
					// Commander Keen style paddle bounce
					this.hSpeed = 4 * paddle.distanceToMiddle(this.x);
				} else {
					this.hSpeed = -this.hSpeed;
				}
			}
		}

		checkCollisionsWithGrid() {
			if (this.x <= BALL_BOX.left) {
				this.hSpeed = -this.hSpeed;
			} else if (this.x >= BALL_BOX.right) {
				this.hSpeed = -this.hSpeed;
			}
			if (this.y >= BALL_BOX.top) {
				this.vSpeed = -this.vSpeed;
			} else if (this.y <= BALL_BOX.bottom) {
				this.destroy();
			}
		}

		destroy() {
			GRID.removeChild(this.visual);
			this.isAlive = false;
		}
	}

	let bricks;
	let paddle;
	let balls;
	let updateTimerId;
	let score;
	let level;
	let lives;

	function restart() {
		updateScore(0);
		level = 0;
		lives = 3;
		startLevel();
	}

	function startLevel() {
		if (updateTimerId) {
			clearInterval(updateTimerId);
		}
		clearGrid();
		createBricks();
		LEVEL_SPAN.innerText = level;
		LIVES_SPAN.innerText = lives;
		paddle = new Paddle();
		balls.push(new Ball());
		updateTimerId = setInterval(update, 20);
	}

	function updateScore(newScore) {
		score = newScore;
		SCORE_SPAN.innerText = score;
	}

	function clearGrid() {
		GRID.style.width = WIDTH + "px";
		GRID.style.height = HEIGHT + "px";
		bricks = [];
		balls = [];
		while (GRID.firstChild) {
			GRID.removeChild(GRID.firstChild);
		}
	}

	function createBricks() {
		let paddedWidth = BRICK_WIDTH + BRICK_PADDING;
		let paddedHeight = BRICK_HEIGHT + BRICK_PADDING;
		let template = LEVELS[level % LEVELS.length];
		for (let j = 0, y = HEIGHT - paddedHeight;
			j < template.length;
			j++, y -= paddedHeight) {
			let row = template[j];
			for (let i = 0, x = BRICK_PADDING;
				i < row.length;
				i++, x += paddedWidth) {
				if (row[i]) {
					bricks.push(new Brick(x, y, row[i]));
				}
			}
		}
	}

	function update() {
		if (balls.length > 0) {
			paddle.update();
			balls.forEach(ball => ball.update(bricks, paddle));
			balls = balls.filter(ball => ball.isAlive);
			bricks = bricks.filter(brick => brick.isAlive);
		} else {
			clearInterval(updateTimerId);
			lives--;
			LIVES_SPAN.innerText = lives;
			if (lives > 0) {
				alert("You lost a ball. Get ready for the next ball!");
				balls.push(new Ball());
				paddle.reset();
				updateTimerId = setInterval(update, 20);
			} else {
				alert("GAME OVER!");
			}
		}
		if (bricks.length === 0) {
			clearInterval(updateTimerId);
			alert("LEVEL CLEARED!");
			level++;
			lives++;
			startLevel();
		}
	}

	const LEFT = 37;
	const RIGHT = 39;
	const R = 82;
	const ENTER = 13;
	
	function controlDown(event) {
		if (event.keyCode === LEFT) {
			paddle.movingLeft = true;
		} else if (event.keyCode === RIGHT) {
			paddle.movingRight = true;
		} else if (event.keyCode === R || event.keyCode === ENTER) {
			restart();
		}
	}
	document.addEventListener("keydown", controlDown);
	
	function controlUp(event) {
		if (event.keyCode === LEFT) {
			paddle.movingLeft = false;
		} else if (event.keyCode === RIGHT) {
			paddle.movingRight = false;
		}
	}
	document.addEventListener("keyup", controlUp);

	restart();

});