document.addEventListener("DOMContentLoaded", () => {

	const HEIGHT = 500;
	const WIDTH = 800;
	const DOPEFISH_HEIGHT = 64;
	const DOPEFISH_WIDTH = 86;
	const MIN_WALL_HEIGHT = 20;
	const VOID_MAX_OFFSET = HEIGHT - MIN_WALL_HEIGHT;
	const VOID_MIN_HEIGHT = DOPEFISH_HEIGHT + 48;
	const VOID_WIDTH = 15;
	const DISPLAY = document.getElementById("display");
	const SCORE_SPAN = document.getElementById("score");
	const HIGHSCORE_SPAN = document.getElementById("highscore");

	class Dopefish {
		constructor() {
			this.x = 20;
			this.y = HEIGHT / 2;
			this.movingUp = false;
			this.vSpeed = 2.0;
			this.isAlive = true;

			this.visual = document.createElement("div");
			this.visual.id = "dopefish";
			this.visual.style.height = DOPEFISH_HEIGHT + "px";
			this.visual.style.width = DOPEFISH_WIDTH + "px";
			this.visual.style.left = this.x + "px";
			this.visual.style.bottom = this.y + "px";
			DISPLAY.appendChild(this.visual);
		}

		update(window, jawbreakers) {
			if (this.vSpeed < 0 && this.movingUp) {
				this.vSpeed = 0.6;
			} else {
				this.vSpeed += this.movingUp ? 0.3 : -0.2;
			}
			this.y += this.vSpeed;
			this.visual.style.bottom = this.y + "px";

			jawbreakers.forEach(jawbreaker => { 
				if (this.x + 12 < jawbreaker.x
					&& this.x + 72 > jawbreaker.x
					&& this.y < jawbreaker.y
					&& this.y + 48 > jawbreaker.y) {
					jawbreaker.destruct();
					score += 200;
					this.visual.style.backgroundImage = "url(images/eating.gif)";
					setTimeout(() => {
						this.visual.style.backgroundImage = "url(images/dopefish.gif)";
					}, 1200);
				}
			});

			if (this.y < window.y || this.y + DOPEFISH_HEIGHT > window.y + window.height) {
				this.isAlive = false;
			}
		}

		destruct() {
			this.isAlive = false;
			DISPLAY.removeChild(this.visual);
		}
	}

	class Slice {
		constructor(x) {
			this.x = x;
			this.y = MIN_WALL_HEIGHT + offset + (Math.random() * 5 - 5);
			let newHeight = voidSize + (Math.random() * 5 - 5);
			this.height = Math.max(VOID_MIN_HEIGHT, newHeight);
			this.isAlive = true;

			this.visual = document.createElement("div");
			this.createTopWall();
			this.createWindow();
			this.createBottomWall();

			this.visual.classList.add("slice");
			this.visual.style.gridTemplateRows = (HEIGHT - this.y - this.height) + "px auto " + this.y + "px";
			this.visual.style.left = this.x + "px";
			this.visual.style.backgroundPositionX = Math.floor(Math.random() * 4) * VOID_WIDTH + "px";
			DISPLAY.appendChild(this.visual);
		}

		createTopWall() {
			let topWall = document.createElement("div");
			topWall.className = "top wall";
			topWall.style.backgroundPositionX = Math.floor(Math.random() * 4) * VOID_WIDTH + "px";
			this.visual.appendChild(topWall);
		}

		createWindow() {
			let window = document.createElement("div");
			window.className = this.getWindowClass();
			this.visual.appendChild(window);
		}

		getWindowClass() {
			let plantSelector = Math.random();
			if (plantSelector < 0.02) {
				return "bottom oneLeaf";
			} else if (plantSelector < 0.04) {
				return "bottom twoLeaf";
			} else if (plantSelector < 0.06) {
				return "top oneLeaf";
			} else if (plantSelector < 0.08) {
				return "top twoLeaf";
			} else {
				return "";
			}
		}

		createBottomWall() {
			let bottomWall = document.createElement("div");
			bottomWall.className = "bottom wall";
			bottomWall.style.backgroundPositionX = Math.floor(Math.random() * 4) * VOID_WIDTH + "px";
			this.visual.appendChild(bottomWall);
		}

		update() {
			this.x -= 5;
			if (this.x < -15) {
				this.destruct();
			} else {
				this.visual.style.left = this.x + "px";
			}
		}

		destruct() {
			this.isAlive = false;
			DISPLAY.removeChild(this.visual);
		}
	}

	class Jawbreaker {
		constructor(window) {
			this.window = window;
			this.x = window.x;
			this.y = 10 + (window.height - 10) * Math.random() + window.y;
			this.isAlive = true;

			this.visual = document.createElement("div");
			this.visual.classList.add("jawbreaker");
			this.visual.style.left = this.x + "px";
			this.visual.style.bottom = this.y + "px";
			DISPLAY.appendChild(this.visual);
		}

		update() {
			this.x = this.window.x;
			if (this.x < -15) {
				this.destruct();
			} else {
				this.visual.style.left = this.x + "px";
			}
		}

		destruct() {
			this.isAlive = false;
			try {
				DISPLAY.removeChild(this.visual);
			} catch (e) {
				// FIXME: when jawbreakers are close together, it either doubly removes the jawbreaker or find another visual to remove.
			}
		}
	}

	let highscore = 0;
	let dopefish;
	let slices = [];
	let jawbreakers = [];
	let updateTimerId;
	let score;
	let voidSize;
	let offset;

	function start() {
		updateTimerId = setInterval(update, 20);
	}

	function populateDisplay() {
		if (updateTimerId) {
			clearInterval(updateTimerId);
		}
		voidSize = HEIGHT - 3 * MIN_WALL_HEIGHT;
		offset = MIN_WALL_HEIGHT / 2;
		DISPLAY.style.width = WIDTH + "px";
		DISPLAY.style.height = HEIGHT + "px";
		jawbreakers.forEach(jawbreaker => jawbreaker.destruct());
		jawbreakers = [];
		createVoids();
		createDopefish();
	}

	function createVoids() {
		slices.forEach(slice => slice.destruct());
		slices = [];
		slices.push(new Slice(0));
		for (let x = 0; x < WIDTH; x += 15) {
			addSlice();
		}
	}

	function createDopefish() {
		if (dopefish) {
			dopefish.destruct();
		}
		score = 0;
		SCORE_SPAN.innerText = score;
		dopefish = new Dopefish();
	}

	function update() {
		if (dopefish.isAlive) {
			updateVoids();
			updateJawbreakers();
			dopefish.update(slices[4], jawbreakers);
		} else {
			clearInterval(updateTimerId);
			if (score > highscore) {
				highscore = score;
				HIGHSCORE_SPAN.innerText = highscore;
			}
			updateTimerId = null;
			alert("You killed Dopefish! :( GAME OVER");
		}
	}

	function updateVoids() {
		slices.forEach(slice => slice.update());
		if (!slices[0].isAlive) {
			slices.shift();
			addSlice();
			score += 2;
			SCORE_SPAN.innerText = score;
		}
	}

	function updateJawbreakers() {
		jawbreakers.forEach(jawbreaker => jawbreaker.update());
		if (jawbreakers.length > 0 && !jawbreakers[0].isAlive) {
			jawbreakers.shift();
		}
	}

	function addSlice() {
		let lastSlice = slices[slices.length - 1];
		let newSlice = new Slice(lastSlice.x + VOID_WIDTH);
		slices.push(newSlice);
		if (Math.random() < 0.02) {
			jawbreakers.push(new Jawbreaker(newSlice));
		}
		if (voidSize > VOID_MIN_HEIGHT) {
			voidSize -= 0.2;
			offset += 0.1;
		}
		offset = Math.min(VOID_MAX_OFFSET - voidSize, offset + 16 * Math.random() - 8);
		if (offset < 0) {
			offset = -offset;
		}
	}

	const UP = 38;
	const R = 82;
	const ENTER = 13;
	
	function controlDown(event) {
		if (event.keyCode === UP && dopefish.isAlive) {
			if (updateTimerId) {
				dopefish.movingUp = true;
			} else {
				start();
			}
		} else if (event.keyCode === R || event.keyCode === ENTER) {
			populateDisplay();
		}
	}
	document.addEventListener("keydown", controlDown);
	
	function controlUp(event) {
		if (event.keyCode === UP && dopefish.isAlive) {
			dopefish.movingUp = false;
		}
	}
	document.addEventListener("keyup", controlUp);

	populateDisplay();

});