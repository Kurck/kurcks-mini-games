/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import utilities.BonusType;
import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.math.ColorRGBA;
import com.jme3.ui.Picture;
import game.Action;
import game.Blob;
import game.Bonus;
import java.util.List;

/**
 *
 * @author kurck
 */
public class LevelUpScreen extends TextCreationNode {

	public static final float WIDTH = 76.0f + Action.BUTTON_WIDTH;
	private static final float HEIGHT = 92.0f + Action.BUTTON_HEIGHT;
	private static final float EXTENDED_HEIGHT = HEIGHT + 57.0f + Achievement.SIZE;
	private static final ColorRGBA TITLE_TEXT_COLOUR = new ColorRGBA(0.0f, 0.0f, 0.0f, 0.85f);

	private BitmapFont guiFont = null;
	private float centreLine = 0f;
	private float y = 0;
	private float height = HEIGHT;

	private Blob blob = null;
	private Bonus bonus = null;
	private boolean actionSwap = false;

	public LevelUpScreen(AssetManager assetManager, Blob player) {
		// Initialisation.
		this.centreLine = WIDTH / 2;
		this.guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		this.blob = player;

		// Get the achievements.
		List<Achievement> achievements = player.getAchievements(assetManager);
		boolean achieved = !achievements.isEmpty();

		// Get the bonus.
		this.bonus = new Bonus(assetManager);
		if (this.bonus.getType() == BonusType.SWAP) {
			this.actionSwap = true;
			Action newAction = new Action(this.blob.getLevel(), Blob.PLAYER_MULTIPLIER, assetManager);
			this.bonus.setAction(newAction);
		}
		this.blob.applyBonus(this.bonus);

		// Show screen.
		this.y = 0.0f;
		addBackground(assetManager, "Interface/overlays/LevelUpScreenBottom.png", 316.0f, 8.0f);
		this.y += 8;
		if (achieved) {
			// Add background.
			addBackground(assetManager, "Interface/overlays/LevelUpScreenCentre.png", 316.0f, 149.0f);

			// Initialise values.
			float halfAchievement = Achievement.SIZE / 2;
			this.y += 28.0f + halfAchievement;
			float score = 0.0f;
			float x = centreLine - (achievements.size() - 1) * (halfAchievement + 2);
			float spacing = 4.0f + Achievement.SIZE;

			// Loop through the achievements to calculate the gained score and to show the achievements.
			for (Achievement achievement : achievements) {
				score += achievement.getScore();
				achievement.setLocalTranslation(x, this.y, 0.0f);
				this.attachChild(achievement);
				x += spacing;
			}

			// Show the gained score.
			this.y -= 24.0f + halfAchievement;
			int roundedScore = (int) Math.round(score);
			String scoreMessage = "Score: +" + roundedScore;
			createText(guiFont, ColorRGBA.White, BitmapFont.Align.Center, centreLine, y, scoreMessage);

			// Show title.
			this.y += 28.0f + Achievement.SIZE;
			String title = "Achievement" + (achievements.size() > 1 ? "s" : "");
			createText(guiFont, ColorRGBA.White, BitmapFont.Align.Center, centreLine, y, title);

			// Update object's properties.
			this.y += 25.0f;
			this.height = EXTENDED_HEIGHT;
		}

		// Show the top of the screen with the bonus.
		addBackground(assetManager, "Interface/overlays/LevelUpScreenTop.png", 316.0f, 164.0f);
		showBonus(this.bonus);

		// Show the title.
		this.y += 28.0f;
		createText(guiFont, TITLE_TEXT_COLOUR, BitmapFont.Align.Center, centreLine, y, "Level up!");
	}

	/**
	 * Adds the image to the node.
	 * @param assetManager	the manager of assets.
	 * @param path the path to the background image.
	 */
	private void addBackground(AssetManager assetManager, String path, float width, float height) {
		Picture background = new Picture("Background");
		background.setImage(assetManager, path, true);
		background.setWidth(width);
		background.setHeight(height);
		background.setPosition(0, this.y);
		this.attachChild(background);
	}

	/**
	 * Replace the current selection with another.
	 * @param playerAction the new selection.
	 */
	public void addSelection(Action playerAction) {
		this.blob.swap(playerAction, this.bonus.getAction());
		this.bonus.setAction(playerAction);
	}

	public float getHeight() { return this.height; }
	public boolean isActionSwap() { return this.actionSwap; }

	private void showBonus(Bonus bonus) {
		// Show the extra information.
		this.y += 4.0f;
		createText(guiFont, ColorRGBA.White, BitmapFont.Align.Center, centreLine, y, bonus.getType().getDescription());
		// Show the new action/bonus.
		this.y += 24.0f;
		float buttonX = this.centreLine - ActionButton.BUTTON_WIDTH / 2;
		bonus.setLocalTranslation(buttonX, this.y, 0);
		this.attachChild(bonus);
		// Show the description.
		this.y += Action.BUTTON_HEIGHT + 4.0f;
		createText(guiFont, ColorRGBA.White, BitmapFont.Align.Center, centreLine, y, bonus.getType().getName());
	}

}
