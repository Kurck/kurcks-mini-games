package gui;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import game.Blob;

/**
 * The collection of GUI components,
 * @author Kurck Gustavson
 */
public class HUD extends Node {

	private Node playerNode;
	private Node opponentNode;

	private ScoreBoard playerBoard;
	private ScoreBoard opponentBoard;

	private Texter texter;
	private Bar playerBar;
	private Bar opponentBar;

	private Picture oppActCentreTop;
	private Picture oppActCentreBottom;
	private Picture oppActBottom;

	public HUD(AssetManager assetManager, float width, float height) {
		// Shared properties.
		BitmapFont font = assetManager.loadFont("Interface/Fonts/Default.fnt");

		// Create the placeholder for the blob details.
		this.playerNode = new Node();
		this.attachChild(this.playerNode);

		// Create the background. (move to individual parts)
		float centreWidth = width - 544.0f;
		attachImageTo(assetManager, "PlayerStatsBack", 8.0f, 8.0f, 0, 240.0f, 132.0f, this.playerNode);
		attachImageTo(assetManager, "PlayerConsoleBack", 256.0f, 8.0f, 0, centreWidth + 32.0f, 138.0f, this.playerNode);

		// Create the health/experience bars that will be in between the background and the overlay.
		this.playerBar = new Bar(assetManager, width, "RedBar", "BlueBar");
		this.playerBar.setLocalTranslation(256.0f, 0.0f, 0.0f);
		this.playerNode.attachChild(this.playerBar);

		// Create text output.
		this.texter = new Texter(font, width);
		this.texter.setLocalTranslation(Blob.RACK_SPACE + 8.0f, 0.0f, 0.0f);
		this.playerNode.attachChild(this.texter);

		// Add the overlay.
		attachImageTo(assetManager, "PlayerStatsFront", 0.0f, 0.0f, 2, 272.0f, 184.0f, this.playerNode);
		attachImageTo(assetManager, "PlayerConsoleFront", 272.0f, 0.0f, 2, centreWidth, 170.0f, this.playerNode);
		attachImageTo(assetManager, "PlayerActionsFront", width - 272.0f, 0.0f, 2, 272.0f, 456.0f, this.playerNode);

		/* OPPONENT */

		// Create the placeholder for the blob details.
		this.opponentNode = new Node();
		this.opponentNode.setLocalTranslation(0.0f, height, 0.0f);
		this.attachChild(this.opponentNode);

		// Create the background.
		attachImageTo(assetManager, "OpponentStatsBack", width - 248.0f, -60.0f, 0, 240.0f, 52.0f, this.opponentNode);
		attachImageTo(assetManager, "OpponentConsoleBack", 264.0f, -20.0f, 0, centreWidth + 16.0f, 12.0f, this.opponentNode);

		// Create the health/experience bars that will be in between the background and the overlay.
		this.opponentBar = new Bar(assetManager, width, "RedBar");
		this.opponentBar.setLocalTranslation(256.0f, -this.opponentBar.getHeight(), 0);
		this.opponentNode.attachChild(this.opponentBar);

		// Add the overlay.
		attachImageTo(assetManager, "OpponentActionsFrontTop", 0.0f, -88.0f, 2, 272.0f, 88.0f, this.opponentNode);
		this.oppActCentreTop = attachImageTo(assetManager, "OpponentActionsFrontCentre", 0.0f, -169.0f, 2, 272.0f, 81.0f, this.opponentNode);
		this.oppActCentreBottom = attachImageTo(assetManager, "OpponentActionsFrontCentre", 0.0f, -250.0f, 2, 272.0f, 81.0f, this.opponentNode);
		this.oppActBottom = attachImageTo(assetManager, "OpponentActionsFrontBottom", 0.0f, 0.0f, 2, 272.0f, 125.0f, this.opponentNode);
		attachImageTo(assetManager, "OpponentConsoleFront", 272.0f, -44.0f, 2, centreWidth, 44.0f, this.opponentNode);
		attachImageTo(assetManager, "OpponentStatsFront", width - 272.0f, -104.0f, 2, 272.0f, 104.0f, this.opponentNode);

		// Create score boards (on top of the HUD overlay)
		this.playerBoard = new ScoreBoard(font, true, "");
		this.playerNode.attachChild(this.playerBoard);
		this.opponentBoard = new ScoreBoard(font, false, "");
		this.opponentBoard.setLocalTranslation(width - ScoreBoard.OPPONENT_BOARD_WIDTH, - ScoreBoard.OPPONENT_BOARD_HEIGHT, 0.0f);
		this.opponentNode.attachChild(this.opponentBoard);
	}

	private Picture attachImageTo(AssetManager assetManager, String name, float x, float y, float z, float width, float height, Node node) {
		Picture image = new Picture(name);
		image.setImage(assetManager, "Interface/overlays/" + name + ".png", true);
		image.setWidth(width);
		image.setHeight(height);
		image.setLocalTranslation(x, y, z);
		node.attachChild(image);
		return image;
	}

	private void organiseOpponentActionsOverlay(int actionCount) {
		// Show/hide the parts in between.
		if (actionCount >= 3) {
			this.opponentNode.attachChild(this.oppActCentreTop);
		} else {
			this.oppActCentreTop.removeFromParent();
		}
		if (actionCount == 4) {
			this.opponentNode.attachChild(this.oppActCentreBottom);
		} else {
			this.oppActCentreBottom.removeFromParent();
		}

		// Position the bottom of the rack.
		float y = -213.0f - 81.0f * (actionCount - 2);
		this.oppActBottom.setLocalTranslation(0.0f, y, 2.0f);
	}

	public void hideOpponent() {
		this.opponentNode.removeFromParent();
	}

	public void showOpponent(Blob blob) {
		this.attachChild(this.opponentNode);
		organiseOpponentActionsOverlay(blob.getActionCount());
	}

	public void updateBlob(Blob blob) {
		if (blob.isPlayer()) {
			this.playerBar.setRatio(blob.getHealthLeft(), 0);
			this.playerBar.setRatio(blob.getExperience(), 1);
		} else {
			this.opponentBar.setRatio(blob.getHealthLeft(), 0);
		}
	}

	public void log(String message, Object... params) {
		this.texter.log(message, params);
	}

	public void log(ColorRGBA colour, String message, Object... params) {
		this.texter.log(colour, message, params);
	}

	public ScoreBoard getScoreBoard(boolean isPlayer) {
		if (isPlayer) {
			return this.playerBoard;
		} else {
			return this.opponentBoard;
		}
	}

}
