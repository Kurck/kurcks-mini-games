package gui;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import java.text.MessageFormat;

/**
 * A class for creating feedback for the player.
 * @author Kurck Gustavson
 */
public class Texter extends TextCreationNode {

	private static final float LINE_SPACING = 18.0f;
	private static final int LINES = 5;
	private BitmapFont font;
	private BitmapText[] texts = new BitmapText[LINES];

	public Texter(BitmapFont hudFont, float appWidth) {
		// Add text.
		this.font = hudFont;
		log(ColorRGBA.White, "Use the keys 0 to 4 to trigger the actions on the right hand side.");
		log(ColorRGBA.White, "When dead, use ''R'' to revive. This will restore your health.");
		log(ColorRGBA.Yellow, "Good luck!");
		log("");
	}

	public final void log(String message, Object... params) {
		log(ColorRGBA.Gray, message, params);
	}

	public final void log(ColorRGBA colour, String message, Object... params) {
		// Remove the item from the graph.
		if (texts[0] != null) {
			texts[0].removeFromParent();
		}

		// Shift all previous messages.
		int lastIndex = texts.length - 1;
		for (int i = 0; i < lastIndex; i++) {
			// Replace the item and shift the new item.
			texts[i] = texts[i + 1];
			if (texts[i] != null) {
				texts[i].move(0.0f, LINE_SPACING, 0.0f);
			}
		}

		// Create and add the new message.
		String formattedMessage = MessageFormat.format(message, params);
		BitmapText text = createText(this.font, colour, BitmapFont.Align.Left, -4.0f, 52.0f, formattedMessage);
		texts[lastIndex] = text;
	}

}
