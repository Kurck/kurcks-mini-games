package gui;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.math.ColorRGBA;
import com.jme3.ui.Picture;
import utilities.AchievementType;

/**
 *
 * @author Kurck Gustavson
 */
public class Achievement extends TextCreationNode {

	public static final float SIZE = 92.0f;
	private static final float INNER_IMAGE_SIZE = 80.0f;

	private float score;

	public Achievement(AssetManager assetManager, AchievementType aType, float ratio, int value) {
		this.score = aType.getScore() * ratio * value * value;

		// Add the images.
		String imagePath = aType.getImagePath();
		if (aType.imageOnly()) {
			centreImage(assetManager, imagePath, SIZE);
		} else {
			centreImage(assetManager, imagePath, INNER_IMAGE_SIZE);
			String ringPath = aType.getRingPath(ratio);
			centreImage(assetManager, ringPath, SIZE);
		}

		// Add label.
		BitmapFont font = assetManager.loadFont("Interface/Fonts/Default.fnt");
		String[] labels = aType.getLabel(value).split("\n");
		ColorRGBA textColour = aType.getTextColour(ratio);
		float y = 9.0f * (labels.length - 2);
		for (String label : labels) {
			createText(font, textColour, BitmapFont.Align.Center, 0.0f, y, label);
			y -= 18.0f;
		}
	}

	/**
	 * Adds the image with its centered at this node.
	 * @param assetManager	the manager of assets.
	 * @param path the path to the background image.
	 * @param size the size of the image (both width and height).
	 */
	private void centreImage(AssetManager assetManager, String path, float size) {
		Picture image = new Picture(path);
		image.setImage(assetManager, path, true);
		image.setWidth(size);
		image.setHeight(size);
		float halfSize = - size / 2;
		image.setLocalTranslation(halfSize, halfSize, 0.0f);
		this.attachChild(image);
	}

	public float getScore() { return this.score; }

}
