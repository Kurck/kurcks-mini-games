/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

/**
 *
 * @author kurck
 */
public class Bar extends Node {

	private static final int BAR_HEIGHT = 12;

	private float width;
	private Picture[] bars = null;

	public Bar(AssetManager assetManager, float appWidth, String barName) {
		width = appWidth - 528.0f;
		bars = new Picture[1];
		bars[0] = createBar(assetManager, 8, barName);
	}

	public Bar(AssetManager assetManager, float appWidth, String barName0, String barName1) {
		width = appWidth - 528.0f;
		// Create the bars.
		bars = new Picture[2];
		bars[0] = createBar(assetManager, 28, barName0);
		bars[1] = createBar(assetManager, 8, barName1);
	}

	private Picture createBar(AssetManager assetManager, float y, String barName) {
		Picture bar = new Picture(barName);
		bar.setImage(assetManager, "Interface/bars/" + barName + ".png", false);
		bar.setWidth(width);
		bar.setHeight(BAR_HEIGHT);
		bar.setLocalTranslation(8, y, 0);
		this.attachChild(bar);
		return bar;
	}

	public float getHeight() {
		return bars.length * (8 + BAR_HEIGHT) + 8;
	}

	public void setRatio(float newRatio, int barNumber) {
		float newWidth = Math.round((float) width * newRatio);
		if (barNumber >= 0 && bars.length > barNumber) {
			bars[barNumber].setWidth(newWidth);
		}
	}

}
