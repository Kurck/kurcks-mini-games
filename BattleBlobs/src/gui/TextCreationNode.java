/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;

/**
 * A node showing 2D text which can be updated.
 * @author kurck
 */
public class TextCreationNode extends Node {

	protected BitmapText createText(BitmapFont guiFont, ColorRGBA colour, BitmapFont.Align alignment, float x, float y, String value) {
		BitmapText text = new BitmapText(guiFont, false);
		text.setSize(guiFont.getCharSet().getRenderedSize());
		text.setText(value);
		text.setColor(colour);
		float offset = calculateOffset(text, alignment);
		text.setLocalTranslation(x + offset, y + text.getLineHeight(), 0);
		this.attachChild(text);
		return text;
	}

	protected void updateText(BitmapText text, BitmapFont.Align alignment, float x, String newText) {
		text.setText(newText);
		float offset = calculateOffset(text, alignment);
		text.setLocalTranslation(x + offset, text.getLocalTranslation().getY(), 0);
	}

	private float calculateOffset(BitmapText text, BitmapFont.Align alignment) {
		switch (alignment) {
			case Center: return -text.getLineWidth() / 2;
			case Right: return -text.getLineWidth();
			default: return 0.0f;
		}
	}

}
