package gui;

import java.util.ArrayList;
import java.util.List;

import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import game.Action;

public class ActionRack extends Node {

	protected static final int RACK_OFFSET_Y = 8;
	protected static final int RACK_OFFSET_X = 8;
	private static final int RACK_SPACING = 8;
	private static final int BUTTON_VERTICAL_SPACING = 1;
	public static final float RACK_SPACE = RACK_OFFSET_X + Action.BUTTON_WIDTH + RACK_SPACING;

	private List<Action> actions = new ArrayList<Action>(5);

	public int getActionCount() { return this.actions.size(); }

	public void addAction(Action action) {
		this.actions.add(action);
	}

	public void addActions(int level, int amount, float multiplier, AssetManager assetManager) {
		for (int i = 0; i < amount; i++) {
			Action action = new Action(level, multiplier, assetManager);
			this.actions.add(action);
		}
	}

	public void displayButtons(int verticalMultiplier) {
		// Display actions.
		int y = 0, dy = verticalMultiplier * (Action.BUTTON_HEIGHT + BUTTON_VERTICAL_SPACING);
		for (Action a : this.actions) {
			a.setLocalTranslation(0, y, 0);
			this.attachChild(a);
			y += dy;
		}
	}

	public void addButtonRackOverlay(AssetManager assetManager, int buttonCount, float rackOffset, boolean isPlayer) {
		Picture rackFront = new Picture("Frontje");
		float bonusWidth = 0.0f;
		float bonusHeight = 0.0f;
		String rackImage;
		if (isPlayer) {
			bonusWidth = 16.0f;
			bonusHeight = 36.0f;
			rackImage = "PlayerActionsFront";
		} else {
			rackImage = "LeftButtonRackFront" + buttonCount;
		}
		rackFront.setImage(assetManager, "Interface/overlays/" + rackImage + ".png", true);
		rackFront.setWidth(Action.BUTTON_WIDTH + RACK_OFFSET_X * 2 + bonusWidth);
		rackFront.setHeight(buttonCount * (Action.BUTTON_HEIGHT + 1) - 1 + RACK_OFFSET_Y * 2 + bonusHeight);
		rackFront.setPosition(-RACK_OFFSET_X - bonusWidth, rackOffset);
		this.attachChild(rackFront);
	}

	/**
	 * Fetches the action from the given slot.
	 * @param slot	the index number of the action.
	 * @return the action, if one corresponds to the slot number.
	 */
	public Action getAction(int slot) {
		if (slot < 0 || slot >= actions.size()) {
			return null;
		} else {
			Action selected = actions.get(slot);
			return selected;
		}
	}

	/**
	 * Fetches the action based on its location.
	 * @param x	the x position of the click.
	 * @param y	the y location of the click.
	 * @return the found action at the clicked location.
	 */
	public Action getAction(float x, float y) {
		for (Action action : this.actions) {
			if (action.inside(x, y)) {
				return action;
			}
		}
		return null;
	}

	/**
	 * Fetches a random action from the set of available actions.
	 * @return the selected action.
	 */
	public Action getRandomAction() {
		return getRandomAction(0);
	}

	public Action getRandomAction(int startIndex) {
		int index = FastMath.nextRandomInt(startIndex, this.actions.size() - 1);
		return getAction(index);
	}

	public void increateAllStrength(float value) {
		for (Action action : this.actions) {
			action.increaseStrength(value);
		}
	}

	public void increaseAllAccuracy(float value) {
		for (Action action : this.actions) {
			action.increaseAccuracy(value);
		}
	}

	/**
	 * Swaps the old action with the new action, replacing each other in the GUI, and the new action also replaces the blob's old action.
	 * @param oldAction	the action of the blob.
	 * @param newAction	the new action to add.
	 */
	public void swap(Action oldAction, Action newAction) {
		// Skip non-existing actions.
		if (oldAction == null) {
			return;
		}

		// Swap translations.
		Vector3f oldLoc = oldAction.getLocalTranslation().clone();
		Vector3f newLoc = newAction.getLocalTranslation().clone();
		oldAction.setLocalTranslation(newLoc);
		newAction.setLocalTranslation(oldLoc);

		// Swap parents.
		int oldIndex = this.getChildIndex(oldAction);
		int newIndex = newAction.getParent().getChildIndex(newAction);
		newAction.getParent().attachChildAt(oldAction, newIndex);
		this.attachChildAt(newAction, oldIndex);

		// Update the list of actions.
		int index = this.actions.indexOf(oldAction);
		if (index < 0) {
			// No such action found.
			return;
		}
		this.actions.set(index, newAction);
	}

}
