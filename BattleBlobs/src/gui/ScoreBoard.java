/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.HashMap;
import java.util.Map;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

/**
 *
 * @author kurck
 */
public class ScoreBoard extends TextCreationNode {

	public static final String NAME = "nameText";
	public static final String LEVEL = "levelText";
	public static final String MAX_HEALTH = "maxHealthText";
	public static final String SCORE = "scoreText";
	public static final String ONE_HIT_WINS = "oneHitWinsText";
	public static final String HITS = "hitsText";
	public static final String CONSECUTIVE_WINS = "consWinsText";

	private static final float LINE_SPACING = 20.0f;
	private static final float RIGHT_LINE = 240.0f;
	public static final float PLAYER_BOARD_WIDTH = 272.0f;
	public static final float PLAYER_BOARD_HEIGHT = 184.0f;
	public static final float OPPONENT_BOARD_WIDTH = 256.0f;
	public static final float OPPONENT_BOARD_HEIGHT = 80.0f;

	private BitmapFont boardFont;
	private float height = 0.0f;

	private Map<String, BitmapText> texts = new HashMap<String, BitmapText>();

	public ScoreBoard(BitmapFont font, boolean isPlayer, String blobName) {
		this.boardFont = font;

		// Show name.
		float nameY = isPlayer ? PLAYER_BOARD_HEIGHT - 40.0f : -4.0f;
		BitmapText nameText = createText(font, ColorRGBA.Black, BitmapFont.Align.Center, 0.0f, nameY, blobName);
		nameText.move(0.0f, 0.0f, 4.0f);
		texts.put(NAME, nameText);

		// Show player-only fields (bottom up).
		this.height = isPlayer ? 15.0f : 27.0f;
		if (isPlayer) {
			createLine(ONE_HIT_WINS, "One-hit wins");
			createLine(CONSECUTIVE_WINS, "Consecutive wins");
			createLine(HITS, "Hits");
			createLine(SCORE, "Score");
		}

		// Show shared fields.
		createLine(MAX_HEALTH, "Maximum health");
		createLine(LEVEL, "Level");
	}

	private void createLine(String textId, String label) {
		createText(boardFont, ColorRGBA.Gray, BitmapFont.Align.Left, 16.0f, this.height, label);
		BitmapText valueText = createText(boardFont, ColorRGBA.Yellow, BitmapFont.Align.Right, RIGHT_LINE, this.height, "0");
		this.texts.put(textId, valueText);
		this.height += LINE_SPACING;
	}

	/**
	 * Updates the value of the specified label.
	 * @param textId the label identifier.
	 * @param newTextValue the new value to display.
	 */
	public void updateText(String textId, Object newTextValue) {
		// Get the actual label.
		BitmapText text = texts.get(textId);
		if (text == null) {
			return;
		}

		// Update text value.
		if (newTextValue instanceof Float) {
			if (MAX_HEALTH.equals(textId)) {
				newTextValue = ((float) newTextValue) * 100;
			}
			newTextValue = (int) Math.round((float) newTextValue);
		}
		String newText = String.valueOf(newTextValue);
		text.setText(newText);

		// Update position.
		Vector3f currentTranslation = text.getLocalTranslation();
		if (NAME.equals(textId)) {
			currentTranslation.setX((OPPONENT_BOARD_WIDTH - text.getLineWidth()) / 2);
		} else {
			currentTranslation.setX(RIGHT_LINE - text.getLineWidth());
		}
		text.setLocalTranslation(currentTranslation);
	}

}
