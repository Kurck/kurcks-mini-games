package gui;

import utilities.BonusType;
import utilities.ActionCategory;
import utilities.ActionType;
import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.ui.Picture;
import game.Action;

/**
 * Represents the displayed elements of an action and bonuses.
 * @author Kurck Gustavson
 */
public class ActionButton extends TextCreationNode {

	// Displaying information.
	public static final int BUTTON_WIDTH = 240;
	public static final int BUTTON_HEIGHT = 80;
	protected static final int ICON_SIZE = 24;
	private static final ColorRGBA BONUS_TEXT_COLOUR = new ColorRGBA(0.0f, 0.0f, 0.0f, 0.85f);

	private AssetManager assetManager;
	private Picture icon, buttonBack;
	private BitmapText nameLabel;
	protected BitmapText strengthLabel;
	protected BitmapText accuracyLabel;

	protected void displayAction(AssetManager assetsManager, String displayName, ActionCategory category, ActionType actionType, float strength, float accuracy) {
		this.assetManager = assetsManager;

		// Add the background image.
		addBackground(assetManager, category.getPath());

		// Load icons.
		displayIcon("Accuracy", 118, assetManager);
		displayIcon(actionType.name(), 9, assetManager);

		// Add labels.
		BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		createText(guiFont, category.getTextColour(), BitmapFont.Align.Center, BUTTON_WIDTH * 0.55f, 50, displayName);
		this.strengthLabel = createText(guiFont, ColorRGBA.White, BitmapFont.Align.Left, 35, 11, getIntifiedString(strength));
		this.accuracyLabel = createText(guiFont, ColorRGBA.White, BitmapFont.Align.Left, 146, 11, getIntifiedString(accuracy));
	}

	protected void displayBonus(AssetManager assetsManager, BonusType bonusType, float value) {
		this.assetManager = assetsManager;

		// Add the background image.
		addBackground(assetManager, bonusType.getPath());

		// Load icon.
		this.icon = displayIcon(bonusType.getIconName(), 9, assetManager);

		// Add labels.
		BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		this.nameLabel = createText(guiFont, BONUS_TEXT_COLOUR, BitmapFont.Align.Center, BUTTON_WIDTH * 0.55f, 50, bonusType.getIconName());
		String labelText = getBonusStrengthLabel(bonusType, value);
		this.strengthLabel = createText(guiFont, ColorRGBA.White, BitmapFont.Align.Left, 35, 11, labelText);
	}

	/**
	 * Adds the background image to the node.
	 * @param assetManager	the manager of assets.
	 * @param path the path to the background image.
	 */
	private void addBackground(AssetManager assetManager, String path) {
		buttonBack = new Picture("ActionBackground");
		buttonBack.setImage(assetManager, path, false);
		buttonBack.setWidth(BUTTON_WIDTH);
		buttonBack.setHeight(BUTTON_HEIGHT);
		this.attachChild(buttonBack);
	}

	/**
	 * Adds a small image to the lower part of the action button.
	 * @param iconName	the name of the icon (and its corresponding asset).
	 * @param x	the x-position of the icon.
	 * @param assetManager	the manager of assets to get the image.
	 */
	private Picture displayIcon(String iconName, int x, AssetManager assetManager) {
		Picture iconPic = new Picture(iconName);
		iconPic.setImage(assetManager, "Interface/icons/" + iconName + ".png", true);
		iconPic.setWidth(ICON_SIZE);
		iconPic.setHeight(ICON_SIZE);
		iconPic.setPosition(x, 9);
		this.attachChild(iconPic);
		return iconPic;
	}

	protected String getIntifiedString(float value) {
		int intified = Math.round(value * 100);
		return Integer.toString(intified);
	}

	/**
	 * Checks whether the given coordinates are within the bounds of this action button.
	 * @param x	the horizontal position.
	 * @param y	the vertical position.
	 * @return	true iff the x and y are within the bounds.
	 */
	public boolean inside(float x, float y) {
		Vector3f worldPos = this.getWorldTranslation();
		float newPosX = x - worldPos.getX();
		boolean withinXBounds = newPosX > 0 && newPosX < BUTTON_WIDTH;
		float newPosY = y - worldPos.getY();
		boolean withinYBounds = newPosY > 0 && newPosY < BUTTON_HEIGHT;
		return withinXBounds && withinYBounds;
	}

	/**
	 * Updates the appearance of the bonus.
	 * @param action	the action the bonus is applied to.
	 * @param bonusType	the type of bonus being applied.
	 * @param increment	the value to display as increment.
	 */
	protected void updateLooks(Action action, BonusType bonusType, float increment) {
		// Update background, name and icon.
		if (action != null) {
			buttonBack.setImage(assetManager, action.getCategory().getPath(), false);
			updateText(nameLabel, BitmapFont.Align.Center, BUTTON_WIDTH * 0.55f, action.getName());

			// Update icon.
			if (bonusType == BonusType.ACTION_STRENGTH) {
				this.icon.removeFromParent();
				String iconName = action.getType().name();
				this.icon = displayIcon(iconName, 9, assetManager);
			}
		}

		// Update the label.
		if (bonusType.isMultiplier()) {
			StringBuilder labelText = new StringBuilder();
			labelText.append('+');
			labelText.append(getIntifiedString(increment));
			this.strengthLabel.setText(labelText.toString());
		}
	}

	private String getBonusStrengthLabel(BonusType bonusType, float value) {
		StringBuilder labelText = new StringBuilder();
		labelText.append('+');
		if (bonusType.isMultiplier()) {
			labelText.append(getIntifiedString(value - 1.0f));
			labelText.append('%');
		} else {
			labelText.append(getIntifiedString(value));
		}
		return labelText.toString();
	}

}
