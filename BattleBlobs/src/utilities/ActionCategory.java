package utilities;

import com.jme3.math.ColorRGBA;

/**
 * The different categories of actions.
 * @author Kurck Gustavson
 */
public enum ActionCategory {

	Earth(new ColorRGBA(0.0f, 0.0f, 0.0f, 0.8f), ColorRGBA.Brown, ColorRGBA.Black),
	Air(new ColorRGBA(0.0f, 0.0f, 0.0f, 0.85f), ColorRGBA.White, ColorRGBA.Cyan),
	Water(new ColorRGBA(0.0f, 0.0f, 0.0f, 0.75f), ColorRGBA.Blue, ColorRGBA.Cyan),
	Fire(new ColorRGBA(0.0f, 0.0f, 0.0f, 0.75f), ColorRGBA.Yellow, ColorRGBA.Red);

	/**
	 * A matrix listing the attack strength against the different action categories.
	 */
	private static final float[][] ATTACK_MULTIPLIERS = {
		{ 1.5f, 1.0f, 1.5f, 1.0f }, // Ground
		{ 1.0f, 1.0f, 2.0f, 0.5f }, // Air
		{ 1.5f, 0.5f, 1.0f, 2.0f }, // Water
		{ 1.0f, 2.0f, 0.5f, 1.0f }  // Fire
	};

	private ColorRGBA textColour;
	private ColorRGBA startColour;
	private ColorRGBA endColour;

	ActionCategory(ColorRGBA textClr, ColorRGBA start, ColorRGBA end) {
		this.textColour = textClr;
		this.startColour = start;
		this.endColour = end;
	}

	/**
	 * Retrieves the relative strength of the attack from this action category to the provided type.
	 * @param defensiveType	the defensive action category.
	 * @return the multiplier for the attack strength.
	 */
	public float getAttackMultiplier(ActionCategory defensiveType) {
		int attOrd = this.ordinal();
		int defOrd = defensiveType.ordinal();
		float multiplier = ATTACK_MULTIPLIERS[attOrd][defOrd];
		return multiplier;
	}

	/**
	 * @return	a path to the corresponding background image for this category.
	 */
	public String getPath() {
		return "Interface/actions/" + this.name() + "Action.jpg";
	}

	public ColorRGBA getTextColour() {
		return this.textColour;
	}

	public ColorRGBA getStartColour() {
		return this.startColour;
	}

	public ColorRGBA getEndColour() {
		return this.endColour;
	}

}
