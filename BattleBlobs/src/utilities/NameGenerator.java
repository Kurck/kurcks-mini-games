/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import utilities.ActionCategory;
import com.jme3.math.FastMath;

/**
 *
 * @author kurck
 */
public class NameGenerator {

	private static final String[] MILD_PREFIX = { "Mild", "Soft", "Little" };
	private static final String[] STRONG_PREFIX = { "Strong", "Heavy" };
	private static final String[] EXTREME_PREFIX = { "Brutal", "Extreme" };
	private static final String[] ATTACK_NAMES = { "Attack", "Force", "Fury" };
	private static final String[] DEFENSE_NAMES = { "Defense", "Shield" };
	private static final String[] BLOB_NAMES = { "Evol", "Bobby", "Bib Bob", "Blobby", "Silly", "Odin", "Youl", "Gully", "Jenny", "Jessy", "Tom", "Jones", "Kim", "Bill", "Victoria", "Lucia", "Dilan", "Pingu", "Calimero", "Quint", "James", "Willy", "Edward", "Roy", "Tanya", "Yuri", "Ursula", "Ike", "Oliver", "Pow", "Angus", "Sarah", "Denise", "Flip", "Grim", "Helen", "Julia", "Kelly", "Lilian", "Zilla", "Xavier", "Condor", "Vivian", "Bradley", "Neo", "Moomoo", "Ringo", "JOD" };

	public static String generateAttackName(ActionCategory category, float strength) {
		return generateName(category, strength, ATTACK_NAMES);
	}

	public static String generateDefenseName(ActionCategory category, float strength) {
		return generateName(category, strength, DEFENSE_NAMES);
	}

	public static String generateName(ActionCategory category, float strength, String[] names) {
		StringBuilder name = new StringBuilder();

		// Add prefix.
		if (strength < 1.0f) {
			name.append(getRandomElement(MILD_PREFIX));
			name.append(" ");
		} else if (strength > 100.0f) {
			name.append(getRandomElement(EXTREME_PREFIX));
			name.append(" ");
		} else if (strength > 25.0f) {
			name.append(getRandomElement(STRONG_PREFIX));
			name.append(" ");
		}

		// Category specific infix.
		name.append(category.toString());
		name.append(" ");

		// Add suffix.
		name.append(getRandomElement(names));

		return name.toString();
	}

	public static String gerenateBlobName() {
		return getRandomElement(BLOB_NAMES);
	}

	/**
	 * Selects a random element from the array.
	 * @param <E>	the type of element of the array and return value.
	 * @param elements	the array.
	 * @return the chosen element.
	 */
	private static <E> E getRandomElement(E[] elements) {
		int index = FastMath.nextRandomInt(0, elements.length - 1);
		return elements[index];
	}

}
