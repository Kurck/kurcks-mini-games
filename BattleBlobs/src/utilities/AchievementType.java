package utilities;

import com.jme3.math.ColorRGBA;
import java.text.MessageFormat;

/**
 *
 * @author Kurck Gustavson
 */
public enum AchievementType {

	CONSECUTIVE_WINS("BackBlue", "{0}\nwins", new int[]{ 5, 10, 25, 50, 75, 100, 250, 500, 1000, 2500 }, 1000.0f, false),
	REACHED_LEVEL("BackRed", "Level\n{0}", new int[]{ 5, 10, 25, 50, 100, 500 }, 1000.0f, false),
	ONE_HIT_WIN("BackDark", "{0}\nOne-hit\nK.O.s", new int[]{ 1, 5, 10, 25, 50 }, 100000.0f, false);

	private static final ColorRGBA GOLD = new ColorRGBA(0.95f, 0.76f, 0.02f, 0.9f);
	private static final ColorRGBA SILVER = new ColorRGBA(0.87f, 0.87f, 0.88f, 0.9f);
	private static final ColorRGBA BRONZE = new ColorRGBA(0.67f, 0.51f, 0.28f, 0.9f);

	private String image;
	private String label;
	private int[] values;
	private float score;
	private boolean imageOnly;

	AchievementType(String imageName, String labelTemplate, int[] vals, float multiplier, boolean oneLayer) {
		this.image = imageName;
		this.label = labelTemplate;
		this.values = vals;
		this.score = multiplier;
		this.imageOnly = oneLayer;
	}

	public String getImagePath() {
		return "Interface/achievements/" + this.image + ".png";
	}

	public String getRingPath(float ratio) {
		String ring = "Bronze";
		if (ratio > 0.68f) {
			ring = "Gold";
		} else if (ratio > 0.34f) {
			ring = "Silver";
		}
		return "Interface/achievements/" + ring + ".png";
	}

	public float getRatio(int value) {
		int index = -2;
		for (int i = 0; i < this.values.length; i++) {
			if (value == this.values[i]) {
				index = i;
				break;
			}
		}
		return (float) (index + 1) / (float) this.values.length;
	}

	public float getScore() { return this.score; }
	public boolean imageOnly() { return this.imageOnly; }

	public ColorRGBA getTextColour(float ratio) {
		if (ratio > 0.68f) {
			return GOLD;
		} else if (ratio > 0.34f) {
			return SILVER;
		} else {
			return BRONZE;
		}
	}

	public String getLabel(int value) {
		return MessageFormat.format(this.label, value);
	}

}
