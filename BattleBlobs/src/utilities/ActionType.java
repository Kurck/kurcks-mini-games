package utilities;

/**
 *
 * @author Kurck Gustavson
 */
public enum ActionType {

	Run,
	Defense,
	Attack;

}
