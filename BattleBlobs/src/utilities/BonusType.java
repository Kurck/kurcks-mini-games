package utilities;

import com.jme3.math.FastMath;

/**
 *
 * @author Kurck Gustavson
 */
public enum BonusType {

	/** Increases the health of the player. */
	HEALTH("Health increased", "Health", 0.1f, new float[]{ 1.02f, 1.04f, 1.08f, 1.16f, 1.32f }),
	/** Increases the strength of a single action. */
	ACTION_STRENGTH("Action strength increased", "Attack", 0.35f, new float[]{ 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 1.9f, 2.0f }),
	/** Increases the accuracy of a single action. */
	ACTION_ACCURACY("Action accuracy increased", "Accuracy", 0.45f, new float[]{ 0.1f, 0.2f, 0.3f }),
	/** Increases the strength of all actions. */
	ALL_ACTIONS_STRENGTH("All actions gain strength", "Attack", 0.55f, new float[]{ 1.05f, 1.1f, 1.15f, 1.2f, 1.25f, 1.3f, 1.35f, 1.4f, 1.45f, 1.5f }),
	/** Increases the accuracy of all actions. */
	ALL_ACTIONS_ACCURACY("All actions gain accuracy", "Accuracy", 0.60f, new float[]{ 0.05f, 0.1f, 0.15f, 0.2f }),
	/** Offers a new action to the user. */
	SWAP("New action unlocked", "Pick an action to swap and hit Enter.", "Attack", 1.0f, new float[]{ 1.0f });

	private String name;
	private String description;
	private String icon;
	private float chance;
	private float[] values;

	BonusType(String bonusName, String iconName, float selectionChance, float[] vals) {
		this(bonusName, "Hit Enter to continue.", iconName, selectionChance, vals);
	}

	BonusType(String bonusName, String bonusDescription, String iconName, float selectionChance, float[] vals) {
		this.name = bonusName;
		this.description = bonusDescription;
		this.icon = iconName;
		this.chance = selectionChance;
		this.values = vals;
	}

	public String getPath() { return "Interface/actions/AllBonus.jpg"; }
	public String getName() { return this.name; }
	public String getDescription() { return this.description; }
	public String getIconName() { return this.icon; }

	public float getRandomValue() {
		int rIndex = FastMath.nextRandomInt(0, this.values.length - 1);
		return this.values[rIndex];
	}

	/**
	 * Selects a random type based on its chance weight.
	 * @return the selected type.
	 */
	public static BonusType getRandomType() {
		float selVal = FastMath.nextRandomFloat();
		for (BonusType type : values()) {
			if (selVal < type.chance) {
				return type;
			}
		}
		return BonusType.SWAP;
	}

	public boolean isMultiplier() {
		return this == HEALTH || this == ACTION_STRENGTH || this == ALL_ACTIONS_STRENGTH;
	}

}
