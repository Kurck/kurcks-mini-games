package game;

import gui.LevelUpScreen;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource.Status;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.RawInputListener;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.event.JoyAxisEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import gui.HUD;

/**
 * The Battle Blobs starter.
 * @author Kurck Gustavson
 */
public class Main extends SimpleApplication implements RawInputListener, ActionListener {

	private Blob player;
	private Blob opponent;
	private HUD hud;
	private LevelUpScreen levelUpScreen = null;
	private boolean cheatMode = false;
	private Arena arena;
	private CameraController camControl;
	private AudioNode music;

    public static void main(String[] args) {
        Main app = new Main();
		AppSettings cfg = new AppSettings(true);
		cfg.setTitle("Battle Blobs");
		cfg.setSettingsDialogImage("Interface/BattleBlobsBanner.png");
		cfg.setFrameRate(60);
		cfg.setVSync(true);
		cfg.setFrequency(60);
		cfg.setResolution(1280, 1024);
		cfg.setFullscreen(false);
		cfg.setSamples(4);
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		app.setSettings(cfg);
		app.setShowSettings(true);
		app.start();
    }

    @Override
    public void simpleInitApp() {
		// Camera control.
		this.flyCam.setEnabled(false);
		this.camControl = new CameraController(this.cam);

		// Add the playground.
		this.arena = new Arena(this.assetManager);
		this.rootNode.attachChild(this.arena);

		// Create the log output.
		this.hud = new HUD(this.assetManager, this.settings.getWidth(), this.settings.getHeight());
		this.guiNode.attachChild(this.hud);

		// Initialise blobs.
		this.player = new Player(this.assetManager, this.hud, this.settings.getWidth());
		this.guiNode.attachChild(this.player);
		attachOpponent();

		// Add keys and audio.
		setupKeys();
		loadAudio();
    }

	private void setupKeys() {
		// Action triggers.
		inputManager.addMapping("0", getTriggers(KeyInput.KEY_0, KeyInput.KEY_NUMPAD0));
		inputManager.addMapping("1", getTriggers(KeyInput.KEY_1, KeyInput.KEY_NUMPAD1));
		inputManager.addMapping("2", getTriggers(KeyInput.KEY_2, KeyInput.KEY_NUMPAD2));
		inputManager.addMapping("3", getTriggers(KeyInput.KEY_3, KeyInput.KEY_NUMPAD3));
		inputManager.addMapping("4", getTriggers(KeyInput.KEY_4, KeyInput.KEY_NUMPAD4));
		// Camera movement.
		inputManager.addMapping("Left", getTriggers(KeyInput.KEY_A, KeyInput.KEY_LEFT));
		inputManager.addMapping("Right", getTriggers(KeyInput.KEY_D, KeyInput.KEY_RIGHT));
		inputManager.addMapping("Forward", getTriggers(KeyInput.KEY_W, KeyInput.KEY_UP));
		inputManager.addMapping("Backward", getTriggers(KeyInput.KEY_S, KeyInput.KEY_DOWN));
		inputManager.addMapping("Up", getTriggers(KeyInput.KEY_Q, KeyInput.KEY_RSHIFT));
		inputManager.addMapping("Down", getTriggers(KeyInput.KEY_Z, KeyInput.KEY_RCONTROL));
		inputManager.addMapping("Switch", new KeyTrigger(KeyInput.KEY_TAB));
		// Game control.
		inputManager.addMapping("Cheats", new KeyTrigger(KeyInput.KEY_SLASH));
		inputManager.addMapping("Defeat", new KeyTrigger(KeyInput.KEY_E));
		inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_R));
		inputManager.addMapping("Enter", getTriggers(KeyInput.KEY_RETURN, KeyInput.KEY_NUMPADENTER));
		inputManager.addMapping("Mute", getTriggers(KeyInput.KEY_M));
		// Set listeners.
		inputManager.addRawInputListener(this);
		inputManager.addListener(this, new String[]{"0", "1", "2", "3", "4", "Cheats", "Defeat", "Reset", "Enter", "Mute"});
		inputManager.addListener(this.camControl, new String[]{"Left", "Right", "Forward", "Backward", "Up", "Down", "Switch"});
	}

	private KeyTrigger[] getTriggers(int... inputs) {
		KeyTrigger[] triggers = new KeyTrigger[inputs.length];
		for (int i = 0; i < inputs.length; i++) {
			triggers[i] = new KeyTrigger(inputs[i]);
		}
		return triggers;
	}

	private void loadAudio() {
		this.music = loadSound(rootNode, "Audio/Kurck_BattleMusicLoopPumped.ogg", true);
		this.music.play();
	}

	private AudioNode loadSound(Node parent, String path, boolean looping) {
		AudioNode sound = new AudioNode(assetManager, path, false);
		sound.setPositional(false);
		sound.setLooping(looping);
		sound.setVolume(2.0F);
		parent.attachChild(sound);
		return sound;
	}

	public void onAction(String name, boolean isPressed, float tpf) {
		// Skip releases.
		if (!isPressed) return;

		// Handle button presses.
		if ("0".equals(name)) {
			invokeAction(0);
		} else if ("1".equals(name)) {
			invokeAction(1);
		} else if ("2".equals(name)) {
			invokeAction(2);
		} else if ("3".equals(name)) {
			invokeAction(3);
		} else if ("4".equals(name)) {
			invokeAction(4);
		} else if ("Cheats".equals(name)) {
			this.cheatMode = !this.cheatMode;
			this.hud.log(ColorRGBA.Magenta, "Cheat mode {0}.", this.cheatMode ? "enabled" : "disabled");
		} else if ("Defeat".equals(name) && this.cheatMode && this.levelUpScreen == null) {
			// Instant death opponent.
			defeat();
		} else if ("Reset".equals(name) && !this.player.isAlive()) {
			this.player.revive();
		} else if ("Enter".equals(name) && this.levelUpScreen != null) {
			this.levelUpScreen.removeFromParent();
			this.levelUpScreen = null;
			attachOpponent();
		} else if ("Mute".equals(name)) {
			Status musicStatus = this.music.getStatus();
			if (musicStatus == Status.Playing) {
				this.music.pause();
			} else {
				this.music.play();
			}
		}
	}

	private void invokeAction(int slot) {
		// Get the action for the given slot of the player.
		Action playerAction = player.getAction(slot);
		invokeAction(playerAction);
	}

	private void invokeAction(float x, float y) {
		// Get the action for the given location.
		Action playerAction = player.getAction(x, y);
		invokeAction(playerAction);
	}

	private void invokeAction(Action playerAction) {
		// Don't act if the player is not alive, if the action is null.
		if (!this.player.isAlive() || playerAction == null) {
			return;
		}

		// If leveling up, the selection of an action indicates a replacement.
		if (this.levelUpScreen != null) {
			if (this.levelUpScreen.isActionSwap()) {
				this.levelUpScreen.addSelection(playerAction);
			}
			return;
		}

		String result = playerAction.trigger();
		this.hud.log("{0} tries {1} and {2}.", this.player.getName(), playerAction.getName(), result);

		// Try to run away. If successful, a new opponent is chosen.
		if (playerAction.isSuccessfulRun()) {
			arena.reset();
			attachOpponent();
			return;
		}

		// Get the action of the opponent.
		Action opponentAction = this.opponent.getRandomAction();
		result = opponentAction.trigger();
		this.hud.log("{0} tries {1} and {2}.", this.opponent.getName(), opponentAction.getName(), result);

		// Show the actions.
		this.arena.showPlayerAction(playerAction);
		this.arena.showOpponentAction(opponentAction);

		// Execute actions.
		if (playerAction.isSuccessfulAttack() && opponentAction.isSuccessfulDefense()) {
			// Player attacks, while the opponents defends.
			attackVersusDefense(this.player, playerAction, this.opponent, opponentAction);
		} else if (playerAction.isSuccessfulDefense() && opponentAction.isSuccessfulAttack()) {
			// Opponents attacks, while the player defends.
			attackVersusDefense(this.opponent, opponentAction, this.player, playerAction);
		} else if (playerAction.isSuccessfulAttack() && opponentAction.isSuccessfulAttack()) {
			// Both attack.
			float playerStrength = playerAction.getStrength();
			this.opponent.doDamage(playerStrength);
			this.player.updateHits();
			float opponentStrength = opponentAction.getStrength();
			this.player.doDamage(opponentStrength);
			this.opponent.updateHits();
		} else if (playerAction.isSuccessfulAttack()) {
			// Only the player attacks.
			float playerStrength = playerAction.getStrength();
			this.opponent.doDamage(playerStrength);
			this.player.updateHits();
		} else if (opponentAction.isSuccessfulAttack()) {
			// Only the opponent attacks.
			float opponentStrength = opponentAction.getStrength();
			this.player.doDamage(opponentStrength);
			this.opponent.updateHits();
		} else {
			// Both miss, or both defend.
			this.hud.log("Nothing happens.");
		}

		// Aftermath.
		if (this.player.isAlive() && !this.opponent.isAlive()) {
			defeat();
		} else if (!this.player.isAlive()) {
			arena.reset();
			this.hud.log(ColorRGBA.Red, "You are out!");
		}
	}

	/**
	 * Executes an attacking and a defending action. It calculates the damage done to both sides using bonuses for certain combinations.
	 * @param attacker	the attacking blob.
	 * @param attack	the	attack action.
	 * @param defender	the defending blob.
	 * @param defense	the defense action.
	 */
	private void attackVersusDefense(Blob attacker, Action attack, Blob defender, Action defense) {
		// Calculate strengths.
		float attackStrength = attack.getStrength();
		float defenseStrength = defense.getStrength();

		// One attacks, while the other defends.
		float strengthMultiplier = attack.getCategory().getAttackMultiplier(defense.getCategory());
		if (strengthMultiplier < 1.0f) {
			this.hud.log("{0}''s attack is not very effective.", attacker.getName());
		} else if (strengthMultiplier > 1.5f) {
			this.hud.log("{0}''s attack is super effective.", attacker.getName());
		} else if (strengthMultiplier > 1.0f) {
			this.hud.log("{0}''s attack is effective.", attacker.getName());
		}
		float damage = strengthMultiplier * attackStrength - defenseStrength;
		if (damage < 0.0f) {
			// Recoil.
			this.hud.log("The attack has been defended successfully. {0} is hit by the recoil.", attacker.getName());
			attacker.doDamage(-damage);
			defender.updateHits();
		} else {
			// Successful attack.
			this.hud.log("{0}''s defense was insufficient.", defender.getName());
			defender.doDamage(damage);
			attacker.updateHits();
		}
	}

	/**
	 * Replace the current opponent by a new one.
	 */
	private void attachOpponent() {
		this.opponent = new Opponent(this.player, this.assetManager, this.hud, this.settings.getHeight(), this.settings.getWidth());
		this.guiNode.attachChild(this.opponent);
		this.hud.showOpponent(this.opponent);
		this.arena.showOpponent();
	}

	public void beginInput() { }
	public void endInput() { }
	public void onJoyAxisEvent(JoyAxisEvent evt) { }
	public void onJoyButtonEvent(JoyButtonEvent evt) { }
	public void onKeyEvent(KeyInputEvent evt) { }
	public void onMouseMotionEvent(MouseMotionEvent evt) { }

	public void onTouchEvent(TouchEvent evt) {
		if (evt.getType() == TouchEvent.Type.TAP) {
			invokeAction(evt.getX(), evt.getY());
		}
	}
	public void onMouseButtonEvent(MouseButtonEvent evt) {
		if (evt.isPressed() && evt.getButtonIndex() == MouseInput.BUTTON_LEFT) {
			invokeAction(evt.getX(), evt.getY());
		}
	}

	@Override
	public void simpleUpdate(float tpf) {
		this.arena.update(tpf);
		this.camControl.update(tpf);
	}

	/**
	 * Handle the death of an opponent and thus a win for the player.
	 */
	private void defeat() {
		boolean leveledUp = this.player.addWinOver(this.opponent);
		this.arena.reset();
		this.arena.hideOpponent();
		this.opponent.removeFromParent();
		if (leveledUp) {
			this.hud.hideOpponent();
			showLevelUp();
		} else {
			attachOpponent();
		}
	}

	private void showLevelUp() {
		// Don't show a new one, if there is already one.
		if (this.levelUpScreen != null) {
			return;
		}

		// Create and add the new screen.
		this.levelUpScreen = new LevelUpScreen(assetManager, player);
		float x = (this.settings.getWidth() - LevelUpScreen.WIDTH) / 2;
		float y = (this.settings.getHeight() - this.levelUpScreen.getHeight()) / 2;
		this.levelUpScreen.setLocalTranslation(x, y, 0);
		this.guiNode.attachChild(this.levelUpScreen);
	}
}
