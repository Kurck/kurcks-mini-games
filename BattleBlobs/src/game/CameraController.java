package game;

import com.jme3.input.controls.ActionListener;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;

/**
 *
 * @author Kurck Gustavson
 */
public class CameraController implements ActionListener {

	private static final float SPEED = 0.2f;
	private static final float MAX_SPEED = 0.4f;
	private static final float FOCUS_FACTOR = 0.1f;
	private static final float SLOWDOWN_FACTOR = 0.3f;
	private static final float TRANSFER_SPEED = 2.5f;

	private static final Vector3f[] CAMERA_POSITIONS = new Vector3f[]{
		new Vector3f(8.5f, 4.5f, 4f),
		new Vector3f(6.5f, 2.8f, 0.0f),
		new Vector3f(0.75f, 21.5f, 0.0f)
	};

	private Camera cam;
	private boolean left = false, right = false;
	private boolean forward = false, backward = false;
	private boolean up = false, down = false;
	private Vector3f currentSpeed = Vector3f.ZERO.clone();
	private final Vector3f lookAt;
	private int currentPosition = -1;
	private Vector3f origin, target;
	private float transfer = 0f;

	public CameraController(Camera camera) {
		this.cam = camera;
		this.lookAt = new Vector3f(-1f, -1f, 0f);
		switchPosition();
	}

	public void onAction(String name, boolean isPressed, float tpf) {
		if ("Left".equals(name)) {
			left = isPressed;
		} else if ("Right".equals(name)) {
			right = isPressed;
		} else if ("Forward".equals(name)) {
			forward = isPressed;
		} else if ("Backward".equals(name)) {
			backward = isPressed;
		} else if ("Up".equals(name)) {
			up = isPressed;
		} else if ("Down".equals(name)) {
			down = isPressed;
		} else if (isPressed && "Switch".equals(name)) {
			switchPosition();
		}
	}

	private void switchPosition() {
		this.currentPosition = (this.currentPosition + 1) % CAMERA_POSITIONS.length;
		this.target = CAMERA_POSITIONS[currentPosition].clone();
		this.origin = this.cam.getLocation().clone();
		this.transfer = 0.0f;
	}

	public void update(float tpf) {
		// Update speed.
		this.currentSpeed.setX(handle(tpf, backward, forward, currentSpeed.getX()));
		this.currentSpeed.setZ(handle(tpf, left, right, currentSpeed.getZ()));
		this.currentSpeed.setY(handle(tpf, up, down, currentSpeed.getY()));

		// Calculate the new position.
		Vector3f newCamPos = Vector3f.ZERO.clone();
		if (target == null) {
			newCamPos = this.cam.getLocation().add(this.currentSpeed);
		} else {
			this.transfer = Math.min(1.0f, this.transfer + tpf * TRANSFER_SPEED);
			this.target.addLocal(this.currentSpeed);
			newCamPos.interpolate(this.origin, this.target, transfer);
			if (this.transfer >= 1.0f) {
				this.target = null;
			}
		}

		// Update camera position.
		this.cam.setLocation(newCamPos);
		this.cam.lookAt(this.lookAt, Vector3f.UNIT_Y);
	}

	private float handle(float tpf, boolean increase, boolean decrease, float value) {
		if (increase && decrease) {
			return value * (1.0f - FOCUS_FACTOR * tpf);
		} else if (increase) {
			return Math.min(MAX_SPEED, value + tpf * SPEED);
		} else if (decrease) {
			return Math.max(-MAX_SPEED, value - tpf * SPEED);
		} else {
			return value * (1.0f - SLOWDOWN_FACTOR * tpf);
		}
	}

}
