package game;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;

import gui.HUD;
import utilities.ActionCategory;
import utilities.ActionType;
import utilities.NameGenerator;

public class Player extends Blob {

	private static final float PLAYER_START_HEALTH = 150.0f;

	/**
	 * Generate a new player blob.
	 */
	public Player(AssetManager assetManager, HUD hud, int appWidth) {
		this.name = NameGenerator.gerenateBlobName();
		this.level = 0;
		Action runAction = new Action("Run", assetManager, ActionCategory.Air, ActionType.Run, 0.5f, 0.0f);
		this.actionRack.addAction(runAction);
		this.actionRack.addActions(this.level, 4, PLAYER_MULTIPLIER, assetManager);
		this.maxHealth = PLAYER_START_HEALTH;
		this.health = this.maxHealth;
		this.hud = hud;
		this.scoreBoard = hud.getScoreBoard(true);
		this.failColour = ColorRGBA.Red;
		this.succColour = ColorRGBA.Yellow;

		// Display actions.
		this.actionRack.setLocalTranslation(appWidth - Action.BUTTON_WIDTH - RACK_OFFSET_X, RACK_OFFSET_Y, 0);
		this.attachChild(this.actionRack);
		this.actionRack.displayButtons(1);

		// Update the displayed statistics.
		this.updateAllStatistics();
	}

	public boolean isPlayer() {
		return true;
	}

}
