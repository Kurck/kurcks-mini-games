package game;

import gui.ActionButton;
import utilities.BonusType;
import com.jme3.asset.AssetManager;

/**
 *
 * @author Kurck Gustavson
 */
public class Bonus extends ActionButton {

	private BonusType type;
	private Action action;
	private float value;

	public Bonus(AssetManager assetManager) {
		this.type = BonusType.getRandomType();
		this.value = this.type.getRandomValue();
		displayBonus(assetManager, this.type, this.value);
	}

	public void setAction(Action newAction) {
		this.action = newAction;
		if (this.action != null && this.action.getParent() != this) {
			this.attachChild(this.action);
		}
	}

	public BonusType getType() { return this.type; }
	public Action getAction() { return this.action; }
	public float getValue() { return this.value; }

	public void updateLooks(Action action) {
		float increment = this.value;
		if (this.type.isMultiplier()) {
			increment = (this.value - 1.0f) * action.getStrength();
		}
		this.updateLooks(action, this.type, increment);
	}

	public void updateLooks(float originalValue) {
		float increment = (this.value - 1.0f) * originalValue;
		this.updateLooks(null, this.type, increment);
	}

}
