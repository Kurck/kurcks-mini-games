package game;

import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;

import gui.HUD;
import utilities.NameGenerator;

public class Opponent extends Blob {

	private static final float OPPONENT_MULTIPLIER = 0.4f;

	/**
	 * Generate a new opponent blob.
	 * @param player the player's blob.
	 */
	public Opponent(Blob player, AssetManager assetManager, HUD hud, int appHeight, int appWidth) {
		this.name = NameGenerator.gerenateBlobName();
		this.level = FastMath.nextRandomInt((int) Math.round((float) player.getLevel() * 0.8f), player.getLevel());
		int actionCount = FastMath.nextRandomInt(2, 4);
		this.actionRack.addActions(this.level, actionCount, OPPONENT_MULTIPLIER, assetManager);
		this.maxHealth = player.maxHealth * (0.5f + FastMath.nextRandomFloat() / 4);
		this.health = this.maxHealth;
		this.hud = hud;
		this.scoreBoard = hud.getScoreBoard(false);
		this.hud.log("A wild {0} has appeared!", this.name);

		// Display actions.
		float rackY = appHeight - RACK_OFFSET_Y - Action.BUTTON_HEIGHT;
		this.actionRack.setLocalTranslation(RACK_OFFSET_X, rackY, 0);
		this.attachChild(this.actionRack);
		this.actionRack.displayButtons(-1);
		//float rackOffset = -(RACK_OFFSET_Y + (1 + Action.BUTTON_HEIGHT) * (actionCount - 1));

		// Update the displayed statistics.
		this.updateAllStatistics();
	}

	public boolean isPlayer() {
		return false;
	}

}
