package game;

import java.util.ArrayList;
import java.util.List;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;

import gui.Achievement;
import gui.ActionRack;
import gui.HUD;
import gui.ScoreBoard;
import utilities.AchievementType;

/**
 * The fighter.
 * @author Kurck Gustavson
 */
public abstract class Blob extends Node {

	private static final float HEALTH_BONUS = 1.1f;
	private static final float HEALTH_LEVEL_UP_RATE = 1.02f;
	private static final float RECOVERY_RATE = 0.25f;

	// Button racks.
	protected static final int RACK_OFFSET_Y = 8;
	protected static final int RACK_OFFSET_X = 8;
	private static final int RACK_SPACING = 8;
	public static final float RACK_SPACE = RACK_OFFSET_X + Action.BUTTON_WIDTH + RACK_SPACING;
	public static final float PLAYER_MULTIPLIER = 1.5f;
	private static final float EXPERIENCE_RATE = 0.95f;

	protected ActionRack actionRack = new ActionRack();
	private boolean alive = true;
	protected int level = 0;
	private float score = 0.0f;
	protected float health = 0.0f;
	protected float maxHealth = 1.0f;
	private float experience = 0.0f;
	protected HUD hud = null;

	// Achievement variables.
	protected ScoreBoard scoreBoard;
	private int highestLevel = 0;
	private int consecutiveWins = 0;
	private int highestWins = 0;
	private int hits = 0;
	private int oneHitWins = 0;
	private int highestOHKOs = 0;

	// Default colours for messages (based on opponent blob).
	protected ColorRGBA failColour = ColorRGBA.Yellow;
	protected ColorRGBA succColour = ColorRGBA.Red;

	/** @return whether the blob is still alive. */
	public boolean isAlive() { return this.alive; }
	/** @return the current level. */
	public int getLevel() { return this.level; }
	/** @return the ratio of health left. */
	public float getHealthLeft() { return this.health / this.maxHealth; }
	/** @return the maximum value of the blob's health. */
	public float getMaxHealth() { return this.maxHealth; }
	/** @return true iff this blob is a player controlled blob. */
	public abstract boolean isPlayer();

	public float getExperience() { return this.experience; }

	public float getScore() { return this.score; }

	public int getActionCount() { return this.actionRack.getActionCount(); }

	public int getHits() { return this.hits; }

	/**
	 * Fetches the action from the given slot.
	 * @param slot	the index number of the action.
	 * @return the action, if one corresponds to the slot number.
	 */
	public Action getAction(int slot) {
		return this.actionRack.getAction(slot);
	}

	/**
	 * Fetches the action based on its location.
	 * @param x	the x position of the click.
	 * @param y	the y location of the click.
	 * @return the found action at the clicked location.
	 */
	public Action getAction(float x, float y) {
		return this.actionRack.getAction(x, y);
	}

	/**
	 * Fetches a random action from the set of available actions.
	 * @return the selected action.
	 */
	public Action getRandomAction() {
		return this.actionRack.getRandomAction();
	}

	/**
	 * Inflicts damage to the blob.
	 * @param damage	the amount of damage.
	 * @return the ratio of health left.
	 */
	public float doDamage(float damage) {
		this.health -= damage;
		hud.log(this.failColour, "{0} was hit.", this.getName());
		if (this.health < 0.0f) {
			this.alive = false;
		}
		this.hud.updateBlob(this);
		return getHealthLeft();
	}

	public void updateHits() {
		this.hits++;
		this.scoreBoard.updateText(ScoreBoard.HITS, this.hits);
	}

	/**
	 * Adds a victory to the blob, increasing its experience, and possibly its level and health.
	 * @param opponent	the defeated blob.
	 * @return true iff the experience pushed the blob to the next level.
	 */
	public boolean addWinOver(Blob opponent) {
		// Update experience and health.
		this.consecutiveWins++;
		this.scoreBoard.updateText(ScoreBoard.CONSECUTIVE_WINS, this.consecutiveWins);
		this.health = Math.min(this.maxHealth, this.health + this.maxHealth * RECOVERY_RATE);
		float gainedExperience = EXPERIENCE_RATE * (float) (1 + opponent.getLevel()) / (float) (1 + this.getLevel());
		hud.log(this.succColour, "{0} defeats {1} and gains {2} experience!", this.name, opponent.getName(), gainedExperience);
		this.experience += gainedExperience;

		// Add bonuses for leveling up.
		boolean leveledUp = false;
		while (this.experience >= 1.0f) {
			this.experience -= 1.0f;
			this.level++;
			this.scoreBoard.updateText(ScoreBoard.LEVEL, this.level);
			leveledUp = true;
			// Health bonus.
			this.maxHealth *= HEALTH_LEVEL_UP_RATE;
			this.scoreBoard.updateText(ScoreBoard.MAX_HEALTH, this.maxHealth);
			this.health = Math.min(this.maxHealth, this.health * HEALTH_BONUS);
			hud.log(ColorRGBA.Yellow, "{0} levels up!", this.getName());
		}

		// Update score and hits.
		this.score += opponent.getMaxHealth();
		this.scoreBoard.updateText(ScoreBoard.SCORE, this.score);
		if (this.hits == 1) {
			this.oneHitWins++;
			this.scoreBoard.updateText(ScoreBoard.ONE_HIT_WINS, this.oneHitWins);
		}
		this.hits = 0;
		this.scoreBoard.updateText(ScoreBoard.HITS, this.hits);

		// Update health bar and experience.
		this.hud.updateBlob(this);

		return leveledUp;
	}

	public void revive() {
		this.alive = true;
		this.score = 0.0f;
		this.consecutiveWins = 0;
		this.health = this.maxHealth;
		updateAllStatistics();
	}

	public void updateAllStatistics() {
		this.hud.updateBlob(this);
		this.scoreBoard.updateText(ScoreBoard.NAME, this.name);
		this.scoreBoard.updateText(ScoreBoard.LEVEL, this.level);
		this.scoreBoard.updateText(ScoreBoard.MAX_HEALTH, this.maxHealth);

		if (isPlayer()) {
			this.scoreBoard.updateText(ScoreBoard.SCORE, this.score);
			this.scoreBoard.updateText(ScoreBoard.ONE_HIT_WINS, this.oneHitWins);
			this.scoreBoard.updateText(ScoreBoard.HITS, this.hits);
			this.scoreBoard.updateText(ScoreBoard.CONSECUTIVE_WINS, this.consecutiveWins);
		}
	}

	/**
	 * Swaps the old action with the new action, replacing each other in the GUI, and the new action also replaces the blob's old action.
	 * @param oldAction	the action of the blob.
	 * @param newAction	the new action to add.
	 */
	public void swap(Action oldAction, Action newAction) {
		this.actionRack.swap(oldAction, newAction);
	}

	/**
	 * Applies the bonus to this blob.
	 * @param bonus	the information about what to update (like health or action strength).
	 */
	public void applyBonus(Bonus bonus) {
		Action currentAction;
		switch(bonus.getType()) {
			case SWAP: return;
			case HEALTH:
				bonus.updateLooks(this.maxHealth);
				this.maxHealth *= bonus.getValue();
				this.scoreBoard.updateText(ScoreBoard.MAX_HEALTH, this.maxHealth);
				this.health *= bonus.getValue();
				this.hud.updateBlob(this);
				break;
			case ACTION_STRENGTH:
				currentAction = this.actionRack.getRandomAction(1);
				bonus.updateLooks(currentAction);
				currentAction.increaseStrength(bonus.getValue());
				break;
			case ACTION_ACCURACY:
				currentAction = this.actionRack.getRandomAction(0);
				bonus.updateLooks(currentAction);
				currentAction.increaseAccuracy(bonus.getValue());
				break;
			case ALL_ACTIONS_STRENGTH:
				this.actionRack.increateAllStrength(bonus.getValue());
				break;
			case ALL_ACTIONS_ACCURACY:
				this.actionRack.increaseAllAccuracy(bonus.getValue());
				break;
		}
	}

	/**
	 * A method for gathering achievements when the player just leveled up.
	 * @param assetManager	the manager of assets.
	 * @return	a list of achievements.
	 */
	public List<Achievement> getAchievements(AssetManager assetManager) {
		List<Achievement> achievements = new ArrayList<Achievement>();

		// Consecutive wins.
		this.highestWins = testAchievement(assetManager, achievements, AchievementType.CONSECUTIVE_WINS, consecutiveWins, highestWins);
		// Reached level.
		this.highestLevel = testAchievement(assetManager, achievements, AchievementType.REACHED_LEVEL, level, highestLevel);
		// One-hit knock out.
		this.highestOHKOs = testAchievement(assetManager, achievements, AchievementType.ONE_HIT_WIN, oneHitWins, highestOHKOs);

		return achievements;
	}


	/**
	 * Tests the given value for the achievement type. If successful, a new achievement is added to the list.
	 * @param assetManager	the manager of assets (required for creating achievement GUI objects).
	 * @param achievements	a list of achievements.
	 * @param achievementType	the type of achievement to test for.
	 * @param value the tested value.
	 * @param highest the current highest value.
	 * @return the new highest value found.
	 */
	private int testAchievement(AssetManager assetManager, List<Achievement> achievements, AchievementType achievementType, int value, int highest) {
		if (value <= highest) {
			return highest;
		}
		float ratio = achievementType.getRatio(value);
		if (ratio >= 0.0f) {
			Achievement achievement = new Achievement(assetManager, achievementType, ratio, value);
			this.score += achievement.getScore();
			this.scoreBoard.updateText(ScoreBoard.SCORE, this.score);
			achievements.add(achievement);
		}
		return value;
	}

}
