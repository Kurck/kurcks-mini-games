package game;

import java.util.HashMap;
import java.util.Map;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

import utilities.ActionCategory;
import utilities.ActionType;

/**
 *
 * @author Kurck Gustavson
 */
public class Arena extends Node {

	private static final float BLOB_FLOOR = 0.25f;
	private static final float ATTACK_FROM_CENTRE = 2.5f;
	private static final float BLOB_FROM_CENTRE = 3.0f;
	private static final float ACTION_OFFSET = 0.3f;

	private static final Vector3f ATTACK_VELOCITY = new Vector3f(10.0f, 0.0f, 0.0f);
	private static final Vector3f DEFENSE_VELOCITY = Vector3f.ZERO.clone();

	// TODO: create separate objects that are properly controllable...
	private Node playerPosition;
	private ParticleEmitter playerEffect;
	private float playerEffectTimeLeft = 0.0f;
	private Spatial playerModel;
	private float playerScale = 1.0f;
	private float playerScalar = 0.0f;
	private int playerScaleDir = -1;
	private ActionType playerMode = null;
	private AudioNode playerEffectSound = null;
	private Map<ActionCategory, AudioNode> playerEffectSounds = null;

	private Node opponentPosition;
	private ParticleEmitter opponentEffect;
	private float opponentEffectTimeLeft = 0.0f;
	private Spatial opponentModel;
	private float opponentScale = 1.0f;
	private float opponentScalar = 0.0f;
	private int opponentScaleDir = -1;
	private ActionType opponentMode = null;
	private AudioNode opponentEffectSound = null;
	private Map<ActionCategory, AudioNode> opponentEffectSounds = null;

	private Spatial[] models;

	public Arena(AssetManager assetManager) {
		// Load a lamp.
		PointLight lamp = new PointLight();
		lamp.setPosition(new Vector3f(0f, 5f, 0f));
		lamp.setColor(ColorRGBA.White);
		this.addLight(lamp);

		// Load the arena.
		Spatial arenaModel = assetManager.loadModel("Scenes/arena.j3o");
		this.attachChild(arenaModel);

		// Load position nodes.
		this.playerPosition = attachNodeAt(new Vector3f(BLOB_FROM_CENTRE, BLOB_FLOOR, 0.0f));
		this.opponentPosition = attachNodeAt(new Vector3f(-BLOB_FROM_CENTRE, BLOB_FLOOR, 0.0f));

		// Load audio.
		this.playerEffectSounds = loadSounds(assetManager, this.playerPosition);
		this.opponentEffectSounds = loadSounds(assetManager, this.opponentPosition);

		// Load models.
		models = new Spatial[3];
		models[0] = assetManager.loadModel("Models/BlueBlob.j3o");
		models[1] = assetManager.loadModel("Models/WarriorBlob.j3o");
		models[2] = assetManager.loadModel("Models/JellyBlob.j3o");

		// Attach models.
		this.playerScale = 0.25f;
		this.playerModel = attachBox(this.playerPosition, this.playerScale, true);

		// Load effects.
		Node playerActionPosition = attachNodeAt(new Vector3f(ATTACK_FROM_CENTRE, BLOB_FLOOR + ACTION_OFFSET, 0.0f));
		this.playerEffect = createParticleEffect(assetManager, playerActionPosition);
		Node opponentActionPosition = attachNodeAt(new Vector3f(-ATTACK_FROM_CENTRE, BLOB_FLOOR + ACTION_OFFSET, 0.0f));
		this.opponentEffect = createParticleEffect(assetManager, opponentActionPosition);
	}

	private Map<ActionCategory, AudioNode> loadSounds(AssetManager assetManager, Node audioPosition) {
		Map<ActionCategory, AudioNode> effectSounds = new HashMap<ActionCategory, AudioNode>();
		effectSounds.put(ActionCategory.Fire, loadSound(assetManager, audioPosition, "Audio/166542__nic3-one__constant-spray-fire-1.ogg"));
		effectSounds.put(ActionCategory.Air, loadSound(assetManager, audioPosition, "Audio/33943__scarbelly25__flamewind.ogg"));
		effectSounds.put(ActionCategory.Water, loadSound(assetManager, audioPosition, "Audio/Kurck_WaterStream.ogg"));
		effectSounds.put(ActionCategory.Earth, loadSound(assetManager, audioPosition, "Audio/Kurck_RoughSurface.ogg"));
		return effectSounds;
	}

	private AudioNode loadSound(AssetManager assetManager, Node parent, String path) {
		AudioNode sound = new AudioNode(assetManager, path, false);
		sound.setPositional(false);
		sound.setLooping(false);
		sound.setVolume(1.5F);
		parent.attachChild(sound);
		return sound;
	}

	private Spatial attachBox(Node node, float size, boolean flip) {
		int randomIndex = FastMath.nextRandomInt(0, models.length - 1);
		Spatial blobModel = models[randomIndex].clone();
		blobModel.scale(size);
		if (flip) {
			blobModel.rotate(0.0f, FastMath.PI, 0.0f);
		}
		node.attachChild(blobModel);
		return blobModel;
	}

	private Node attachNodeAt(Vector3f location) {
		Node node = new Node();
		node.setLocalTranslation(location);
		this.attachChild(node);
		return node;
	}

	private ParticleEmitter createParticleEffect(AssetManager assetManager, Node parent) {
		ParticleEmitter effect = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 60);
		Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		material.setTexture("Texture", assetManager.loadTexture("Textures/flame.png"));
		effect.setMaterial(material);
		effect.setImagesX(2);
		effect.setImagesY(2);
		effect.setEndColor(ColorRGBA.Red);
		effect.setStartColor(ColorRGBA.Yellow);
		effect.getParticleInfluencer().setInitialVelocity(Vector3f.ZERO.clone());
		effect.setStartSize(0.3f);
		effect.setEndSize(0.2f);
		effect.setGravity(0f, 0.01f, 0f);
		effect.setLowLife(0.5f);
		effect.setHighLife(1.0f);
		effect.getParticleInfluencer().setVelocityVariation(0.05f);
		effect.setEnabled(false);
		parent.attachChild(effect);
		return effect;
	}

	public void showPlayerAction(Action action) {
		this.playerEffectSound = this.playerEffectSounds.get(action.getCategory());
		showAction(this.playerEffect, this.playerEffectSound, action, true);
		this.playerMode = action.getType();
		this.playerEffectTimeLeft = action.getDuration();
	}

	public void showOpponentAction(Action action) {
		this.opponentEffectSound = this.opponentEffectSounds.get(action.getCategory());
		showAction(this.opponentEffect, this.opponentEffectSound, action, false);
		this.opponentMode = action.getType();
		this.opponentEffectTimeLeft = action.getDuration();
	}

	private void showAction(ParticleEmitter effect, AudioNode effectSound, Action action, boolean flip) {
		// Hide failed actions.
		if (!action.isSuccessful() || ActionType.Run == action.getType()) {
			disableEffect(effect, effectSound);
			return;
		}

		// Show the effect as blast or shield.
		if (action.getType() == ActionType.Attack) {
			Vector3f velocity = ATTACK_VELOCITY.clone();
			if (flip) {
				velocity.multLocal(-1);
			}
			effect.getParticleInfluencer().setInitialVelocity(velocity);
			if (effectSound != null) {
				effectSound.play();
			}
		} else {
			effect.getParticleInfluencer().setInitialVelocity(DEFENSE_VELOCITY);
		}

		// Update colours of the effect.
		ActionCategory category = action.getCategory();
		effect.setStartColor(category.getStartColour());
		effect.setEndColor(category.getEndColour());
		effect.setParticlesPerSec(100.0f);
		effect.setEnabled(true);
	}

	public void update(float tpf) {
		playerEffectTimeLeft = updateEffect(playerEffect, playerEffectSound, playerEffectTimeLeft, tpf);

		if (playerEffectTimeLeft < 0.0f)
			playerMode = null;
		if (playerScalar > 0.005f)
			playerScaleDir = -1;
		else if (playerScalar < -0.005f)
			playerScaleDir = 1;
		float playerFlux = playerMode == ActionType.Attack ? 0.05f : playerMode == ActionType.Defense ? 0.001f : 0.01f;
		playerScalar += playerFlux * tpf * playerScaleDir;
		playerModel.setLocalScale(playerScale + playerScalar);

		opponentEffectTimeLeft = updateEffect(opponentEffect, opponentEffectSound, opponentEffectTimeLeft, tpf);

		if (opponentEffectTimeLeft < 0.0f)
			opponentMode = null;
		if (opponentScalar > 0.005f)
			opponentScaleDir = -1;
		else if (opponentScalar < -0.005f)
			opponentScaleDir = 1;
		float opponentFlux = opponentMode == ActionType.Attack ? 0.05f : opponentMode == ActionType.Defense ? 0.001f : 0.01f;
		opponentScalar += opponentFlux * tpf * opponentScaleDir;
		opponentModel.setLocalScale(opponentScale + opponentScalar);
	}

	private float updateEffect(ParticleEmitter effect, AudioNode effectSound, float timeLeft, float tpf) {
		if (timeLeft > 0.0f) {
			timeLeft -= tpf;
			if (timeLeft <= 0.0f) {
				effect.setParticlesPerSec(0);
				if (effectSound != null) {
					effectSound.stop();
				}
			}
		} else {
			if (effect.getNumVisibleParticles() == 0) {
				disableEffect(effect, effectSound);
			}
		}
		return timeLeft;
	}

	/**
	 * Stops all running effects.
	 */
	public void reset() {
		disableEffect(playerEffect, playerEffectSound);
		disableEffect(opponentEffect, opponentEffectSound);
	}

	private void disableEffect(ParticleEmitter effect, AudioNode effectSound) {
		if (effect != null) {
			effect.killAllParticles();
			effect.setEnabled(false);
		}
		if (effectSound != null) {
			effectSound.stop();
		}
	}

	public void hideOpponent() {
		this.opponentModel.removeFromParent();
	}

	public void showOpponent() {
		this.opponentScale = 0.20f + 0.1f * FastMath.nextRandomFloat();
		this.opponentModel = attachBox(this.opponentPosition, this.opponentScale, false);
	}
}
