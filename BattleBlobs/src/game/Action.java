package game;

import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;

import gui.ActionButton;
import utilities.ActionCategory;
import utilities.ActionType;
import utilities.NameGenerator;

/**
 * Represents action buttons.
 * @author Kurck Gustavson
 */
public class Action extends ActionButton {

	private static final float ATTACK_TYPE_CHANCE = 0.55f;
	private static final int STRENGTH_MULTIPLIER = 10;

	private ActionCategory category;
	private ActionType type;
	private float accuracy;
	private float strength;
	private float duration = 2.0f;
	private boolean successful;

	/**
	 * Generates a random action.
	 * @param level	the level of experience to base the action on.
	 * @param multiplier a value to weigh the strength calculation.
	 * @param assetManager the manager to retrieve the images and font.
	 */
	public Action(int level, float multiplier, AssetManager assetManager) {
		this.category = getRandomElement(ActionCategory.values());
		this.accuracy = Math.min(1.0f, 1.5f * FastMath.nextRandomFloat());
		this.strength = multiplier * STRENGTH_MULTIPLIER * (level + 1) * FastMath.nextRandomFloat();
		if (FastMath.nextRandomFloat() < ATTACK_TYPE_CHANCE) {
			this.type = ActionType.Attack;
			this.duration = 0.5f + FastMath.nextRandomFloat();
			this.name = NameGenerator.generateAttackName(this.category, this.strength);
		} else {
			this.type = ActionType.Defense;
			this.name = NameGenerator.generateDefenseName(this.category, this.strength);
		}
		displayAction(assetManager, this.name, this.category, this.type, this.strength, this.accuracy);
	}

	/**
	 * Creates a new action.
	 * @param title	the name of the action.
	 * @param actionCategory	the category the action is in.
	 * @param actionType	the type of action.
	 * @param precision	the accuracy of the action.
	 * @param value	the strength of the action.
	 */
	public Action(String title, AssetManager assetManager, ActionCategory actionCategory, ActionType actionType, float precision, float value) {
		this.name = title;
		this.category = actionCategory;
		this.type = actionType;
		this.accuracy = precision;
		this.strength = value;
		displayAction(assetManager, this.name, this.category, this.type, this.strength, this.accuracy);
	}

	/**
	 * Selects a random element from the array.
	 * @param <E>	the type of element of the array and return value.
	 * @param elements	the array.
	 * @return the chosen element.
	 */
	private <E> E getRandomElement(E[] elements) {
		int index = FastMath.nextRandomInt(0, elements.length - 1);
		return elements[index];
	}

	public ActionCategory getCategory() { return this.category; }
	public ActionType getType() { return this.type; }
	public float getStrength() { return this.strength; }
	public float getDuration() { return this.duration; }

	/**
	 * Signal of a try for this action. Updates the successful flag.
	 * @return a textual feedback of the resulting value.
	 */
	public String trigger() {
		this.successful = FastMath.nextRandomFloat() < this.accuracy;
		return this.successful ? "succeeds" : "fails";
	}

	public boolean isSuccessfulAttack() { return isSuccessful(ActionType.Attack); }
	public boolean isSuccessfulDefense() { return isSuccessful(ActionType.Defense); }
	public boolean isSuccessfulRun() { return isSuccessful(ActionType.Run); }
	private boolean isSuccessful(ActionType actionType) { return this.successful && this.type == actionType; }

	/**
	 * Multiplies the current strength value with the given value.
	 * @param value		the multiplier.
	 */
	public void increaseStrength(float value) {
		this.strength *= value;
		this.strengthLabel.setText(getIntifiedString(this.strength));
	}

	/**
	 * Adds the given value to the current accuracy value, up to 1.0f.
	 * @param value the increment in accuracy.
	 */
	public void increaseAccuracy(float value) {
		this.accuracy = Math.min(1.0f, this.accuracy + value);
		this.accuracyLabel.setText(getIntifiedString(this.accuracy));
	}

	public boolean isSuccessful() {
		return this.successful;
	}

}
