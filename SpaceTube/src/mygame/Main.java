package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.FogFilter;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Space Tube Game.
 *
 * @author Kurck Gustavson
 */
public class Main extends SimpleApplication implements ActionListener {

	// General settings.
	private static final int TUBE_SIZE = 5;
	private static final int BULLET_SCORE = -2;
	private static final int OBSTACLE_SCORE = 4;
	private int score = 0;
	private float gameSpeed = 30f;
	private boolean bloomEnabled = false;
	private int level = 0;
	// Bullet characteristics.
	private static final float BULLET_SPEED = 65f;
	private static final float MAX_BULLET_DEPTH = -150f;
	private static final Box BULLET_BOX = new Box(0.02f, 0.02f, 0.4f);
	private static Material bulletMaterial;
	// Rotation speed constants.
	private static final float ROTATION_KICK = 200f;
	private static final float MAX_ROTATION_SPEED = 800f;
	private static final int ROTATION_SLOW_DOWN_FACTOR = 10;
	// Rotation state.
	private float rotationSpeed = 0f;
	private boolean rotateLeft = false;
	private boolean rotateRight = false;
	// Obstacle properties.
	private static final float SPAWN_DEPTH = 2 * MAX_BULLET_DEPTH;
	private float spawnChance = 0.5f;
	private static final float COLLISION_DISTANCE = 1.0f;
	private static Material obstacleMaterial;
	private Spatial[] obstacleDefaults;
	// Scene objects.
	private Node spaceCentre, camNode;
	private Spatial defaultRing;
	private Spatial spaceCraft;
	private List<Spatial> obstacles;
	private List<Spatial> bullets;
	private BitmapText scoreText;
	private BitmapText levelText;
	// Rings.
	private float elapsedTime = 15f;
	private float lastRing = 0f;
	private int ringCount = 0;

	public static void main(String[] args) {
		// Set-up game.
		Main app = new Main();
		AppSettings cfg = new AppSettings(true);

		// Change appearance.
		cfg.setTitle("Space Whale");
		cfg.setSettingsDialogImage("Interface/spaceWhaleBanner.png");
		try {
			cfg.setIcons(new BufferedImage[]{ImageIO.read(new File("assets/Interface/spaceWhale.gif"))});
		} catch (IOException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.WARNING, "Icon missing.", ex);
		}

		// Display settings.
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		cfg.setFrameRate(60);
		cfg.setVSync(true);
		cfg.setFrequency(60);
		cfg.setResolution(1280, 1024);
		cfg.setFullscreen(false);
		cfg.setSamples(2);
		//app.setShowSettings(false);

		// Apply settings and start.
		app.setSettings(cfg);
		app.start();
	}

	@Override
	public void simpleInitApp() {
		// Set-up materials.
		bulletMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		bulletMaterial.setColor("Color", ColorRGBA.Red);
		obstacleMaterial = new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md");

		// Initialise scene objects.
		spaceCentre = new Node();
		camNode = new Node();
		obstacles = new ArrayList<Spatial>();
		bullets = new ArrayList<Spatial>();
		defaultRing = assetManager.loadModel("Models/ring.j3o");

		// Create obstacles.
		obstacleDefaults = new Spatial[3];
		obstacleDefaults[0] = getObstacleModel("obstacleTeeth");
		obstacleDefaults[1] = getObstacleModel("obstaclePyramids");
		obstacleDefaults[2] = getObstacleModel("obstacleWall");

		// Create the space craft.
		spaceCraft = assetManager.loadModel("Models/spacePlane.j3o");
		Material defaultMat = new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md");
		spaceCraft.setMaterial(defaultMat);
		spaceCraft.setLocalScale(0.1f);
		spaceCraft.rotate(0, FastMath.HALF_PI, 0);
		spaceCraft.setLocalTranslation(0, -TUBE_SIZE, 0);

		// Set-up the scene connection.
		spaceCentre.attachChild(spaceCraft);
		spaceCentre.attachChild(camNode);
		camNode.setLocalTranslation(0, 0, 14f);
		rootNode.attachChild(spaceCentre);

		// Write text on the screen (HUD)
		guiNode.detachAllChildren();
		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		createText(15, 15, "Score:");
		scoreText = createText(70, 15, "0");
		updateScore(16);
		createText(15, 35, "Level:");
		levelText = createText(70, 35, "0");

		// Set-up key configuration.
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_LEFT));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_RIGHT));
		inputManager.addMapping("Shoot", new KeyTrigger(KeyInput.KEY_SPACE));
		inputManager.addListener(this, "Left", "Right", "Shoot");

		// Set-up camera.
		flyCam.setEnabled(false);
		// This doesn't seem to help :S
		inputManager.setCursorVisible(false);

		// Add bloom.
		if (bloomEnabled) {
			FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
			BloomFilter bloom = new BloomFilter();
			fpp.addFilter(bloom);
			viewPort.addProcessor(fpp);
		}

		// Add fog to a scene.
		FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
		FogFilter fog = new FogFilter();
		fog.setFogColor(new ColorRGBA(0f, 0f, 0f, 1.0f));
		fog.setFogDistance(100);
		fog.setFogDensity(2.0f);
		fpp.addFilter(fog);
		viewPort.addProcessor(fpp);
	}

	private BitmapText createText(int x, int y, String value) {
		BitmapText text = new BitmapText(guiFont, false);
		text.setSize(guiFont.getCharSet().getRenderedSize());
		text.setText(value);
		text.setLocalTranslation(x, y + text.getLineHeight(), 0);
		guiNode.attachChild(text);
		return text;
	}

	private Spatial getObstacleModel(String modelName) {
		Spatial model = assetManager.loadModel("Models/" + modelName + ".j3o");
		model.setMaterial(obstacleMaterial);
		model.setLocalTranslation(0, -TUBE_SIZE, SPAWN_DEPTH);
		return model;
	}

	@Override
	public void onAction(String name, boolean keyPressed, float tpf) {
		if ("Right".equals(name)) {
			rotateRight = keyPressed;
		} else if ("Left".equals(name)) {
			rotateLeft = keyPressed;
		} else if ("Shoot".equals(name) && keyPressed) {
			createBullet();
		}
	}

	@Override
	public void simpleUpdate(float tpf) {
		spaceCentre.move(0, 0, -tpf * gameSpeed);
		cam.setLocation(camNode.getWorldTranslation());
		updateBullets(tpf);
		updateObstacles(tpf);
		updateRotation(tpf);
		createRing(tpf);
	}

	private void updateBullets(float tpf) {
		// Update the locations of all bullets and remove them if needed.
		List<Spatial> deletables = new ArrayList<Spatial>();
		float bulletMovement = -tpf * BULLET_SPEED;
		float bulletOutOfSight = spaceCentre.getLocalTranslation().getZ() + MAX_BULLET_DEPTH;
		for (Spatial bullet : bullets) {
			bullet.move(0, 0, bulletMovement);
			if (bullet.getLocalTranslation().getZ() < bulletOutOfSight) {
				// Test depth.
				deletables.add(bullet);
			} else {
				// Test collissions
				Spatial collider = checkCollision(bullet);
				if (collider != null) {
					removeGeometry(obstacles, collider);
					deletables.add(bullet);
					//createExplosion(collider.getLocalTranslation());
					updateScore(OBSTACLE_SCORE);
				}
			}
		}

		// Remove the bullets that are too far.
		for (Spatial deletable : deletables) {
			removeGeometry(bullets, deletable);
		}
	}

	private void createExplosion(Vector3f location) {
		ParticleEmitter debrisEffect = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 10);
		Material debrisMat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		debrisMat.setTexture("Texture", assetManager.loadTexture("Textures/Debris.png"));
		debrisEffect.setMaterial(debrisMat);
		debrisEffect.setImagesX(3);
		debrisEffect.setImagesY(3);
		debrisEffect.setRotateSpeed(4);
		debrisEffect.setSelectRandomImage(true);
		debrisEffect.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 4, 0));
		debrisEffect.setStartColor(new ColorRGBA(1f, 1f, 1f, 1f));
		debrisEffect.setGravity(0f, 6f, 0f);
		debrisEffect.getParticleInfluencer().setVelocityVariation(.60f);
		debrisEffect.setLocalTranslation(location);
		rootNode.attachChild(debrisEffect);
		debrisEffect.emitAllParticles();
		obstacles.add(debrisEffect);
	}

	private void removeGeometry(List<Spatial> geometries, Spatial deletable) {
		geometries.remove(deletable);
		deletable.removeFromParent();
	}

	private Spatial checkCollision(Spatial collider) {
		for (Spatial obstacle : obstacles) {
			float d = collider.getLocalTranslation().distanceSquared(obstacle.getLocalTranslation());
			if (d < COLLISION_DISTANCE) {
				return obstacle;
			}
		}
		return null;
	}

	private void updateObstacles(float tpf) {
		List<Spatial> deletables = new ArrayList<Spatial>();
		float cutoff = spaceCentre.getLocalTranslation().getZ() + 5f;
		for (Spatial obstacle : obstacles) {
			// TODO: collisions
			if (obstacle.getLocalTranslation().getZ() > cutoff) {
				deletables.add(obstacle);
			}
		}
		for (Spatial deletable : deletables) {
			removeGeometry(obstacles, deletable);
		}
	}

	/**
	 * Updates the rotation of the space craft based on the pressed buttons and the elapsed time.
	 *
	 * @param tpf the amount of time elapsed.
	 */
	private void updateRotation(float tpf) {
		// Rotation change factors.
		float rotationSpeedChange = tpf * ROTATION_KICK;
		float rotationSlowDown = 1f - ROTATION_SLOW_DOWN_FACTOR * tpf;

		// Update rotation speed.
		if (rotateRight && rotateLeft) {
			rotationSpeed *= rotationSlowDown;
		} else if (rotateRight) {
			rotationSpeed = Math.min(rotationSpeed + rotationSpeedChange, MAX_ROTATION_SPEED);
		} else if (rotateLeft) {
			rotationSpeed = Math.max(rotationSpeed - rotationSpeedChange, -MAX_ROTATION_SPEED);
		}

		// Update the actual rotation.
		if (Math.abs(rotationSpeed) > 0.01f) {
			spaceCentre.rotate(0, 0, rotationSpeed * tpf);
			rotationSpeed *= rotationSlowDown;
		}
	}

	/**
	 * Creates a new bullet fired from the space craft.
	 */
	private void createBullet() {
		Vector3f startLocation = spaceCraft.getWorldTranslation();
		Geometry bullet = new Geometry("Bullit", BULLET_BOX);
		bullet.setMaterial(bulletMaterial);
		bullet.setLocalTranslation(startLocation);
		// TODO: would it be better to attach the bullets to a separate node that only moves in one direction?
		rootNode.attachChild(bullet);
		bullets.add(bullet);
		updateScore(BULLET_SCORE);
	}

	/**
	 * Adds the difference to the current score and update the score label.
	 *
	 * @param difference the increase/decrease in score.
	 */
	private void updateScore(int difference) {
		score += difference;
		scoreText.setText(Integer.toHexString(score));
	}

	private void createRing(float tpf) {
		elapsedTime += tpf;
		while (elapsedTime > 0.5f) {
			// Create the ring.
			elapsedTime -= 0.5f;
			lastRing -= gameSpeed / 2;
			Spatial ring = defaultRing.clone();
			ring.setMaterial(getRingColour());
			ring.setLocalTranslation(0f, 0f, lastRing);
			ring.rotate(0f, FastMath.HALF_PI / 4, 0);
			rootNode.attachChild(ring);
			// TODO: delete rings when out of view.

			// TODO: create obstacles over here.
			if (Math.random() > spawnChance) {
				spawnObstacle(lastRing);
			}
		}
	}

	private void spawnObstacle(float lastRing) {
		Vector3f playerLocation = spaceCraft.getWorldTranslation();
		Vector3f spawnLocation = new Vector3f(playerLocation.getX(), playerLocation.getY(), lastRing);

		int obstacleIndex = FastMath.nextRandomInt(0, 2);
		Spatial obstacle = obstacleDefaults[obstacleIndex].clone();
		obstacle.setLocalTranslation(spawnLocation);
		obstacle.setLocalRotation(spaceCraft.getWorldRotation());
		rootNode.attachChild(obstacle);
		obstacles.add(obstacle);
	}

	private Material getRingColour() {
		ringCount++;
		float amount = 0f;
		ColorRGBA startColour = ColorRGBA.Blue;
		ColorRGBA endColour = ColorRGBA.Blue;
		if (ringCount < 30) {
			amount = (float) ringCount / 30f;
			startColour = ColorRGBA.Blue.clone();
			endColour = ColorRGBA.Green.clone();
		} else if (ringCount < 60) {
			amount = (float) (ringCount - 30) / 30f;
			startColour = ColorRGBA.Green.clone();
			endColour = ColorRGBA.Yellow.clone();
		} else if (ringCount < 90) {
			amount = (float) (ringCount - 60) / 30f;
			startColour = ColorRGBA.Yellow.clone();
			endColour = ColorRGBA.Red.clone();
		} else if (ringCount < 120) {
			amount = (float) (ringCount - 90) / 30f;
			startColour = ColorRGBA.Red.clone();
			endColour = ColorRGBA.Blue.clone();
		} else {
			ringCount = 0;
			// Level up!
			level++;
			levelText.setText(Integer.toHexString(level));
			gameSpeed *= 1.2;
			spawnChance *= 1.1;
		}
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		startColour.interpolate(endColour, amount);
		mat.setColor("Color", startColour);
		return mat;
	}
}
