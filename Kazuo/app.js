const squares = document.querySelectorAll(".square");
const moles = document.querySelectorAll(".mole");
const timeLeft = document.querySelector("#timeLeft");

let score = document.querySelector("#score");
let result = 0;
let currentTime = timeLeft.textContent;
let moleTimer = null;
let countDownTimer = null;

function countDown() {
	timeLeft.textContent = --currentTime;
	if (currentTime <= 0) {
		clearInterval(moleTimer);
		clearInterval(countDownTimer);
		alert("You finished the game with " + result + " points! :D");
	}
}
countDownTimer = setInterval(countDown, 1000);

function spawnMole() {
	let randomPosition = squares[Math.floor(Math.random() * 9)];
	randomPosition.classList.toggle("mole");
}
moleTimer = setInterval(spawnMole, 500);

function whack() {
	if (this.classList.contains("mole")) {
		this.classList.remove("mole");
		score.textContent = ++result;
	} else {
		score.textContent = --result;
	}
}

squares.forEach(square => {
	square.addEventListener("mouseup", whack);
});
