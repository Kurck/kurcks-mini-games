"use strict";

const objectSize = 20;
const squaredRadiiSum = 4 * objectSize * objectSize;
const g = 300;

var canvas;
var bounds;
var context;
var previousTime = 0;
var gameObjects = [];

window.onload = init;

function init() {
	canvas = document.getElementById('canvas');
	bounds = new Vector2D(canvas.width, canvas.height);
	context = canvas.getContext('2d');
	for (var i = 0; i < 100; i++) {
		var position = new Vector2D(100 + Math.random() * (bounds.x - 200), 100 + Math.random() * (bounds.y - 200));
		var speed = new Vector2D(Math.random() * 300 - 150, Math.random() * 300 - 150);
		//gameObjects.push(new Square(position, speed));
		gameObjects.push(new Circle(position, speed));
	}
	window.requestAnimationFrame(gameLoop);
}

function gameLoop(timeStamp) {
	var secondsPassed = (timeStamp - previousTime) / 1000;
	previousTime = timeStamp;
	update(secondsPassed);
	draw();
	window.requestAnimationFrame(gameLoop);
}

function update(secondsPassed) {
	for (var i = 0; i < gameObjects.length; i++) {
		gameObjects[i].update(secondsPassed, bounds);
		gameObjects[i].bumpWalls(bounds);
		gameObjects[i].isColliding = false;
	}

	for (var i = 0; i < gameObjects.length; i++) {
		for (var j = i + 1; j < gameObjects.length; j++) {
			if (gameObjects[i].collidesWith(gameObjects[j])) {
				gameObjects[i].isColliding = true;
				gameObjects[j].isColliding = true;

				var positionA = gameObjects[i].position;
				var positionB = gameObjects[j].position;
				var vCollision = positionB.minus(positionA).normalize();
				var vRelativeVelocity = gameObjects[i].speed.minus(gameObjects[j].speed);
				var speed = vRelativeVelocity.x * vCollision.x + vRelativeVelocity.y * vCollision.y;
				
				if (speed >= 0) {
					var vChange = vCollision.multiplyBy(speed);
					gameObjects[i].speed = gameObjects[i].speed.plus(vChange);
					gameObjects[j].speed = gameObjects[j].speed.minus(vChange).multiplyBy(-1);
				}
			}
		}
	}
}

function draw() {
	context.clearRect(0, 0, canvas.width, canvas.height);
	for (var i = 0; i < gameObjects.length; i++) {
		gameObjects[i].draw(context);
	}
}

class Vector2D {
	constructor (x, y) {
		this.x = x;
		this.y = y;
	}
	
	length() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	
	normalize() {
		return this.devideBy(this.length());
	}

	squaredDistanceTo(other) {
		return (this.x - other.x) * (this.x - other.x)
			+ (this.y - other.y) * (this.y - other.y);
	}
	
	distanceTo(other) {
		return Math.sqrt(this.squaredDistanceTo(other));
	}
	
	minus(other) {
		return new Vector2D(other.x - this.x, other.y - this.y);
	}
	
	plus(other) {
		return new Vector2D(this.x + other.x, this.y + other.y);
	}
	
	multiplyBy(factor) {
		return new Vector2D(this.x * factor, this.y * factor);
	}

	devideBy(factor) {
		return new Vector2D(this.x / factor, this.y / factor);
	}
}

class GameObject {
	constructor (position, speed) {
		this.position = position;
		this.speed = speed;
		this.isColliding = false;
	}
}

class Circle extends GameObject {
	constructor (position, speed) {
		super(position, speed);
	}

	update(secondsPassed) {
		this.speed.y += g * secondsPassed;
		this.position.x += this.speed.x * secondsPassed;
		this.position.y += this.speed.y * secondsPassed;
	}

	bumpWalls(bounds) {
		if (this.position.x <= objectSize) {
			this.position.x = objectSize;//2 * objectSize - this.position.x;
			this.speed.x = -this.speed.x;
		}
		var maxX = bounds.x - objectSize;
		if (this.position.x >= maxX) {
			this.position.x = maxX;//2 * maxX - this.position.x;
			this.speed.x = -this.speed.x;
		}
		if (this.position.y <= objectSize) {
			this.position.y = objectSize;//2 * objectSize - this.position.y;
			this.speed.y = -this.speed.y;
		}
		var maxY = bounds.y - objectSize;
		if (this.position.y >= maxY) {
			this.position.y = maxY;//2 * maxY - this.position.y;
			this.speed.y = -this.speed.y;
		}
	}
	
	draw(context) {
		context.fillStyle = this.isColliding ? '#990000' : '#000000';
		context.beginPath();
		context.arc(this.position.x, this.position.y, objectSize, 0, 2 * Math.PI);
		context.fill();
	}

	collidesWith(other) {
		return this.position.squaredDistanceTo(other.position) <= squaredRadiiSum;
	}
}

class Square extends GameObject {
	constructor (position, speed) {
		super(position, speed);
	}

	update(secondsPassed) {
		this.position.x += this.speed.x * secondsPassed;
		this.position.y += this.speed.y * secondsPassed;
	}

	draw(context) {
		context.fillStyle = this.isColliding ? '#990000' : '#000000';
		context.fillRect(this.position.x, this.position.y, objectSize, objectSize);
	}
	
	collidesWith(other) {
		return other.position.x < this.position.x + objectSize
			&& this.position.x < other.position.x + objectSize
			&& other.position.y < this.position.y + objectSize
			&& this.position.y < other.position.y + objectSize;
	}
}
