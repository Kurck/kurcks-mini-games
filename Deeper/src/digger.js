Crafty.c("Digger", {
	init: function() {
		this.requires("Element, Collision, Fourway, DiggerSprite, SpriteAnimation")
			.fourway(100) // TODO: based on mode: flying or digging
			.collision([4, 4, 12, 4, 12, 12, 4, 12])
			.reel("DiggingLeft", 200, 0, 0, 3)
			.reel("DiggingUp", 200, 0, 1, 3)
			.reel("DiggingRight", 200, 0, 2, 3)
			.reel("DiggingDown", 200, 0, 3, 3)
			.bind('NewDirection', function(data) {
				if (data.x > 0) {
					this.animate("DiggingRight", -1);
				} else if (data.x < 0) {
					this.animate("DiggingLeft", -1);
				} else if (data.y > 0) {
					this.animate("DiggingDown", -1);
				} else if (data.y < 0) {
					this.animate("DiggingUp", -1);
				} else {
					this.pauseAnimation();
				}
			})
			.onHit("Diggable", this.dig)
			.onHit("Solid", this.stopMovement);
	},

	dig: function(hitData) {
		var s = Game.scaling;
		if (this.dx > 0 || this.dx < 0) {
			this.y -= (this.y - Math.round(this.y / s) * s) / 4;
		} else if (this.dy > 0 || this.dy < 0) {
			this.x -= (this.x - Math.round(this.x / s) * s) / 4;
		}

		// TODO: if the cargo bay is full, any collectable becomes a blocker or is dropped
		for (var i = 0; i < hitData.length; i++) {
			if (Math.abs(this.x - hitData[i].obj.x) < 8 &&
				Math.abs(this.y - hitData[i].obj.y) < 8) {
				Crafty.e("Air").attr({ x: hitData[i].obj.x, y: hitData[i].obj.y, z: 0});
				if (hitData[i].obj.getValue() > 0) {
					console.log("Collected " + hitData[i].obj.getValue());
					// TODO: add to cargo bay
				}
				hitData[i].obj.destroy();
			}
		}
	},

	stopMovement: function(hitData) {
		// TODO: Add an extra check?
		this.x -= this.dx;
		this.y -= this.dy;
		this.resetMotion();
	}
})

Crafty.c("CargoBay", {
	cargo: [],
	size: 10,

	hasRoom: function() {
		return cargo.length < size;
	}
});