Crafty.scene("Map", function() {
	var materialChances = [
		[1, 1, 1, 1, 1, 1],
		[0.1, 0.9, 0.9, 0.9, 1, 1],
		[0.1, 0.9, 0.9, 0.9, 0.98, 1],
		[0.01, 0.64, 0.8, 0.81, 0.9, 0.95, 1],
		[0, 0.15, 0.95, 1, 1, 1]
	];

	for (var y = 0; y < Game.rows; y++) {
		var chances = materialChances[Math.min(4, Math.floor(y / 5))];
		for (var x = 0; x < Game.columns; x++) {
			var pointer = Math.random();
			if (chances[0] > pointer) {
				Crafty.e("Air").at(x, y);
			} else if (chances[1] > pointer) {
				Crafty.e("Rock").at(x, y);
			} else if (chances[2] > pointer) {
				Crafty.e("Magma").at(x, y);
			} else if (chances[3] > pointer) {
				Crafty.e("Diamond").at(x, y);
			} else if (chances[4] > pointer) {
				Crafty.e("Iron").at(x, y);
			} else {
				Crafty.e("Gold").at(x, y);
			}
		}
	}

	Crafty.e("Digger")
		.at(Math.floor(Game.columns / 2), 2);
});

Crafty.c("Element", {
	init: function() {
		this.requires("2D, Canvas")
		this.value = 0;
	},

	at: function(x, y) {
		if (x === undefined && y === undefined) {
			return { x: this.x / Game.scaling, y: this.y / Game.scaling, z: 1 };
		} else {
			this.attr({ x: x * Game.scaling, y: y * Game.scaling, z: 1 });
			return this;
		}
	},

	getValue: function() {
		return this.value;
	}
});

Crafty.c("Air", {
	init: function() {
		this.requires("Element, AirSprite" + Crafty.math.randomInt(0, 3));
	}
});

Crafty.c("Rock", {
	init: function() {
		this.requires("Element, Diggable, RockSprite" + Crafty.math.randomInt(0, 3));
	}
});

Crafty.c("Magma", {
	init: function() {
		this.requires("Element, Solid, MagmaSprite" + Crafty.math.randomInt(0, 3));
	}
});

Crafty.c("Diamond", {
	init: function() {
		this.requires("Element, Diggable, DiamondSprite" + Crafty.math.randomInt(0, 3));
		this.value = 96;
	}
});

Crafty.c("Iron", {
	init: function() {
		this.requires("Element, Diggable, IronSprite" + Crafty.math.randomInt(0, 3));
		this.value = 12;
	}
});

Crafty.c("Gold", {
	value: 48,
	init: function() {
		this.requires("Element, Diggable, GoldSprite" + Crafty.math.randomInt(0, 3));
		this.value = 48;
	}
});