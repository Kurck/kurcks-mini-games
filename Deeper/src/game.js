Game = {

	scaling: 32,
	rows: 28,
	columns: 56,

	start: function() {
		Crafty.init(Game.scaling * Game.columns + 84, Game.scaling * Game.rows);
		Crafty.scene("Loading");
	}
}

$text_css = { "size": "24px", "family": "Arial", "color": "red", "text-align": "center" };

Crafty.scene("Loading", function() {
	Crafty.background("#000");
	Crafty.e("2D, DOM, Text")
		.text("Loading; please wait...")
		.attr({ x: 0, y: 96, w: 400 })
		.textFont($text_css);

	var elementNames = ["Air", "Rock", "Magma", "Diamond", "Iron", "Gold", "TODO", "EmptyCargo"];
	var elements = {};
	for (var i = 0; i < elementNames.length; i++) {
		for (var j = 0; j < 4; j++) {
			elements[elementNames[i] + "Sprite" + j] = [j, i];
		}
	}
	var assets = {
		"sprites": {
			"assets/elements.png": {
				"tile": 32, "tileh": 32,
				"map": elements,
			},
			"assets/digger.png": {
				"tile": 30, "tileh": 30,
				"paddingX": 2, "paddingY": 2,
				"map": { "DiggerSprite": [1,1] }
			},
			"assets/gui.png": {
				"tile": 12, "tileh": 12,
				"map": {
					"CargoBayTopSprite": [0,0,7,4],
					"CargoBaySlotSprite": [0,1,7,3],
					"CargoBayBottomSprite": [0,2,7,2]
				}
			}
		}
	};

	Crafty.load(assets,
		function() { Crafty.scene("Map"); },
		function(e) { },
		function(e) { alert(e); }
	);
});