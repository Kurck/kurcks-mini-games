kaboom({
	global: true,
	fullscreen: true,
	scale: 1,
	clearColor: [0,0,0,1],

	debug: true,
})

loadRoot("https://i.imgur.com/")
loadSprite("brick", "pogC9x5.png")
loadSprite("rocks", "M6rwarW.png")
loadSprite("empty-block", "bdrLpi6.png")
loadSprite("bonus-block", "gesQ1KP.png")
loadSprite("pipe-top-left", "ReTPiWY.png")
loadSprite("pipe-top-right", "hj2GK4n.png")
loadSprite("pipe-left", "c1cYSbt.png")
loadSprite("pipe-right", "nqQ79eI.png")
loadSprite("goomba", "KPO3fR9.png")
loadSprite("coin", "wbKxhcd.png")
loadSprite("mushroom", "0wMd92p.png")
loadSprite("kario", "Wb1qfhK.png")

scene("game", ({ level, score }) => {
	layers(["bg", "obj", "ui"], "obj")

	const levels = 
	[
		[
			"                                        ",
			"                                        ",
			"                                        ",
			"                                        ",
			"                                        ",
			"                                        ",
			"                                        ",
			"           -- $ $ -%-         *-        ",
			"                                    []  ",
			"                                    ()  ",
			"                           @  @     ()  ",
			"================================   ====="
		],[
			"                                        ",
			"                                        ",
			"                                        ",
			"         $    *  $   $   $ $ $      $   ",
			"                                     @  ",
			"             @     @                []  ",
			"            -- -   -   -   ---    - ()  ",
			"               $   $   $   $$$    $ ()  ",
			"         --                         ()  ",
			"                                    ()  ",
			"                @   @     @   @    @()  ",
			"========================================"
		]
	]

	const levelConfig = { 
		width: 20,
		height: 20,
		"-": [sprite("brick"), solid()],
		"=": [sprite("rocks"), solid()],
		"#": [sprite("empty-block"), solid()],
		"%": [sprite("bonus-block"), solid(), "bonus-coin"],
		"*": [sprite("bonus-block"), solid(), "bonus-mushroom"],
		"[": [sprite("pipe-top-left"), solid(), scale(0.5), "finish"],
		"]": [sprite("pipe-top-right"), solid(), scale(0.5), "finish"],
		"(": [sprite("pipe-left"), solid(), scale(0.5)],
		")": [sprite("pipe-right"), solid(), scale(0.5)],
		"@": [sprite("goomba"), solid(), body(), "goomba"],
		"$": [sprite("coin"), "coin"],
		"+": [sprite("mushroom"), body(), "mushroom"],
	}

	const gameLevel = addLevel(levels[level % levels.length], levelConfig)

	add([
		text("L " + level),
		pos(4, 6)
	])
	add([
		sprite("coin"), scale(0.5),
		pos(3, 16),
		layer("ui")
	])
	const scoreLabel = add([
		text(score),
		pos(20, 16),
		layer("ui"),
		{ value: score }
	])

	action("goomba", (goomba) => {
		goomba.move(-30, 0)
	})
	action("mushroom", (mushroom) => {
		mushroom.move(30, 0)
	})

	const DEFAULT_MOVE_SPEED = 180
	const DEFAULT_JUMP_FORCE = 360
	let moveSpeed = DEFAULT_MOVE_SPEED;
	let jumpForce = DEFAULT_JUMP_FORCE;
	let isJumping = false;
	function big() {
		let isBig = false
		return {
			isBig() { return this.isBig },
			grow() {
				this.scale = vec2(2)
				moveSpeed *= 1.4
				jumpForce *= 1.4
				this.isBig = true
			},
			shrink() {
				this.scale = vec2(1)
				moveSpeed = DEFAULT_MOVE_SPEED
				jumpForce = DEFAULT_JUMP_FORCE
				this.isBig = false
			}
		}
	}
	const player = add([
		sprite("kario"), solid(),
		pos(30, 0),
		body(),
		big(),
		origin("bot")
	])
	player.action(() => {
		if (player.grounded()) {
			isJumping = false
		}
		camPos(player.pos)
		if (player.pos.y > 400) {
			die()
		}
	})
	keyDown("left", () => { player.move(-moveSpeed, 0) })
	keyDown("right", () => { player.move(moveSpeed, 0) })
	keyDown("space", () => {
		if (player.grounded()) {
			player.jump(jumpForce)
			isJumping = true
		}
	})
	function spawnBonus(block, bonus) {
		gameLevel.spawn(bonus, block.gridPos.sub(0,1))
		destroy(block)
		gameLevel.spawn("#", block.gridPos)
	}
	player.on("headbump", (obj) => {
		if (obj.is("bonus-coin")) {
			spawnBonus(obj, "$")
		} else if (obj.is("bonus-mushroom")) {
			spawnBonus(obj, "+")
		}
	})
	player.collides("mushroom", (mushroom) => {
		destroy(mushroom)
		player.grow()
	})
	player.collides("coin", (coin) => {
		destroy(coin)
		scoreLabel.value++
		scoreLabel.text = scoreLabel.value
	})
	player.collides("finish", () => {
		keyPress("down", () => {
			go("game", { level: level+1, score: scoreLabel.value })
		})
	})
	function die() {
		go("endscreen", { score: scoreLabel.value })
	}
	player.collides("goomba", (goomba) => {
		if (isJumping) {
			destroy(goomba)
		} else {
			if (player.isBig) {
				player.shrink()
			} else {
				die()
			}
		}
	})
})

scene("endscreen", ({score}) => {
	add([text("You died :(", 32), origin("center"), pos(width()/2, height()/2 - 48)])
	add([text("You collected " + score + " coins", 32), origin("center"), pos(width()/2, height()/2)])
})

start("game", { level: 0, score: 0 })