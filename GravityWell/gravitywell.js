document.addEventListener("DOMContentLoaded", () => {

	const HEIGHT = 600;
	const WIDTH = 800;
	const FULL_CIRCLE = 2 * Math.PI;

	const DISPLAY = document.getElementById("display");
	const SCORE_SPAN = document.getElementById("score");
	const LEVEL_SPAN = document.getElementById("level");
	const MESSAGE_BOX = document.getElementById("message");
	const SPRITES = document.getElementById("sprites");

	class Vector2D {
		constructor(x, y) {
			this.x = x;
			this.y = y;
		}

		squaredLength() {
			return this.x * this.x + this.y * this.y;
		}

		length() {
			return Math.sqrt(this.squaredLength());
		}

		normalize() {
			return this.devideBy(this.length());
		}

		normalizeSquared() {
			return this.devideBy(this.squaredLength());
		}

		squaredDistanceTo(other) {
			return (this.x - other.x) * (this.x - other.x)
				+ (this.y - other.y) * (this.y - other.y);
		}

		distanceTo(other) {
			return Math.sqrt(this.squaredDistanceTo(other));
		}

		minus(other) {
			return new Vector2D(this.x - other.x, this.y - other.y);
		}

		plus(other) {
			return new Vector2D(this.x + other.x, this.y + other.y);
		}

		multiplyBy(factor) {
			return new Vector2D(this.x * factor, this.y * factor);
		}

		devideBy(factor) {
			return new Vector2D(this.x / factor, this.y / factor);
		}
	}

	class Target {
		constructor(canvas, level) {
			this.canvas = canvas;
			this.position = new Vector2D(0, 0);
			this.radius = 50;
			this.phase = 0.0;
		}

		update(timePassed) {
			this.phase += timePassed;
		}

		draw() {
			this.canvas.beginPath();
			this.canvas.arc(this.position.x, this.position.y, this.radius, 0, FULL_CIRCLE);
			let opacity = 10 + Math.abs(Math.floor(66 * Math.sin(this.phase)));
			this.canvas.fillStyle = `#009900${opacity}`;
			this.canvas.fill();
		}
	}

	const STATE_AIMING = 0;
	const STATE_FLYING = 1;
	const STATE_CRASHED = 2;
	const STATE_SUCCESS = 3;

	class Spacecraft {
		constructor(canvas) {
			this.canvas = canvas;
			this.speed = new Vector2D(0, 0);
			this.radius = 10;
			this.prepareNextFlight(new Vector2D(0, 0));
		}

		aim(event) {
			if (this.state === STATE_AIMING) {
				event = event ? event : window.event;
				this.speed = new Vector2D(
					event.clientX - DISPLAY.offsetLeft - this.position.x,
					event.clientY - DISPLAY.offsetTop - this.position.y
				);
			}
		}

		launch() {
			this.attempts.push(this.speed);
			this.state = STATE_FLYING;
			MESSAGE_BOX.innerText = "Click left to revert to the launch position.";
		}

		update(timePassed) {
			if (this.state !== STATE_FLYING) {
				return;
			}

			for (let i = 0; i < planets.length; i++) {
				let planet = planets[i];
				let distance = this.position.distanceTo(planet.position);
				if (distance < this.radius + planet.radius) {
					this.updateState(STATE_CRASHED, "Your spacecraft crashed :(<br />Click to try again.");
					return;
				} else {
					let gravity = planet.position
						.minus(this.position)
						.normalizeSquared()
						.multiplyBy(planet.mass);
					this.speed = this.speed.plus(gravity);
				}
			}
			this.position = this.position.plus(this.speed.multiplyBy(timePassed));

			if (this.position.distanceTo(target.position) < this.radius + target.radius) {
				this.endLevel();
			}
		}

		endLevel() {
			this.updateState(STATE_SUCCESS, "You hit the target! :D<br />Click to proceed to the next level.");
			score += level + Math.floor(128 * Math.pow(2, 1-this.attempts.length));
			SCORE_SPAN.innerText = score;
			DISPLAY.classList.add("success");
		}

		draw() {
			this.attempts.forEach(attempt => this.drawAttempt(attempt, "#AAAAAA22"));
			if (this.state === STATE_AIMING) {
				this.drawAttempt(this.speed, "#FFFFFF");
			}

			this.canvas.beginPath();
			if (this.position.x < -this.radius || this.position.x > WIDTH + this.radius
				|| this.position.y < -this.radius || this.position.y > HEIGHT + this.radius) {
				let x = Math.max(5, Math.min(WIDTH - 5, this.position.x));
				let y = Math.max(5, Math.min(HEIGHT - 5, this.position.y));
				this.canvas.arc(x, y, 5, 0, FULL_CIRCLE);
			} else {
				this.canvas.drawImage(SPRITES, 0, 768, 28, 25, this.position.x - 14, this.position.y - 12.5, 28, 25);
			}
			this.canvas.fillStyle = "#990000";
			this.canvas.fill();
		}

		drawAttempt(offset, style) {
			this.canvas.beginPath();
			this.canvas.moveTo(this.originalPosition.x, this.originalPosition.y);
			this.canvas.lineTo(this.originalPosition.x + offset.x, this.originalPosition.y + offset.y);
			this.canvas.strokeStyle = style;
			this.canvas.lineWidth = 3;
			this.canvas.stroke();
		}

		updateState(state, message) {
			this.state = state;
			MESSAGE_BOX.innerHTML = message;
			DISPLAY.classList.add("error");
		}

		reset() {
			this.state = STATE_AIMING;
			this.position = this.originalPosition;
			DISPLAY.classList.remove("error");
			MESSAGE_BOX.innerText = "Click left to launch.";
		}

		prepareNextFlight(newPosition) {
			this.originalPosition = newPosition;
			this.reset();
			this.attempts = [];
		}
	}

	class Planet {
		constructor(canvas, position, radius) {
			this.canvas = canvas;
			this.position = position;
			this.radius = radius;
			this.mass = radius * 8;
			this.image = {
				source: {
					x: Math.floor(Math.random() * 4) * 256,
					y: this.radius > 70 ? 0 : (1 + Math.floor(Math.random() * 2)) * 256,
					size: 256
				},
				target: {
					x: this.position.x - this.radius,
					y: this.position.y - this.radius,
					size: this.radius * 2
				}
			};
			
		}

		draw() {
			this.canvas.drawImage(
				SPRITES,
				this.image.source.x, this.image.source.y, this.image.source.size, this.image.source.size,
				this.image.target.x, this.image.target.y, this.image.target.size, this.image.target.size
			);
		}
	}

	const MIN_MARGIN = 25;
	const MARGIN_RANGE = 100;
	const X_RANGE = WIDTH - 2 * MIN_MARGIN;
	const Y_RANGE = HEIGHT - 2 * MIN_MARGIN;
	const MIN_RADIUS = 10;
	const SIZE_RANGE = 90;

	let canvas;
	let previousTime = 0;
	let score;
	let level = -1;
	let planets = [];
	let target;
	let spacecraft;
	let focussed = true;

	initialize();

	function initialize() {
		score = 0;
		canvas = DISPLAY.getContext('2d');
		target = new Target(canvas);
		spacecraft = new Spacecraft(canvas);
		loadNextLevel();
		window.onmousemove = (e) => spacecraft.aim(e);
		window.onblur = () => focussed = false;
		window.onfocus = () => focussed = true;
		window.onclick = (e) => handleClick(e);
		document.addEventListener("keydown", handleKeyDown);
		window.requestAnimationFrame(update);
	}

	function loadNextLevel() {
		DISPLAY.classList.remove("success");
		LEVEL_SPAN.innerText = ++level;

		spacecraft.prepareNextFlight(new Vector2D(randomScalar(MIN_MARGIN, MARGIN_RANGE), HEIGHT / 2));

		target.radius = Math.max(MIN_RADIUS, target.radius - 1);
		target.position = new Vector2D(WIDTH - randomScalar(MIN_MARGIN, MARGIN_RANGE), randomScalar(MIN_MARGIN, Y_RANGE));

		loadPlanets();
	}

	function loadPlanets() {
		planets = [];
		let planetPosition = target.position
			.minus(spacecraft.originalPosition)
			.devideBy(2)
			.plus(spacecraft.originalPosition);
		planets.push(new Planet(canvas, planetPosition, randomScalar(MIN_RADIUS, SIZE_RANGE)));

		for (let i = 0; i < level / 2; i++) {
			for (let tries = 0; tries < 3; tries++) {
				let position = new Vector2D(randomScalar(MIN_MARGIN, X_RANGE), randomScalar(MIN_MARGIN, Y_RANGE));
				let radius = randomScalar(MIN_RADIUS, SIZE_RANGE);
				let overlap = hasOverlap(position, radius + MIN_MARGIN);
				if (!overlap) {
					planets.push(new Planet(canvas, position, radius));
					break;
				}
			}
		}
	}

	function randomScalar(min, range) {
		return min + Math.random() * range;
	}

	function hasOverlap(position, radius) {
		if (spacecraft.position.distanceTo(position) < spacecraft.radius + 2 * radius
			|| target.position.distanceTo(position) < target.radius + 2 * radius) {
			return true;
		}
		for (let i = 0; i < planets.length; i++) {
			if (planets[i].position.distanceTo(position) < planets[i].radius + radius) {
				return true;
			}
		}
		return false;
	}

	function handleClick(e) {
		switch (spacecraft.state) {
			case STATE_AIMING: spacecraft.launch(); break;
			case STATE_SUCCESS: loadNextLevel(); break;
			default: spacecraft.reset(); break;
		}
	}

	const KEY_B = 66;
	const KEY_G = 71;
	const KEY_R = 82;
	const KEY_ENTER = 13;

	function handleKeyDown(event) {
		if (event.keyCode === KEY_R || event.keyCode === KEY_ENTER) {
			if (spacecraft.state === STATE_SUCCESS) {
				loadNextLevel();
			} else {
				spacecraft.reset();
			}
		} else if (event.keyCode === KEY_G || event.keyCode === KEY_B) {
			score -= 128 + 512 / level;
			SCORE_SPAN.innerText = score;
			level--;
			loadNextLevel();
		}
	}

	function update(timeStamp) {
		if (focussed) {
			let timePassed = (timeStamp - previousTime) / 1000;
			previousTime = timeStamp;
			target.update(timePassed);
			spacecraft.update(timePassed, planets);
			draw();
		}
		window.requestAnimationFrame(update);
	}

	function draw() {
		canvas.clearRect(0, 0, DISPLAY.width, DISPLAY.height);
		planets.forEach(planet => planet.draw());
		target.draw();
		spacecraft.draw();
	}

});