package fruits;

import game.FruitOrganiser;

/**
 * Created by kurck on 16-2-16.
 */
public class Apple extends Fruit {

	private int attackStreak;

	public Apple(FruitOrganiser parent) {
		super(parent);
	}

	@Override
	protected void doSomething() {
		if (attackStreak < 3) {
			Fruit opponent = organiser.getHighValueOpponentWithLowTech(position, getTechLevel());
			if (opponent == null) {
				opponent = organiser.getRandomOpponent(position);
			}
			if (opponent != null) {
				attack(opponent);
				attackStreak++;
			}
		} else {
			buyCow();
			upgradeTechnology();
			upgradeDefence();
			upgradeOffence();
			attackStreak = 0;
		}
	}
}
