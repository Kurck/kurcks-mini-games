package fruits;

import game.FruitOrganiser;
import game.NameGenerator;

/**
 * Created by kurck on 15-2-16.
 */
public abstract class Fruit implements Comparable<Fruit> {

	protected String name = NameGenerator.generate();
	protected FruitOrganiser organiser;
	private int credits = 100;
	private int actionsLeft = 3;
	private int countdownToNewAction = 12;
	private int defenceLevel = 0;
	private int attackLevel = 0;
	private int cows = 1;
	private int techLevel = 0;
	private int score = 1024;
	private int successfulAttacks = 0;
	private int numberOfAttacks = 0;
	private int successfulDefences = 0;
	private int numberOfDefences = 0;
	private float attackBonus = 1.0f;
	private float defenceBonus = 1.0f;
	protected int position = -1;
	protected boolean notify = false;

	public Fruit(FruitOrganiser parent) {
		this.organiser = parent;
	}

	public int getCredits() { return this.credits; }
	public int getTechLevel() { return this.techLevel; }
	public void setPosition(int newPosition) {
		this.position = newPosition;
	}

	public final void update() {
		credits += cows;
		countdownToNewAction--;
		if (countdownToNewAction <= 0) {
			actionsLeft++;
			countdownToNewAction = 12;
			//notify("A new action opportunity was added, bringing the total to %d.\n", actionsLeft);
		}
		if (actionsLeft > 0) {
			doSomething();
		}
	}

	protected abstract void doSomething();

	protected void upgradeTechnology() {
		// Check credits.
		int upgradeCosts = upgradeTechCosts();
		if (upgradeCosts > credits) {
			notify("Unable to upgrade to a new technology level due to insufficient funds. You have %d credits, but you need %d for upgrading.\n", credits, upgradeCosts);
			return;
		}

		// Execute upgrade.
		credits -= upgradeCosts;
		techLevel++;
		notify("Upgraded to technology level %d for %d credits. You have %d credits left on your account.\n", techLevel, upgradeCosts, credits);
	}

	protected void upgradeDefence() { defenceLevel = getNewDefOffLevel(defenceLevel, "defence"); }

	protected void upgradeOffence() {
		attackLevel = getNewDefOffLevel(attackLevel, "offence");
	}

	protected void buyCow() {
		int cowCosts = cowCosts();
		if (cowCosts > credits) {
			notify("Unable to buy a new cow. You have %d credits, but a new cow costs %d.\n", credits, cowCosts);
			return;
		}

		credits -= cowCosts;
		cows++;
		notify("You bought a new cow for %d credits. You now own %d cows.\n", cowCosts, cows);
	}

	private int cowCosts() {
		return 100 * (int) Math.pow(1.5, cows + 1);
	}

	private int upgradeTechCosts() {
		return 60 * (int) Math.pow(2.5, techLevel + 1);
	}

	private int upgradeDefAttCosts(int currentLevel) {
		int newLevel = currentLevel + 1;
		int upgradeCosts = (1 << newLevel) * newLevel * 60;
		return upgradeCosts;
	}

	private int getNewDefOffLevel(int currentLevel, String type) {
		// The technology should allow the next level.
		if (techLevel <= currentLevel) {
			notify("You need to upgrade your technology to %d before upgrading your %s.\n", currentLevel + 1, type);
			return currentLevel;
		}
		// The player requires enough credits.
		int upgradeCosts = upgradeDefAttCosts(currentLevel);
		if (upgradeCosts > credits) {
			notify("Unable to upgrade your %s due to insufficient funds. You have %d credits, but you need %d for upgrading.\n", type, credits, upgradeCosts);
			return currentLevel;
		}
		// Enough credits and sufficient technology:
		credits -= upgradeCosts;
		int newLevel = currentLevel + 1;
		notify("Upgraded your %s to level %d for %d credits. You have %d credits left on your account.\n", type, newLevel, upgradeCosts, credits);
		return newLevel;
	}

	protected void notify(String template, Object... arguments) {
		if (notify) {
			System.out.format(template, arguments);
		}
	}

	protected void attackRandomOpponent() {
		int tryCount = 0;
		Fruit opponent = null;
		// The random opponent can return null if the found opponent is the player itself. In such a case, another try is done, up to five times if needed. Hitting this limit is a rare event.
		while (opponent == null && tryCount < 5) {
			opponent = organiser.getRandomOpponent(position);
			tryCount++;
		}
		if (opponent == null) {
			// Failed to find an opponent.
			notify("Failed to find a random opponent to attack.\n");
			return;
		}
		attack(opponent);
	}

	protected void attack(Fruit opponent) {
		if (actionsLeft < 1) {
			notify("Insufficient attack opportunities left.\n");
			return;
		} else if (opponent == this) {
			notify("That's you, smartass ;)\n");
			return;
		}

		actionsLeft--;
		this.numberOfAttacks++;
		opponent.numberOfDefences++;

		// Strengths are powers of two:
		int offensiveStrength = (int) ((1 << this.attackLevel) * attackBonus);
		int defensiveStrength = (int) ((1 << opponent.defenceLevel) * opponent.defenceBonus);
		if (offensiveStrength >= defensiveStrength) {
			// Win
			this.credits += opponent.credits;
			int wonScore = opponent.score / 64;
			opponent.score -= wonScore;
			this.score += wonScore;
			this.notify("You successfully smushed %s (%d) and stole %d credits and %d points.\n", opponent.name, opponent.position, opponent.credits, wonScore);
			this.findSomeL00t();
			opponent.notify("%s (%d) smushed you. Your opponent stole %d credits and %d points from you.\n", name, position, opponent.credits, wonScore);
			opponent.credits = 0;
			this.successfulAttacks++;
		} else {
			// Loose
			int lostScore = this.score / 128;
			this.score -= lostScore;
			opponent.score += lostScore;
			if (this.attackLevel > 0) {
				this.attackLevel--;
				this.notify("You couldn't break the defence of %s (%d). You lost %d points and your attack level dropped by 1.\n", opponent.name, opponent.position, lostScore);
				if (notify && Math.random() < 0.1 && defensiveStrength > 10 * offensiveStrength) {
					// http://cc314.shikadi.net/oldcc314/ascii-dopefish.htm
					System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
					System.out.println("W This fort is garded by WWWWWW..;;;;;.WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
					System.out.println("W a dopefish. You had no WWWW.;;;;;;;WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
					System.out.println("W chance of winning. WWWWWWW;;;;;;;;;WWWWWWWWWWWWWWWWWWWWWW..WWWWWWWWWWWWWWW");
					System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWW.;;;;;;;;.WWWWWWWWWWWWWWWWWWWWWWW..;;.WWWWWWWWWWW");
					System.out.println("WWWWWWWWWWWWWWWWWWWWW...;;;.......;;..WWWWWWWWWWWWWWWWWWWWWWWW.;;..WWWWWWWWW");
					System.out.println("WWWWWWWWWWWWWWWW...;;;;;;;;;;;;;;;;;;;;;;..WWWWWWWWWWWWWWWWWWWW.;;;.WWWWWWWW");
					System.out.println("WWWWWWWWWWWWWW.;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWWWWWWWWWWWWWWW.;;;;..WWWWWW");
					System.out.println("WWWWWWWWWWW;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;.WWWWWWWWWWWWWW.;;;;;..WWWWW");
					System.out.println("WWWWWWWWW.;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;.WWWWWWWWWWWW.;;;;;;..WWWW");
					System.out.println("WWWWWWW....;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;.WWWWWWWWWW.;;;;;;;;..WWW");
					System.out.println("WWWWW.VVV..VVVVV..;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWWWWW.;;;;;;;;;..WWW");
					System.out.println("WWW.VVVVVVVVVVVVVVVV;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWW.;;;;;;;;;;;..WWW");
					System.out.println("WW.VV......VVVVVVVVV.;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;....;;;;;;;;;;;..WWWW");
					System.out.println("WW.VV......VVVVVVVVVV.;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWWW");
					System.out.println("WW..VVV.....VVVVVVVVV.;;;;;;...;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWWWWWW");
					System.out.println("WW.;.V.VVVVVVVVVVVVV.;;;;;;;;..;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;.WWWWWWWW");
					System.out.println("WW...;.VV..VVVVVVVV.;..;;;;;;..;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWWWW");
					System.out.println("WWW......;;;......;;;;;;..;;;..;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWWW");
					System.out.println("WWW.XX.XX...............;;;;;..;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;..WWWW");
					System.out.println("WWW.XX.XXXXX.;;;;;;.;....;;;...;;;;;;;;;;;;;;;;;;;;;;;;....;;;;;;;;;;;..WWWW");
					System.out.println("WWW.XX.XXXXX....;;;;;;;..;;;..;;;;;;;;;;;;;;;;;;;;;;;..WWWW..;;;;;;;;;..WWWW");
					System.out.println("WWW.XX.XXXXX.;;;;;;.;;;;.;;;;;;;;;;;;;;;;;;;;;..;;....WWWWWW..;;;;;;;;..WWWW");
					System.out.println("WWW.XX.XXXXX.....;;;;;;;;;;;;;;;;;;;;;;;;;.;;;;....WWWWWWWWWWW.;;;;;;..WWWWW");
					System.out.println("WWW.XX.XXXXX.;;;;;;;;;;;;;;;;;;;;;;;;;;;;;.;;.....WWWWWWWWWWWW.;;;;;;..WWWWW");
					System.out.println("WWW....XXXXX..;;;;;;;.;;;;;;;;;;;;;;;;;;;;......WWWWWWWWWWWWWW.;;;;;..WWWWWW");
					System.out.println("WWWWWWW...WV.WW......;;;;;;;;;;;;;;;;;;;;;;...WWWWWWWWWWWWWWW.;;;;;;.WWWWWWW");
					System.out.println("WWWWWWWWWWWWWWWWWWWWW..;;;;;;;;;;;;;;;;;...WWWWWWWWWWWWWWWWWW.;;;..WWWWWWWWW");
					System.out.println("WWWWWWWWWWWWWWWWWWWWWWWW..;;;;;;;;;...WWWWWWWWWWWWWWWWWWWWWW.;;.WWWWWWWWWWWW");
					System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
				}
			} else {
				this.notify("You couldn't break the defence of %s (%d). You lost %d points.\n", opponent.name, opponent.position, lostScore);
			}
			opponent.notify("You were attacked by %s (%d) but your defences held. You gained %d points.\n", name, position, lostScore);
			opponent.successfulDefences++;
		}
	}

	public void findSomeL00t() {
		double magRnd = Math.random();
		float magnitude;
		if (magRnd < 0.1) {
			magnitude = 0.001f;
		} else if (magRnd < 0.15) {
			magnitude = 0.002f;
		} else if (magRnd < 0.175) {
			magnitude = 0.004f;
		} else if (magRnd < 0.1875) {
			magnitude = 0.008f;
		} else if (magRnd < 0.19375) {
			magnitude = 0.016f;
		} else if (magRnd < 0.2) {
			notify("You didn't find any loot, but you did find a bunny:\n");
			notify(" (\\_/)\n");
			notify(" (^.^)\n");
			notify("(\") (\")\n");
			return;
		} else {
			// No loot today.
			return;
		}
		double typeRnd = Math.random();
		if (typeRnd < 0.5) {
			notify("You found a defence bonus of %.1f%%!\n", magnitude * 100);
			defenceBonus += magnitude;
		} else {
			notify("You found an attack bonus of %.1f%%!\n", magnitude * 100);
			attackBonus += magnitude;
		}
	}

	public String toShortString() {
		StringBuilder description = new StringBuilder();
		description.append(position);
		description.append('\t');
		description.append(techLevel);
		description.append('\t');
		description.append(credits);
		description.append('\t');
		description.append(name);
		return description.toString();
	}

	public String toDetailedString() {
		StringBuilder description = new StringBuilder();
		description.append(position);
		description.append('\t');
		description.append(getClass().getSimpleName());
		description.append('\t');
		description.append(score);
		description.append('\t');
		description.append(techLevel);
		description.append('\t');
		description.append(cows);
		description.append('\t');
		description.append(credits);
		description.append('\t');
		description.append(name);
		return description.toString();
	}

	@Override
	public String toString() {
		StringBuilder description = new StringBuilder();
		description.append("####################################################");
		description.append("\n# Name:\t\t\t");
		description.append(name);
		if (!notify) {
			description.append("\n# Type:\t\t\t");
			description.append(getClass().getSimpleName());
		}
		description.append("\n# Score:\t\t");
		description.append(score);
		description.append("\n# Position:\t\t");
		description.append(position);
		description.append("\n# Credits:\t\t");
		description.append(credits);
		description.append("\n# Smushings left:\t");
		description.append(actionsLeft);
		description.append("\n####################################################");
		description.append("\n# Fruit smushed:\t");
		description.append(successfulAttacks);
		description.append('/');
		description.append(numberOfAttacks);
		description.append("\n# Prevented smushes:\t");
		description.append(successfulDefences);
		description.append('/');
		description.append(numberOfDefences);
		description.append("\n####################################################");
		description.append("\n# Cows:\t\t\t");
		description.append(cows);
		description.append(" (a new one costs ");
		description.append(cowCosts());
		description.append(" credits)");
		description.append("\n# Technology level:\t");
		description.append(techLevel);
		description.append(" (upgrading costs ");
		description.append(upgradeTechCosts());
		description.append(" credits)");
		description.append("\n# Defensive level:\t");
		description.append(defenceLevel);
		description.append(" +");
		description.append(String.format("%.1f", (defenceBonus - 1) * 100));
		description.append("% (upgrading costs ");
		description.append(upgradeDefAttCosts(defenceLevel));
		description.append(" credits)");
		description.append("\n# Offensive level:\t");
		description.append(attackLevel);
		description.append(" +");
		description.append(String.format("%.1f", (attackBonus - 1) * 100));
		description.append("% (upgrading costs ");
		description.append(upgradeDefAttCosts(attackLevel));
		description.append(" credits)");
		description.append("\n####################################################");
		return description.toString();
	}

	@Override
	public int compareTo(Fruit other) {
		int difference = Integer.compare(other.score, this.score);
		return difference;
	}
}
