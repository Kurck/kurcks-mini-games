package fruits;

import game.FruitOrganiser;

/**
 * Created by kurck on 16-2-16.
 */
public class Peach extends Fruit {

	public Peach(FruitOrganiser parent) {
		super(parent);
	}

	@Override
	protected void doSomething() {
		Fruit opponent = organiser.getHighValueOpponentWithLowTech(position, getTechLevel());
		if (opponent != null) {
			attack(opponent);
		}
		if (Math.random() > 0.5) {
			upgradeTechnology();
			upgradeOffence();
			upgradeDefence();
			buyCow();
		}
	}

}
