package fruits;

import game.FruitOrganiser;

/**
 * Created by kurck on 15-2-16.
 */
public class Banana extends Fruit {

	public Banana(FruitOrganiser parent) {
		super(parent);
	}

	@Override
	protected void doSomething() {
		double rnd = Math.random();
		if (rnd < 0.1) {
			upgradeTechnology();
		} else if (rnd < 0.2) {
			upgradeDefence();
		} else if (rnd < 0.3) {
			upgradeOffence();
		} else if (rnd < 0.45) {
			attackRandomOpponent();
		} else if (rnd < 0.7) {
			buyCow();
		}
		// 30% chance on idle time.
	}
}
