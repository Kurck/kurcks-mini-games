package fruits;

import game.FruitOrganiser;

/**
 * Created by kurck on 15-2-16.
 */
public class Pineapple extends Fruit {

	public Pineapple(FruitOrganiser parent) {
		super(parent);
	}

	@Override
	protected void doSomething() {
		double rnd = Math.random();
		if (rnd < 0.15) {
			upgradeTechnology();
		} else if (rnd < 0.3) {
			upgradeDefence();
		} else if (rnd < 0.6) {
			buyCow();
		}
		// 50% chance on idle time.
	}
}
