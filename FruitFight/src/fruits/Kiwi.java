package fruits;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import game.FruitOrganiser;

/**
 * Created by kurck on 15-2-16.
 */
public class Kiwi extends Fruit {

	private static final Set<String> ATTACK_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "a", "att", "attack" }));
	private static final Set<String> UPGRADE_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "u", "up", "upgrade" }));
	private static final Set<String> UPGRADE_TECH_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "t", "tech", "technology" }));
	private static final Set<String> UPGRADE_DEFENSE_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "d", "def", "defense", "defence" }));
	private static final Set<String> UPGRADE_OFFENSE_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "a", "att", "attack", "off", "offense", "offence" }));
	private static final Set<String> UPGRADE_COW_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "c", "cow", "cows" }));
	private static final Set<String> LIST_OPPONENTS_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "l", "ls", "list" }));
	private static final Set<String> LIST_FILTERED_OPPONENTS_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "lf" }));
	private static final Set<String> DETAILED_LIST_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "p", "page" }));
	private static final Set<String> CURRENT_STATISTICS_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "s", "stats", "statistics" }));
	private static final Set<String> SPY_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "spy" }));
	private static final Set<String> STOP_GAME_COMMANDS = new HashSet<>(Arrays.asList(new String[]{ "q", "quit", "exit" }));

	private boolean continuePlaying = true;

	public Kiwi(FruitOrganiser parent) {
		super(parent);
		notify = true;
		name = "YOU";
	}

	public boolean wantsToContinue() {
		return continuePlaying;
	}

	@Override
	public void doSomething() {
		// To have a better design: maybe a command queue should be created.
	}

	public void executeCommand(String command) {
		// Skip empty commands.
		if (command == null || command.isEmpty()) {
			return;
		}

		// Parse the command in parts.
		String[] commandParts = command.split(" ");
		String mainCommand = commandParts[0].toLowerCase();
		if (ATTACK_COMMANDS.contains(mainCommand)) {
			parseAttackCommand(commandParts);
		} else if (UPGRADE_COMMANDS.contains(mainCommand)) {
			parseUpgradeCommand(command, commandParts);
		} else if (LIST_OPPONENTS_COMMANDS.contains(mainCommand)) {
			organiser.listOpponents(position, Integer.MAX_VALUE);
		} else if (LIST_FILTERED_OPPONENTS_COMMANDS.contains(mainCommand)) {
			organiser.listOpponents(position, getTechLevel());
		} else if (CURRENT_STATISTICS_COMMANDS.contains(mainCommand)) {
			System.out.println(this);
		} else if (SPY_COMMANDS.contains(mainCommand)) {
			int oppId = parseFirstArgumentAsInteger(commandParts);
			if (oppId >= 0) {
				Fruit opponent = organiser.getOpponent(oppId);
				System.out.println(opponent);
			}
		} else if (STOP_GAME_COMMANDS.contains(mainCommand)) {
			continuePlaying = false;
			System.out.println("Notifying game to stop...");
		} else if (DETAILED_LIST_COMMANDS.contains(mainCommand)) {
			int pageId = parseFirstArgumentAsInteger(commandParts);
			if (pageId >= 0) {
				organiser.listDetailsAtPage(pageId);
			}
		} else {
			System.out.println("The command '" + command + "' was not recognised.");
			showHelpInformation();
		}
	}

	private void parseAttackCommand(String[] commandParts) {
		int oppId = parseFirstArgumentAsInteger(commandParts);
		if (oppId < 0) {
			attackRandomOpponent();
		} else {
			Fruit opponent = organiser.getOpponent(oppId);
			if (opponent == null) {
				System.out.println("No opponent could be found for id " + oppId);
			} else {
				attack(opponent);
			}
		}
	}

	private int parseFirstArgumentAsInteger(String[] commandParts) {
		if (commandParts.length < 2) {
			return -1;
		}

		try {
			return Integer.parseInt(commandParts[1]);
		} catch (NumberFormatException nfe) {
			System.out.println("Could not read the provided argument '" + commandParts[1] + "'.");
			return -1;
		}
	}

	private void parseUpgradeCommand(String command, String[] commandParts) {
		if (commandParts.length < 2) {
			System.out.println("Missing component to upgrade.");
		} else if (UPGRADE_TECH_COMMANDS.contains(commandParts[1])) {
			System.out.println("Upgrading technology");
			upgradeTechnology();
		} else if (UPGRADE_DEFENSE_COMMANDS.contains(commandParts[1])) {
			System.out.println("Upgrading defense");
			upgradeDefence();
		} else if (UPGRADE_OFFENSE_COMMANDS.contains(commandParts[1])) {
			System.out.println("Upgrading offense");
			upgradeOffence();
		} else if (UPGRADE_COW_COMMANDS.contains(commandParts[1])) {
			System.out.println("Trying to buy a cow");
			buyCow();
		} else {
			System.out.println("Unrecognised component '" + commandParts[1] + "' in command '" + command + "'.");
		}
	}

	public void showHelpInformation() {
		System.out.println("  To attack, use one of the following commands, followed by the position of the opponent:");
		listCommands(ATTACK_COMMANDS);
		System.out.println("  To upgrade, use one of the following commands, followed by the component to upgrade:");
		listCommands(UPGRADE_COMMANDS);
		System.out.print("    Technology:");
		listCommands(UPGRADE_TECH_COMMANDS);
		System.out.print("    Defense:");
		listCommands(UPGRADE_DEFENSE_COMMANDS);
		System.out.print("    Offense:");
		listCommands(UPGRADE_OFFENSE_COMMANDS);
		System.out.print("    Cows:");
		listCommands(UPGRADE_COW_COMMANDS);
		System.out.println("  Listing opponents:");
		listCommands(LIST_OPPONENTS_COMMANDS);
		System.out.println("  Showing statistics:");
		listCommands(CURRENT_STATISTICS_COMMANDS);
		System.out.println("  To stop playing, enter:");
		listCommands(STOP_GAME_COMMANDS);
	}

	private void listCommands(Collection<String> commands) {
		for (String command : commands) {
			System.out.print('\t');
			System.out.print(command);
		}
		System.out.println();
	}

}
