package fruits;

import game.FruitOrganiser;

/**
 * Created by kurck on 15-2-16.
 */
public class Orange extends Fruit {

	public Orange(FruitOrganiser parent) {
		super(parent);
	}

	@Override
	protected void doSomething() {
		double rnd = Math.random();
		if (rnd < 0.15) {
			upgradeTechnology();
		} else if (rnd < 0.3) {
			upgradeOffence();
		} else if (rnd < 0.45) {
			attackRandomOpponent();
		} else if (rnd < 0.65) {
			buyCow();
		}
		// 35% chance on idle time.
	}
}
