package game;

/**
 * Created by kurck on 15-2-16.
 */
public class NameGenerator {

	private static final String[] PREFIXES = { "King", "Private", "Dr.", "Knight", "Queen", "Prof.", "Prince", "Princess", "Duke", "Farmer", "Uncle" };
	private static final String[] FIRST_NAMES = { "Han", "Leia", "Obi", "Peter", "Kyle", "Rebecca", "Simon", "Aeron", "Bomber", "Dave", "Dina", "Albert", "Anna", "Ben", "Bella", "Curt", "Cecile", "Edward", "Elisa", "Frank", "Felicia", "Tom", "Kurck", "Gina", "Travis", "Mandy", "Melissa", "James", "Jimmy" };
	private static final String[] LAST_NAMES = { "Solo", "Skywalker", "Horrible", "Says", "Williams", "Manneke", "Gustavson", "Honda", "McLaren", "Randal", "Peterson", "Cage", "Vanderbilt", "Iniesta", "Trump", "Obama", "Clinton", "Cameron", "Lucas", "Kongolo", "Day", "Cooper", "Carr" };

	/**
	 * Generates a new random name based on three parts.
	 * @return the generated name.
	 */
	public static String generate() {
		StringBuilder newName = new StringBuilder();
		newName.append(getRandomEntry(PREFIXES));
		newName.append(' ');
		newName.append(getRandomEntry(FIRST_NAMES));
		newName.append(' ');
		newName.append(getRandomEntry(LAST_NAMES));
		String fullName = newName.toString();
		return fullName;
	}

	private static String getRandomEntry(String[] entries) {
		int position = (int) (Math.random() * entries.length);
		String selectedEntry = entries[position];
		return selectedEntry;
	}

}
