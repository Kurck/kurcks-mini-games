package game;

import java.util.Arrays;

import fruits.Apple;
import fruits.Banana;
import fruits.Fruit;
import fruits.Kiwi;
import fruits.Orange;
import fruits.Peach;
import fruits.Pineapple;

/**
 * Created by kurck on 15-2-16.
 */
public class FruitOrganiser implements Runnable {

	public static final int PAGE_SIZE = 50;

	private Fruit[] fruit;
	private Kiwi humanControlled;
	private int cycles = 0;
	private boolean runGame = true;

	public Kiwi getKiwi() { return humanControlled; }

	public FruitOrganiser(int size) {
		fruit = new Fruit[size];

		// Populate the array of fruit.
		int bottom = size - 1;
		for (int i = 0; i < bottom; i++) {
			double rnd = Math.random();
			Fruit newFruit;
			if (rnd < 0.05) {
				newFruit = new Apple(this);
			} else if (rnd < 0.1) {
				newFruit = new Peach(this);
			} else if (rnd < 0.4) {
				newFruit = new Banana(this);
			} else if (rnd < 0.7) {
				newFruit = new Pineapple(this);
			} else {
				newFruit = new Orange(this);
			}
			newFruit.setPosition(i);
			fruit[i] = newFruit;
		}

		// The human player will start at the bottom of the list and needs to fight to the top.
		humanControlled = new Kiwi(this);
		humanControlled.setPosition(bottom);
		fruit[bottom] = humanControlled;
	}

	private void update() {
		// Update the fruit collection from back to front, allowing the items with a lower score to attack first.
		for (int i = fruit.length - 1; i >= 0; i--) {
			Fruit item = fruit[i];
			item.update();
		}

		// Every now and then, the whole list is shuffled.
		cycles++;
		if (cycles % 64 == 0) {
			sort();
		}
	}

	/**
	 * Fetches an opponent near the current position (on the same 'page'), with a small chance of returning any fruit on the list.
	 * @param currentPosition the position of the attacker/caller.
	 * @return the found opponent.
	 */
	public Fruit getRandomOpponent(int currentPosition) {
		// Get the opponent. If the generated position is the caller's, return null.
		if (currentPosition < 0 || currentPosition >= fruit.length) {
			System.err.format("The provided position %d is out of bounds.\n", currentPosition);
			return null;
		}
		int pageStartsAt = PAGE_SIZE * (currentPosition / PAGE_SIZE);
		int opponentPosition;
		if (Math.random() < 0.01) {
			opponentPosition = (int) (Math.random() * fruit.length);
		} else {
			opponentPosition = pageStartsAt + (int) (Math.random() * PAGE_SIZE);
		}
		if (opponentPosition == currentPosition) {
			return null;
		} else {
			return fruit[opponentPosition];
		}
	}

	public Fruit getOpponent(int position) {
		if (position < 0 || position >= fruit.length) {
			return null;
		} else {
			return fruit[position];
		}
	}

	public Fruit getHighValueOpponentWithLowTech(int currentPosition, int maxTechLevel) {
		if (currentPosition < 0 || currentPosition >= fruit.length) {
			System.err.format("The provided position %d is out of bounds.\n", currentPosition);
			return null;
		}
		int pageStartsAt = PAGE_SIZE * (currentPosition / PAGE_SIZE);
		int nextPageStartsAt = pageStartsAt + PAGE_SIZE;
		Fruit highestValue = null;
		int highestCredits = 0;
		for (int i = pageStartsAt; i < nextPageStartsAt; i++) {
			// Ensure the opponent is not you, has a tech level lower or equal to maxTechLevel, and has more credits than the previous top.
			if (i == currentPosition) {
				continue;
			}
			Fruit opponent = fruit[i];
			if (opponent.getTechLevel() > maxTechLevel) {
				continue;
			}
			if (opponent.getCredits() > highestCredits) {
				highestValue = opponent;
				highestCredits = opponent.getCredits();
			}
		}
		return highestValue;
	}

	/**
	 * Sorts all fruit based on their scores and updates the internally stored position.
	 */
	private void sort() {
		Arrays.sort(fruit);
		for (int i = 0; i < fruit.length; i++) {
			Fruit item = fruit[i];
			item.setPosition(i);
		}
		System.out.println("The fruit order has been updated.");
	}

	public void listOpponents(int position, int maxTechLevel) {
		// Note that the page number is rounded down.
		int startOfPage = PAGE_SIZE * (position / PAGE_SIZE);
		int nextPageStartsAt = startOfPage + PAGE_SIZE;
		for (int i = startOfPage; i < nextPageStartsAt; i++) {
			Fruit opponent = fruit[i];
			if (opponent.getTechLevel() <= maxTechLevel) {
				System.out.println(opponent.toShortString());
			}
		}
	}

	public void listDetailsAtPage(int page) {
		int startOfPage = PAGE_SIZE * page;
		if (page >= 0 && startOfPage < fruit.length) {
			int nextPageStartsAt = startOfPage + PAGE_SIZE;
			for (int i = startOfPage; i < nextPageStartsAt; i++) {
				Fruit opponent = fruit[i];
				System.out.println(opponent.toDetailedString());
			}
		}
	}

	@Override
	public void run() {
		while (humanControlled.wantsToContinue() && runGame) {
			try {
				Thread.sleep(1500L);
			} catch (InterruptedException ie) {
				System.err.println("Failed to wait for the next step.");
				ie.printStackTrace();
			}
			update();
		}
	}

	public void stopGame() {
		runGame = false;
	}
}
