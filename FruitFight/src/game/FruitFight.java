package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fruits.Kiwi;

public class FruitFight {

	private static final int NUMBER_OF_COMPETING_FRUITS = 1000;

	public static void main(String... args) {
		// Show logo and provide a quick intro.
		System.out.println();
		System.out.println("FFFFFFFFF RRRRRR  UUU   UUU IIIIII TTTTTTTT      FFFFFFFFF IIIIII  GGGGGGG HH   HHH TTTTTTTT");
		System.out.println("FFFFFFFFF RRRRRRR  UU   UUU IIIIII TTTTTTTT      FFFFFFFFF IIIIII GGGGGGGG HH   HHH TTTTTTTT");
		System.out.println("FFF       RR   RRR UU   UUU  III     TTT         FFF        III   GGG      HH   HHH   TTT");
		System.out.println("FFF      RRR   RRR UU   UUU  III     TTT         FFF        III  GGG       HH   HHH   TTT");
		System.out.println("FFFFFF   RRRRRRR   UU   UUU  III     TTT         FFFFFF     III  GGG  GGGG HHHHHHHH   TTT");
		System.out.println("FFFFFF   RRRRRRR   UU   UUU  III     TTT         FFFFFF     III  GGG  GGGG HHHHHHHH   TTT");
		System.out.println("FFF      RRR   RRR UU   UUU  III     TTT         FFF        III  GGG   GGG HH   HHH   TTT");
		System.out.println("FFF      RRR   RRR UUU UUUU  III     TTT         FFF        III   GGG  GGG HH   HHH   TTT");
		System.out.println("FFF      RRR   RRR UUUUUUU  IIIIII   TTT         FFF      IIII by Kurck Gustavson, 2016 T");
		System.out.println("FFF      RRR   RRR  UUUUU  IIIIIII   TTT         FFF      IIIIIII  GGGGGGG HH   HHH   TTT");
		System.out.println();

		// Populate the battle grounds.
		FruitOrganiser organiser = new FruitOrganiser(NUMBER_OF_COMPETING_FRUITS);
		Kiwi humanPlayer = organiser.getKiwi();

		System.out.println("In the fruit bowl rages a terrifying war. Your mission as a kiwi is not to get smushed.");
		System.out.println("To do so, enter commands like 'list', 'attack 782' and 'upgrade cows':\n");
		humanPlayer.showHelpInformation();
		System.out.println("\nThese are your statistics (available with the command 's'):");
		System.out.println(humanPlayer);
		System.out.println("All hell is about to break loose. Good luck kiwi!");
		System.out.println("- Oh, btw, don't trust peaches and apples. They are the worst. -");
		System.out.println();

		// Start the game and read commands from the input stream.
		try (InputStreamReader input = new InputStreamReader(System.in);
				BufferedReader commandFeed = new BufferedReader(input)) {

			// Start updating (and attacking) as soon as the player decides to hit Enter.
			System.out.println("Hit Enter to start the game.");
			commandFeed.readLine();
			System.out.println("Starting game...");
			Thread aorta = new Thread(organiser);
			aorta.start();

			// Read commands from ze hooman as long as the game runs.
			while (aorta.isAlive() && humanPlayer.wantsToContinue()) {
				String command = commandFeed.readLine();
				humanPlayer.executeCommand(command);
			}
		} catch (IOException ioe) {
			System.err.println("Failed to read from the command input stream. The game needs to close down.");
			ioe.printStackTrace();
			organiser.stopGame();
		}

		// End
		System.out.println("The game ended.");
		System.out.println(humanPlayer);
		System.out.println("Thank you for playing!");
		System.out.println("  @..@");
		System.out.println(" (----)");
		System.out.println("( |__| )");
		System.out.println("^^^  ^^^");
		System.out.println("~ Kurck Gustavson");
	}

}