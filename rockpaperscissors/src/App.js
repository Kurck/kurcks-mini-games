import { useState } from 'react';
import './App.css';

function App() {

	const DEFAULT_HANDS = [
		{ id: 'rock', defeats: ['scissors', 'lizard'] },
		{ id: 'paper', defeats: ['rock', 'spock'] },
		{ id: 'scissors', defeats: ['paper', 'lizard'] }
	];
	const EXPERT_HANDS = [
		...DEFAULT_HANDS,
		{ id: 'lizard', defeats: ['spock', 'paper'] },
		{ id: 'spock', defeats: ['scissors', 'rock'] }
	];

	const [ gameMode, setGameMode ] = useState('default');
	const [ userHand, setUserHand ] = useState(null);
	const [ computerHand, setComputerHand ] = useState(null);
	const [ hands, setHands ] = useState(DEFAULT_HANDS);
	const [ matchResult, setMatchResult ] = useState(null);
	const [ wins, setWins ] = useState(0);
	const [ draws, setDraws ] = useState(0);
	const [ losses, setLosses ] = useState(0);

	const selectGameMode = (mode) => {
		setGameMode(mode);
		setHands(mode === 'expert' ? EXPERT_HANDS : DEFAULT_HANDS);
	}

	const selectHand = (userChoice) => {
		setUserHand(userChoice);
		const computerChoice = hands[Math.floor(Math.random() * hands.length)];
		setComputerHand(computerChoice);
		if (userChoice === computerChoice) {
			setMatchResult('draw');
			setDraws(draws + 1);
		} else if (userChoice.defeats.includes(computerChoice.id)) {
			setMatchResult('win');
			setWins(wins + 1);
		} else {
			setMatchResult('loss');
			setLosses(losses + 1);
		}
	}

	return (
		<div>
			<h3>Game mode: {gameMode}</h3>
			{ gameMode === 'expert'
				? <button onClick={() => selectGameMode('default')}>Change to normal</button>
				: <button onClick={() => selectGameMode('expert')}>Change to expert</button>}
			<h3>Choose a hand:</h3>
			{hands.map((hand, index) => <button key={index} className={userHand === hand ? 'selected' : 'regular'} onClick={() => selectHand(hand)}>{hand.id}</button>)}
			<h3>Opponent's hand: {computerHand ? computerHand.id : ''}</h3>
			<h3>{matchResult === 'win' ? 'You won :D' : matchResult === 'loss' ? 'You lost :(' : 'It\'s a draw!' }</h3>
			<table>
				<tr><th>Wins</th><th>Draws</th><th>Losses</th></tr>
				<tr><td>{wins}</td><td>{draws}</td><td>{losses}</td></tr>
			</table>
		</div>
	);
}

export default App;
