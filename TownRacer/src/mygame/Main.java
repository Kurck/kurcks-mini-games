package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.font.*;
import com.jme3.input.*;
import com.jme3.input.controls.*;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.*;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FogFilter;
import com.jme3.scene.*;
import com.jme3.system.AppSettings;
import com.jme3.ui.Picture;
import java.util.*;

public class Main extends SimpleApplication implements ActionListener {

	private BulletAppState bulletAppState;
	private Node carNode;
	private VehicleControl player;
	// Due to compiling and decompiling, the constants were filled in.
	private static final int SPEED_DIF = 1600;
	private static final int BRAKE_DIF = 800;
	private static final float COMP = 0.2f;
	private static final float DAMP = 0.3f;
	private static final float STIFFNESS = 120f;
	private static final float MASS = 600f;
	private float wheelRadius;
	private float steeringValue;
	private float accelerationValue;
	private static final float COLLISION_DISTANCE = 3f;
	private static final float BOOK_ROTATION = 2f;
	private static final float BOOK_HEIGHT = 2f;
	private static final String BOOK_TYPE = "bookType";
	private Material blueMat;
	private List<Spatial> books;
	private int blueBooks;
	private BitmapText blueCountText;
	private int redBooks;
	private BitmapText redCountText;
	private Picture blueWin;
	private Picture redWin;
	private AudioNode jeej;
	private AudioNode plop;
	private AudioNode toet;

	public Main() {
		steeringValue = 0.0F;
		accelerationValue = 0.0F;
		blueMat = null;
		books = new ArrayList();
		blueBooks = 0;
		redBooks = 0;
	}

	public static void main(String args[]) {
		Main app = new Main();
		AppSettings cfg = new AppSettings(true);
		cfg.setTitle("Mystical Motors");
		cfg.setSettingsDialogImage("Interface/townSplash.png");
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		cfg.setFrameRate(60);
		cfg.setVSync(true);
		cfg.setFrequency(60);
		cfg.setResolution(1280, 1024);
		cfg.setFullscreen(false);
		cfg.setSamples(2);
		app.setSettings(cfg);
		app.start();
	}

	public void simpleInitApp() {
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		Spatial scene = assetManager.loadModel("Models/HKA-town.j3o");
		scene.setShadowMode(com.jme3.renderer.queue.RenderQueue.ShadowMode.CastAndReceive);
		rootNode.attachChild(scene);
		com.jme3.bullet.collision.shapes.CollisionShape sceneShape = CollisionShapeFactory.createMeshShape(scene);
		RigidBodyControl landscape = new RigidBodyControl(sceneShape, 0.0F);
		scene.addControl(landscape);
		bulletAppState.getPhysicsSpace().add(landscape);
		buildPlayer((Node) scene);
		addBooks((Node) scene);
		PointLight sun = new PointLight();
		sun.setPosition(new Vector3f(10F, 100F, 30F));
		scene.addLight(sun);
		flyCam.setEnabled(false);
		cam.setFrustumFar(200F);
		ChaseCamera chaseCam = new ChaseCamera(cam, carNode);
		chaseCam.setDefaultHorizontalRotation(1.570796F);
		chaseCam.setDefaultVerticalRotation(0.2617994F);
		setupKeys();
		loadSounds();
		FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
		FogFilter fog = new FogFilter();
		fog.setFogColor(ColorRGBA.White);
		fog.setFogDistance(180F);
		fog.setFogDensity(1.5F);
		fpp.addFilter(fog);
		viewPort.addProcessor(fpp);
		guiNode.detachAllChildren();
		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		createText(15, 15, "Blue books:");
		blueCountText = createText(110, 15, "0");
		createText(15, 35, "Red books:");
		redCountText = createText(110, 35, "0");
		blueWin = loadWin("Blue Win", "Interface/blueWin.png");
		redWin = loadWin("Red Win", "Interface/redWin.png");
	}

	private void loadSounds() {
		jeej = loadSound("Sounds/jeej.ogg", false);
		plop = loadSound("Sounds/plop.ogg", false);
		toet = loadSound("Sounds/toet.ogg", false);
		AudioNode music = loadSound("Sounds/er.ogg", true);
		music.play();
	}

	private AudioNode loadSound(String path, boolean looping) {
		AudioNode sound = new AudioNode(assetManager, path, false);
		sound.setPositional(false);
		sound.setLooping(looping);
		sound.setVolume(2.0F);
		rootNode.attachChild(sound);
		return sound;
	}

	private Picture loadWin(String name, String path) {
		Picture win = new Picture(name);
		win.setImage(assetManager, path, true);
		win.setWidth(settings.getWidth() / 2);
		win.setHeight(settings.getHeight() / 3);
		win.setPosition(settings.getWidth() / 4, settings.getHeight() / 4);
		return win;
	}

	private BitmapText createText(int x, int y, String value) {
		BitmapText text = new BitmapText(guiFont, false);
		text.setSize(guiFont.getCharSet().getRenderedSize());
		text.setText(value);
		text.setLocalTranslation(x, (float) y + text.getLineHeight(), 0.0F);
		guiNode.attachChild(text);
		return text;
	}

	private void setupKeys() {
		inputManager.addMapping("Lefts", new Trigger[]{new KeyTrigger(203)});
		inputManager.addMapping("Rights", new Trigger[]{new KeyTrigger(205)});
		inputManager.addMapping("Ups", new Trigger[]{new KeyTrigger(200)});
		inputManager.addMapping("Downs", new Trigger[]{new KeyTrigger(208)});
		inputManager.addMapping("Space", new Trigger[]{new KeyTrigger(57)});
		inputManager.addMapping("Reset", new Trigger[]{new KeyTrigger(19)});
		inputManager.addListener(this, new String[]{"Lefts", "Rights", "Ups", "Downs", "Space", "Reset"});
	}

	private void buildPlayer(Node scene) {
		carNode = (Node) assetManager.loadModel("Models/Ferrari/Car.j3o");
		carNode.setShadowMode(com.jme3.renderer.queue.RenderQueue.ShadowMode.Cast);
		Geometry chasis = findGeom(carNode, "Car");
		com.jme3.bullet.collision.shapes.CollisionShape carHull = CollisionShapeFactory.createDynamicMeshShape(chasis);
		player = new VehicleControl(carHull, MASS);
		carNode.addControl(player);
		player.setSuspensionCompression(COMP * FastMath.sqrt(STIFFNESS));
		player.setSuspensionDamping(DAMP * FastMath.sqrt(STIFFNESS));
		player.setSuspensionStiffness(STIFFNESS);
		player.setMaxSuspensionForce(10000F);
		Vector3f wheelDirection = new Vector3f(0.0F, -1F, 0.0F);
		Vector3f wheelAxle = new Vector3f(-1F, 0.0F, 0.0F);
		Geometry wheel_fr = findGeom(carNode, "WheelFrontRight");
		wheel_fr.center();
		BoundingBox box = (BoundingBox) wheel_fr.getModelBound();
		wheelRadius = box.getYExtent();
		float back_wheel_h = wheelRadius * 1.7F - 1.0F;
		float front_wheel_h = wheelRadius * 1.9F - 1.0F;
		player.addWheel(wheel_fr.getParent(), box.getCenter().add(0.0F, -front_wheel_h, 0.0F), wheelDirection, wheelAxle, 0.2F, wheelRadius, true);
		addWheel(wheelDirection, wheelAxle, "WheelFrontLeft", -front_wheel_h, true);
		addWheel(wheelDirection, wheelAxle, "WheelBackRight", -back_wheel_h, false);
		addWheel(wheelDirection, wheelAxle, "WheelBackLeft", -back_wheel_h, false);
		scene.attachChild(carNode);
		bulletAppState.getPhysicsSpace().add(player);
	}

	private void addWheel(Vector3f wheelDirection, Vector3f wheelAxle, String name, float y, boolean steers) {
		Geometry wheel = findGeom(carNode, name);
		wheel.center();
		BoundingBox box = (BoundingBox) wheel.getModelBound();
		player.addWheel(wheel.getParent(), box.getCenter().add(0.0F, y, 0.0F), wheelDirection, wheelAxle, 0.2F, wheelRadius, steers);
	}

	private Geometry findGeom(Spatial spatial, String name) {
		if (spatial instanceof Node) {
			Node node = (Node) spatial;
			for (int i = 0; i < node.getQuantity(); i++) {
				Spatial child = node.getChild(i);
				Geometry result = findGeom(child, name);
				if (result != null) {
					return result;
				}
			}
		} else if ((spatial instanceof Geometry) && spatial.getName().startsWith(name)) {
			return (Geometry) spatial;
		}
		return null;
	}

	public void onAction(String binding, boolean value, float tpf) {
		if (binding.equals("Lefts")) {
			steeringValue += value ? 0.5F : -0.5F;
			player.steer(steeringValue);
		} else if (binding.equals("Rights")) {
			steeringValue += value ? -0.5F : 0.5F;
			player.steer(steeringValue);
		} else if (binding.equals("Ups")) {
			accelerationValue += value ? -SPEED_DIF : SPEED_DIF;
			player.accelerate(accelerationValue);
		} else if (binding.equals("Downs")) {
			accelerationValue += value ? BRAKE_DIF : -BRAKE_DIF;
			player.accelerate(accelerationValue);
		} else if (binding.equals("Reset")) {
			if (value) {
				accelerationValue = 0.0F;
				player.setPhysicsLocation(Vector3f.ZERO);
				player.setPhysicsRotation(new Matrix3f());
				player.setLinearVelocity(Vector3f.ZERO);
				player.setAngularVelocity(Vector3f.ZERO);
				player.resetSuspension();
			}
		} else if (value && binding.equals("Space")) {
			int rnd = FastMath.nextRandomInt(0, 2);
			if (rnd == 0) {
				plop.play();
			} else if (rnd == 1) {
				jeej.play();
			} else {
				toet.play();
			}
		}
	}

	@Override
	public void simpleUpdate(float tpf) {
		float rotation = BOOK_ROTATION * tpf;
		List<Spatial> deletables = new ArrayList<Spatial>();
		for (Spatial book : books) {
			float d = book.getWorldTranslation().distance(carNode.getWorldTranslation());
			if (d < COLLISION_DISTANCE) {
				score(book);
				deletables.add(book);
			} else {
				book.rotate(rotation, rotation, rotation);
			}
		}
		for (Spatial deleted : deletables) {
			deleted.removeFromParent();
			books.remove(deleted);
		}
	}

	private void score(Spatial book) {
		Boolean bookType = (Boolean) book.getUserData(BOOK_TYPE);
		plop.play();
		if (Boolean.TRUE.equals(bookType)) {
			blueBooks++;
			blueCountText.setText(Integer.toString(blueBooks));
			if (blueBooks == 9) {
				guiNode.attachChild(blueWin);
				jeej.play();
				paused = true;
			}
		} else {
			redBooks++;
			redCountText.setText(Integer.toString(redBooks));
			if (redBooks == 9) {
				guiNode.attachChild(redWin);
				jeej.play();
				paused = true;
			}
		}
	}

	private void addBooks(Node parent) {
		blueMat = assetManager.loadMaterial("Materials/BlueBook.j3m");
		createBook(parent, Boolean.TRUE, new Vector3f(5F, BOOK_HEIGHT, -20F));
		createBook(parent, Boolean.FALSE, new Vector3f(45.12372F, BOOK_HEIGHT, -55.67119F));
		createBook(parent, Boolean.FALSE, new Vector3f(60.49716F, BOOK_HEIGHT, -37.14453F));
		createBook(parent, Boolean.FALSE, new Vector3f(61.05171F, BOOK_HEIGHT, -27.48885F));
		createBook(parent, Boolean.TRUE, new Vector3f(70.07639F, BOOK_HEIGHT, 11.2823F));
		createBook(parent, Boolean.FALSE, new Vector3f(44.04437F, BOOK_HEIGHT, -72.59648F));
		createBook(parent, Boolean.FALSE, new Vector3f(-37.20469F, BOOK_HEIGHT, -49.87758F));
		createBook(parent, Boolean.FALSE, new Vector3f(-44.50374F, BOOK_HEIGHT, -3.01887F));
		createBook(parent, Boolean.FALSE, new Vector3f(-12.96959F, BOOK_HEIGHT, 60.73855F));
		createBook(parent, Boolean.TRUE, new Vector3f(5.660543F, BOOK_HEIGHT, 97.01031F));
		createBook(parent, Boolean.TRUE, new Vector3f(32.12542F, BOOK_HEIGHT, 81.5125F));
		createBook(parent, Boolean.FALSE, new Vector3f(64.943F, BOOK_HEIGHT, 53.5264F));
		createBook(parent, Boolean.TRUE, new Vector3f(79.95737F, BOOK_HEIGHT, 44.56189F));
		createBook(parent, Boolean.TRUE, new Vector3f(70.45358F, BOOK_HEIGHT, -55.46691F));
		createBook(parent, Boolean.TRUE, new Vector3f(1.708759F, BOOK_HEIGHT, -53.80676F));
		createBook(parent, Boolean.FALSE, new Vector3f(7.445362F, BOOK_HEIGHT, 35.45949F));
		createBook(parent, Boolean.TRUE, new Vector3f(42.64879F, BOOK_HEIGHT, 37.98483F));
		createBook(parent, Boolean.TRUE, new Vector3f(35.89121F, BOOK_HEIGHT, -14.91813F));
	}

	private void createBook(Node parent, boolean blueBook, Vector3f location) {
		Spatial book = assetManager.loadModel("Models/SimpleRedBook.j3o");
		if (blueBook) {
			book.setMaterial(blueMat);
		}
		book.setLocalTranslation(location);
		books.add(book);
		book.setUserData("bookType", blueBook);
		parent.attachChild(book);
	}

}
