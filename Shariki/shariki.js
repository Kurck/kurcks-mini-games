document.addEventListener("DOMContentLoaded", () => {

	// Game state displays

	class Display {
		constructor(id, initialValue) {
			this.initialValue = initialValue;
			this.element = document.getElementById(id);
			this.reset();
		}

		decreaseBy(value) {
			this.incrementBy(-value);
		}

		incrementBy(value) {
			this.value += value;
			this.element.innerText = this.value;
		}

		reset() {
			this.value = this.initialValue;
			this.element.innerText = this.value;
		}
	}
	let level = new Display("level", 0);
	let score = new Display("score", 0);
	let tilesLeft = new Display("tilesLeft", 356);

	// Gem creation

	const GRID = document.getElementById("grid");
	const ROWS = 10;
	const COLUMNS = 10;
	const EMPTY_TYPE = { image: null, value: 0 };
	const GEM_TYPES = [
		{ image: "url('images/circle.png')", value: 1 },
		{ image: "url('images/square.png')", value: 2 },
		{ image: "url('images/triangle.png')", value: 3 },
		{ image: "url('images/diamond.png')", value: 4 },
		{ image: "url('images/drop.png')", value: 5 },
		{ image: "url('images/octagon.png')", value: 6 },
		{ image: "url('images/stopsign.png')", value: 7 },
		{ image: "url('images/pentagon.png')", value: 8 },
		{ image: "url('images/star.png')", value: 9 },
	];
	class Gem {
		constructor(row, column) {
			this.row = row;
			this.column = column;

			this.visual = document.createElement("div");
			this.visual.setAttribute("draggable", true);
			this.visual.addEventListener("dragstart", () => { dragStart(row, column); });
			this.visual.addEventListener("dragend", (event) => { dragEnd(event); });
			this.visual.addEventListener("dragover", (event) => { event.preventDefault(); });
			this.visual.addEventListener("dragenter", (event) => { trySwap(event, row, column); });
			this.visual.addEventListener("dragleave", (event) => { event.preventDefault(); });
			this.visual.addEventListener("drop", (event) => { event.preventDefault(); });

			this.setRandomType();
			GRID.appendChild(this.visual);
		}

		setRandomType() {
			let maxType = Math.min(level.value + 4, GEM_TYPES.length);
			this.setType(GEM_TYPES[Math.floor(Math.random() * maxType)]);
			tilesLeft.decreaseBy(1);
		}

		setType(type) {
			this.type = type;
			this.visual.style.backgroundImage = this.type.image;
		}
	}
	let gems;
	let draggedGem;
	function createGems() {
		gems = [];
		for (let row = 0; row < ROWS; row++) {
			gems[row] = [];
			for (let col = 0; col < COLUMNS; col++) {
				gems[row][col] = new Gem(row, col);
			}
		}
		draggedGem = null;
	}
	function dropNewGems() {
		for (let column = 0; column < COLUMNS; column++) {
			let gem = gems[0][column];
			if (gem.type == EMPTY_TYPE) {
				gem.setRandomType();
			}
		}
	}
	createGems();

	// Swapping

	function dragStart(row, column) {
		draggedGem = gems[row][column];
	}
	function dragEnd(event) {
		event.preventDefault();
		draggedGem = null;
	}
	function trySwap(event, row, column) {
		event.preventDefault();
		if (isValidMove(row, column)) {
			let other = gems[row][column];
			swap(draggedGem, other);
			// Swap back if it doesn't result in combos
			let combos = findCombos();
			if (combos.length === 0) {
				swap(draggedGem, other);
				GRID.style.borderColor = "#A00";
				setTimeout(() => { GRID.style.borderColor = null; }, 400);
			}
			draggedGem = null;
		}
	}
	function isValidMove(row, column) {
		if (!draggedGem) {
			return false;
		}
		return (row === draggedGem.row && Math.abs(column - draggedGem.column) === 1)
			|| (Math.abs(row - draggedGem.row) === 1 && column === draggedGem.column);
	}
	function swap(gemA, gemB) {
		let typeA = gemA.type;
		gemA.setType(gemB.type);
		gemB.setType(typeA);
	}

	// Combos & scoring

	function findCombos() {
		let combos = [];
		addCombosFromLines(combos, true);
		addCombosFromLines(combos, false);
		return combos;
	}
	function addCombosFromLines(combos, horizontal) {
		let iMax = horizontal ? ROWS : COLUMNS;
		let jMax = horizontal ? COLUMNS : ROWS;
		for (let i = 0; i < iMax; i++) {
			let currentType = null;
			let combo = [];
			for (let j = 0; j < jMax; j++) {
				let gem = horizontal ? gems[i][j] : gems[j][i];
				if (gem.type != currentType) {
					if (combo.length > 2) {
						combos.push(combo);
					}
					combo = [];
					currentType = gem.type;
				}
				combo.push(gem);
			}
			if (combo.length > 2) {
				combos.push(combo);
			}
		}
	}
	function applyGravity() {
		for (let column = 0; column < COLUMNS; column++) {
			for (let row = ROWS - 1; row > 0; row--) {
				let current = gems[row][column];
				let oneUp = gems[row - 1][column];
				if (current.type === EMPTY_TYPE && oneUp.type !== EMPTY_TYPE) {
					swap(current, oneUp);
				}
			}
		}
	}

	// Game loop

	function updateGems() {
		let combos = findCombos();
		combos.forEach(combo => {
			let comboScore = Math.pow(combo[0].type.value, combo.length);
			score.incrementBy(comboScore);
			combo.forEach(gem => gem.setType(EMPTY_TYPE));
		});
		applyGravity();
		dropNewGems();
		if (tilesLeft.value <= 0) {
			level.incrementBy(1);
			tilesLeft.incrementBy(Math.pow(2, 7 + level.value));
		}
	}
	window.setInterval(updateGems, 200);

});