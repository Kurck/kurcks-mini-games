package game;

public class Weapon {

	public String name;
	public float minLevel;
	public boolean requiresStamina;
	public Effect[] results;

	public Weapon(String newName, float newMinLevel, Effect... effects) {
		this(newName, newMinLevel, false, effects);
	}

	public Weapon(String newName, float newMinLevel, boolean newRequiresStamina, Effect... effects) {
		name = newName;
		minLevel = newMinLevel;
		requiresStamina = newRequiresStamina;
		results = effects;
	}

	public float calculateInitialStrength(Fighter source, Fighter target) {
		source.attack = 0f;
		for (Effect effect : results) {
			if (effect.isAttack()) {
				effect.applyTo(source, target);
			}
		}
		return source.attack;
	}

	public void alterOpponentsAttack(Fighter source, Fighter target) {
		for (Effect effect : results) {
			if (effect.isShield()) {
				effect.applyTo(source, target);
			}
		}
	}

	public void applyOtherEffects(Fighter source, Fighter target) {
		for (Effect effect : results) {
			if (!effect.hasBeenApplied()) {
				effect.applyTo(source, target);
			}
		}
	}

}
