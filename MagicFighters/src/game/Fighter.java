package game;

import java.io.Serializable;

public class Fighter implements Serializable {

	private static final long serialVersionUID = 3820366844411414449L;
	private static final String[] OPPONENT_NAMES = { "Buddy", "Meg", "Mug", "Incisor", "Hurricane", "Knight" };

	public String name;
	public float level;
	public float health;
	public int stamina;
	public float attack;

	public Fighter() {
		setName("Player");
		resetLevel();
		resetHealth();
		resetStamina();
	}

	private Fighter(Fighter player, float strength) {
		this.name = OPPONENT_NAMES[(int) (Math.random() * OPPONENT_NAMES.length)];
		this.level = (float) (2 * player.level * Math.max(Math.random(), strength));
		this.health = (1.5f + strength) * player.level + strength * player.health;
		this.stamina = 1 + (int) (player.stamina * Math.random());
	}

	public void setName(String newName) {
		name = newName;
	}

	public float resetLevel() {
		level = 1f;
		return level;
	}

	public float resetHealth() {
		health = 20f;
		return health;
	}

	private int resetStamina() {
		stamina = 10;
		return stamina;
	}

	public void prepareForFight() {
		if (health <= 0) {
			resetHealth();
		}
		health += 5 + level;
		stamina += 4;
	}

	public Fighter getOpponent(float strength) {
		Fighter opponent = new Fighter(this, strength);
		return opponent;
	}

	public boolean isDead() {
		return health <= 0;
	}

	public void addWin(Fighter opponent) {
		this.level += opponent.level / 4;
	}

	public boolean canUse(Weapon item) {
		boolean sufficientLevel = item.minLevel <= level;
		boolean enoughStamina = !item.requiresStamina || stamina > 0;
		return sufficientLevel && enoughStamina;
	}

	public void updateStamina(Weapon weapon) {
		if (weapon.requiresStamina) {
			stamina--;
		}
	}

}
