package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MagicFighters extends Thread {

	public static final float versionNumber = 5.0f;
	private static MagicFighters gameInstance;

	public static MagicFighters getInstance() { return gameInstance; }

	public static void main(String[] args) {
		gameInstance = new MagicFighters();
		gameInstance.start();
	}

	private Fighter player;
	private Stage currentStage;

	public Fighter getPlayer() { return player; }

	private MagicFighters() {
		// Show intro information.
		System.out.println("+------------------+");
		System.out.println("|  Magic Fighters  |");
		System.out.format("|   Version %.2f   |\n", versionNumber);
		System.out.println("|        by        |");
		System.out.println("| Kurck  Gustavson |");
		System.out.println("+------------------+");

		player = new Fighter();
		currentStage = new MainMenu();
	}

	@Override
	public void run() {
		try (InputStreamReader input = new InputStreamReader(System.in);
				BufferedReader reader = new BufferedReader(input)) {
			while (currentStage != null) {
				String command = reader.readLine();
				if (command == null) {
					currentStage = null;
					break;
				}
				command = command.toUpperCase();
				currentStage = currentStage.updateStage(command);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
