package game;

public class Effect {

	public enum Part {
		RANDOM_NUMBER, SRC_LEVEL, TRG_LEVEL, SRC_HEALTH, TRG_HEALTH, SRC_ATTACK, TRG_ATTACK;
		float getEffect(Fighter source, Fighter target) {
			switch (this) {
			case RANDOM_NUMBER: return (float) Math.random();
			case SRC_LEVEL: return source.level;
			case TRG_LEVEL: return target.level;
			case SRC_HEALTH: return source.health;
			case TRG_HEALTH: return target.health;
			case SRC_ATTACK: return source.attack;
			case TRG_ATTACK: return target.attack;
			default: return 1f;
			}
		}
	}

	private Part affectedPart;
	private float scalar;
	private Part[] parts;
	private boolean applied;

	public Effect(Part appliesTo, float strength, Part... elements) {
		affectedPart = appliesTo;
		applied = false;
		scalar = strength;
		parts = elements == null? new Part[0] : elements;
	}

	public static Effect attack(float strength) {
		return new Effect(Effect.Part.SRC_ATTACK, strength);
	}

	public static Effect randomisedAttack(float strength) {
		return new Effect(Effect.Part.SRC_ATTACK, strength, Effect.Part.RANDOM_NUMBER);
	}

	public static Effect levelAttack(float strength) {
		return new Effect(Effect.Part.SRC_ATTACK, strength, Effect.Part.SRC_LEVEL);
	}

	public static Effect randomisedLevelAttack(float strength) {
		return new Effect(Effect.Part.SRC_ATTACK, strength, Effect.Part.SRC_LEVEL, Effect.Part.RANDOM_NUMBER);
	}

	public static Effect getHurt(float strength) {
		return new Effect(Effect.Part.SRC_HEALTH, -strength);
	}

	public static Effect targetHealthAttack(float strength) {
		return new Effect(Effect.Part.SRC_ATTACK, strength, Effect.Part.TRG_HEALTH);
	}

	public static Effect shield(float shieldStrength) {
		float partOfAttackRemains = 1f - shieldStrength;
		return new Effect(Effect.Part.TRG_ATTACK, partOfAttackRemains, Effect.Part.TRG_ATTACK);
	}

	public static Effect regenerateBy(float strength, Part part) {
		return new Effect(Effect.Part.SRC_HEALTH, strength, part);
	}

	public static Effect recoil(float strength) {
		return new Effect(Effect.Part.TRG_HEALTH, -strength, Effect.Part.TRG_ATTACK);
	}

	public static Effect getMimicAttack(float strength) {
		return new Effect(Effect.Part.SRC_ATTACK, strength, Effect.Part.TRG_ATTACK);
	}

	public boolean isAttack() {
		return affectedPart == Effect.Part.SRC_ATTACK;
	}

	public boolean isShield() {
		return affectedPart == Effect.Part.TRG_ATTACK;
	}

	public boolean hasBeenApplied() {
		return applied;
	}

	public void applyTo(Fighter source, Fighter target) {
		// Calculate the strength of the effect.
		float effectStrength = scalar;
		for (Part part : parts) {
			effectStrength *= part.getEffect(source, target);
		}

		// Apply the effect.
		switch (affectedPart) {
		case SRC_HEALTH: source.health += effectStrength;
		case TRG_HEALTH: target.health += effectStrength;
		case SRC_ATTACK: source.attack += effectStrength;
		case TRG_ATTACK: target.attack += effectStrength;
		}

		applied = true;
	}

}
