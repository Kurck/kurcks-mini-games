package game;

public abstract class Stage {

	protected void showMenu(String... menuItems) {
		System.out.println();
		for (String menuItem : menuItems) {
			System.out.println(menuItem);
		}
	}

	public abstract Stage updateStage(String command);

}
