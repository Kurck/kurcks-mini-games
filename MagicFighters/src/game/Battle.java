package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Battle extends Stage {

	public enum Mode {
		TRAINING("training", 1),
		TOURNAMENT("tournament", 8);

		String label;
		int numberOfMatches;
		Mode(String humanReadable, int matches) {
			label = humanReadable;
			numberOfMatches = matches;
		}
	}

	private static final Map<String, Weapon> PLAYER_WEAPONS;
	private static final Map<String, Weapon> PLAYER_SHIELDS;
	private static final List<Weapon> OPPONENT_WEAPONS;
	static {
		PLAYER_WEAPONS = new HashMap<>();
		PLAYER_WEAPONS.put("T", new Weapon("T-Witch Power", 25f, true, Effect.levelAttack(4f), Effect.getHurt(5f)));
		PLAYER_WEAPONS.put("H", new Weapon("θ-Beam", 7f, Effect.randomisedLevelAttack(2.5f)));
		PLAYER_WEAPONS.put("N", new Weapon("Night Blade", 0f, Effect.attack(10f)));
		PLAYER_WEAPONS.put("P", new Weapon("Power Gazz", 5f, Effect.randomisedLevelAttack(3f), Effect.getHurt(3f)));
		PLAYER_WEAPONS.put("L", new Weapon("Splitter", 25f, Effect.targetHealthAttack(0.25f), Effect.getHurt(2f)));
		PLAYER_WEAPONS.put("S", new Weapon("Shields", 0f));
		PLAYER_WEAPONS.put("Q", new Weapon("Quit", 0f));

		PLAYER_SHIELDS = new HashMap<>();
		PLAYER_SHIELDS.put("B", new Weapon("Basic Shield", 0f, Effect.shield(0.25f)));
		PLAYER_SHIELDS.put("A", new Weapon("Suit of Armour", 0f, Effect.shield(0.5f)));
		PLAYER_SHIELDS.put("X", new Weapon("RefleXion", 20f, true, Effect.getHurt(1f), Effect.recoil(1f), Effect.regenerateBy(1f, Effect.Part.TRG_ATTACK)));
		PLAYER_SHIELDS.put("U", new Weapon("Sunlight", 30f, Effect.regenerateBy(5f, Effect.Part.SRC_LEVEL)));
		PLAYER_SHIELDS.put("G", new Weapon("Grib", 0f, Effect.attack(1f), Effect.shield(1f)));
		PLAYER_SHIELDS.put("W", new Weapon("Weapons", 0f));
		PLAYER_SHIELDS.put("Q", new Weapon("Quit", 0f));

		OPPONENT_WEAPONS = new ArrayList<>();
		OPPONENT_WEAPONS.add(new Weapon("Thunder Absorb", 7f, Effect.randomisedLevelAttack(3f), Effect.regenerateBy(1f, Effect.Part.SRC_ATTACK)));
		OPPONENT_WEAPONS.add(new Weapon("HP-Bolt", 25f, true, new Effect(Effect.Part.SRC_ATTACK, 1f, Effect.Part.SRC_HEALTH, Effect.Part.TRG_LEVEL, Effect.Part.RANDOM_NUMBER)));
		OPPONENT_WEAPONS.add(new Weapon("Magic Blade", 0f, Effect.randomisedLevelAttack(1f)));
		OPPONENT_WEAPONS.add(new Weapon("Snail", 0f, Effect.randomisedAttack(1f)));
		OPPONENT_WEAPONS.add(new Weapon("Slug", 0f, Effect.randomisedAttack(2f)));
		OPPONENT_WEAPONS.add(new Weapon("Volcano", 10f, Effect.randomisedLevelAttack(2f)));
		OPPONENT_WEAPONS.add(new Weapon("Scythe", 6f, Effect.levelAttack(2f)));
		OPPONENT_WEAPONS.add(new Weapon("Reborn", 25f, true, Effect.regenerateBy(10f, Effect.Part.SRC_LEVEL)));
		OPPONENT_WEAPONS.add(new Weapon("Mastershield", 20f, true, Effect.regenerateBy(1f, Effect.Part.TRG_ATTACK), Effect.getMimicAttack(2f)));
		OPPONENT_WEAPONS.add(new Weapon("Tombstones", 0f, Effect.attack(1f), Effect.shield(1f)));
		OPPONENT_WEAPONS.add(new Weapon("Jump", 5f, Effect.levelAttack(4f)));
		OPPONENT_WEAPONS.add(new Weapon("Soulsuck", 15f, true, Effect.regenerateBy(1f, Effect.Part.SRC_ATTACK), Effect.levelAttack(3f)));
		OPPONENT_WEAPONS.add(new Weapon("Suit of Armour", 0f, Effect.shield(0.5f)));
		OPPONENT_WEAPONS.add(new Weapon("Hassock", 0f, Effect.shield(1f)));
		OPPONENT_WEAPONS.add(new Weapon("Crap Attack", 5f, true, Effect.shield(0.25f), Effect.levelAttack(5f)));
		OPPONENT_WEAPONS.add(new Weapon("Golden Blades", 0f, Effect.shield(0.75f)));
		OPPONENT_WEAPONS.add(new Weapon("Darts", 0f, Effect.attack(3f)));
		OPPONENT_WEAPONS.add(new Weapon("ToddlersAttack", 0f, Effect.attack(2.5f)));
		OPPONENT_WEAPONS.add(new Weapon("Hurricane", 10f, Effect.attack(50f)));
		OPPONENT_WEAPONS.add(new Weapon("Sword", 0f, Effect.attack(5f)));
		OPPONENT_WEAPONS.add(new Weapon("Hysteric", 20f, true, Effect.shield(0.25f), new Effect(Effect.Part.SRC_ATTACK, 2f, Effect.Part.TRG_HEALTH, Effect.Part.RANDOM_NUMBER)));
	}

	private Mode mode;
	private Fighter player;
	private Fighter opponent;
	private int matchCount = 0;
	private boolean waitForRetryInput = false;

	public Battle(Mode battleType) {
		mode = battleType;
		System.out.format("Starting %s!\n", mode.label);
		player = MagicFighters.getInstance().getPlayer();
		initiateBattle();
	}

	private void initiateBattle() {
		player.prepareForFight();
		float opponentStrength;
		if (mode == Mode.TOURNAMENT) {
			opponentStrength = (float) matchCount / (float) mode.numberOfMatches;
		} else {
			opponentStrength = (float) (0.6 * Math.random());
		}
		opponent = player.getOpponent(opponentStrength);

		matchCount++;
		if (mode.numberOfMatches > 0) {
			System.out.format("\nStarting battle %d/%d\n", matchCount, mode.numberOfMatches);
		}
		System.out.format("You are at level %.0f\nand have %.0f HP.\n", player.level, player.health);
		System.out.format("Your enemy is at %.0f\nand has %.0f HP.\n", opponent.level, opponent.health);

		if (mode == Mode.TRAINING) {
			System.out.println(
					  "Each  round  of  the\n"
					+ "battle,  both  sides\n"
					+ "choose   an  action.\n"
					+ "Either  a shield  or\n"
					+ "an attack.  Each has\n"
					+ "unique  effects  and\n"
					+ "influences   on  the\n"
					+ "action of choice  of\n"
					+ "the opponent.");
		}
		showWeaponsMenu();
		if (mode == Mode.TRAINING) {
			System.out.println(
					  "Select a weapon (N)\n"
					+ "or switch to the\n"
					+ "shields menu (S).");
		}
	}

	private void showWeaponsMenu() {
		System.out.println("\n====={ Weapons }====");
		showAvailableItems(PLAYER_WEAPONS);
	}

	@Override
	public Stage updateStage(String command) {
		Stage nextStage = this;
		if (waitForRetryInput) {
			nextStage = processRetryInput(command, nextStage);
		} else if (command.equals("Q")) {
			System.out.println("You run away. Coward!");
			nextStage = new MainMenu();
		} else if (command.equals("S")) {
			System.out.println("\n====={ Shields }====");
			showAvailableItems(PLAYER_SHIELDS);
		} else if (command.equals("W")) {
			showWeaponsMenu();
		} else {
			// Select and perform actions.
			Weapon sourceAttack = PLAYER_WEAPONS.get(command);
			if (sourceAttack == null) {
				sourceAttack = PLAYER_SHIELDS.get(command);
			}
			if (sourceAttack == null || sourceAttack.minLevel > player.level) {
				System.out.format("Your entry '%s'\nwas  not recognised.\nPlease, try again.\n", command);
				if (mode == Mode.TRAINING) {
					System.out.println("Only   the   letters\nshown on the left of\nthe menu are  avail-\nable.");
				}
			} else {
				Weapon counterAttack = getRandomAvailableItem(opponent, OPPONENT_WEAPONS);
				attack(sourceAttack, counterAttack);
			}
			// Check whether everyone is able to battle another round.
			if (player.isDead()) {
				showMenu("===={ You lost! }===", "A) Try again", "Q) Quit");
				waitForRetryInput = true;
			} else if (opponent.isDead()) {
				player.addWin(opponent);
				if (mode == Mode.TOURNAMENT && matchCount == mode.numberOfMatches) {
					System.out.format("\nCongratulations, you\nwon the %s!\n", mode.label);
				} else {
					System.out.println("\nCongratulations,\nyou won the battle!");
				}
				if (mode == Mode.TRAINING) {
					System.out.println(
							"\nEach  time  you win,\n"
							+ "your level is raised\n"
							+ "a little. Next match\n"
							+ "will  continue  with\n"
							+ "your   higher  level\n"
							+ "which gives you more\n"
							+ "weapons, shields and\n"
							+ "health.");
				}

				// Next step depends on battle mode.
				if (mode == Mode.TOURNAMENT && matchCount < mode.numberOfMatches) {
					initiateBattle();
				} else {
					System.out.println("\nReturning  to   main\nmenu...");
					nextStage = new MainMenu();
				}
			} else {
				showWeaponsMenu();
			}
		}
		return nextStage;
	}

	private Stage processRetryInput(String command, Stage nextStage) {
		if (command.equals("A")) {
			waitForRetryInput = false;
			nextStage = new Battle(mode);
		} else if (command.equals("Q")) {
			waitForRetryInput = false;
			System.out.println(
					  "Returning   to  main\nmenu...");
			nextStage = new MainMenu();
		} else {
			System.out.format("Your entry '%s'\nwas  not recognised.\nPlease, try again.\n", command);
		}
		return nextStage;
	}

	private void attack(Weapon sourceAttack, Weapon targetAttack) {
		// Save the initial values of the attacks in the fighters.
		sourceAttack.calculateInitialStrength(player, opponent);
		targetAttack.calculateInitialStrength(opponent, player);
		// Some actions use up stamina.
		player.updateStamina(sourceAttack);
		opponent.updateStamina(targetAttack);
		// Calculate modifications to the attack strength; effects caused by the opponent. Note that the opponent goes first here.
		targetAttack.alterOpponentsAttack(opponent, player);
		sourceAttack.alterOpponentsAttack(player, opponent);
		// Handle all other effects.
		sourceAttack.applyOtherEffects(player, opponent);
		targetAttack.applyOtherEffects(player, opponent);
		// Execute attack.
		System.out.format("Your  opponent  uses\n%s.\n", targetAttack.name);
		if (opponent.attack >= 0.1f) {
			player.health -= opponent.attack;
			System.out.format("You  got  hit!   You\nhave %.1f HP left.\n", player.health);
		} else {
			System.out.println("He failed.");
		}
		System.out.format("You use\n%s.\n", sourceAttack.name);
		if (player.attack >= 0.1f) {
			opponent.health -= player.attack;
			System.out.format("You  hit  your enemy\nleaving   him   with\n%.1f HP.\n", opponent.health);
		} else {
			System.out.println("You failed.");
		}
	}

	private Weapon getRandomAvailableItem(Fighter user, Iterable<Weapon> items) {
		List<Weapon> availableItems = new ArrayList<Weapon>();
		for (Weapon item : items) {
			if (user.canUse(item)) {
				availableItems.add(item);
			}
		}

		int randomIndex = (int) (Math.random() * availableItems.size());
		Weapon randomAvailableItem = availableItems.get(randomIndex);
		return randomAvailableItem;
	}

	private void showAvailableItems(Map<String, Weapon> items) {
		for (String command : items.keySet()) {
			Weapon item = items.get(command);
			if (player.canUse(item)) {
				System.out.format("%s) %s\n", command, item.name);
			}
		}
	}

}
