package game;

public class OptionsMenu extends Stage {

	private static final String[] MENU_ITEMS = {
			"====={ Options }====",
			"L) Reset level",
			"H) Reset health",
			"N) Set nickname",
			"S) Show statistics",
			"C) Show credits",
			"B) Back"
		};
	private boolean waitForNickname = false;
	private Fighter player;

	public OptionsMenu() {
		player = MagicFighters.getInstance().getPlayer();
		showMenu(MENU_ITEMS);
	}

	@Override
	public Stage updateStage(String command) {
		Stage nextStage = this;
		if (waitForNickname) {
			waitForNickname = false;
			player.setName(command);
			System.out.println("Done.");
			showMenu(MENU_ITEMS);
		} else if (command.equals("L")) {
			float newLevel = player.resetLevel();
			System.out.format("Your level has been\nreset to %.0f.\n", newLevel);
			showMenu(MENU_ITEMS);
		} else if (command.equals("H")) {
			float newHealth = player.resetHealth();
			System.out.format("Your health has been\nreset to %.0f.\n", newHealth);
			showMenu(MENU_ITEMS);
		} else if (command.equals("N")) {
			System.out.println("Your new name:");
			waitForNickname = true;
		} else if (command.equals("S")) {
			System.out.println("==={ Statistics }===");
			System.out.format("Name: %s\n", player.name);
			System.out.format("Level: %.0f\n", player.level);
			System.out.format("Health: %.1f HP\n", player.health);
			System.out.format("Stamina: %d\n", player.stamina);
			showMenu(MENU_ITEMS);
		} else if (command.equals("C")) {
			System.out.println("+------------------+");
			System.out.println("|  Magic Fighters  |");
			System.out.format("|   Version %.2f   |\n", MagicFighters.versionNumber);
			System.out.println("|        by        |");
			System.out.println("| Kurck  Gustavson |");
			System.out.println("|    Thanks  to    |");
			System.out.println("|    KN  and MW    |");
			System.out.println("+------------------+");
			showMenu(MENU_ITEMS);
		} else if (command.equals("B")) {
			nextStage = new MainMenu();
		}
		return nextStage;
	}

}
