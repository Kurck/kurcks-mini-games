package game;

public class MainMenu extends Stage {

	private static final String[] MENU_ITEMS = {
			"={ Magic Fighters }=",
			"S) Single match",
			"T) Tournament",
			"O) Options",
			"Q) Quit"
		};

	public MainMenu() {
		showMenu(MENU_ITEMS);
	}

	@Override
	public Stage updateStage(String command) {
		Stage nextStage = this;
		if (command.equals("S")) {
			nextStage = new Battle(Battle.Mode.TRAINING);
		} else if (command.equals("T")) {
			nextStage = new Battle(Battle.Mode.TOURNAMENT);
		} else if (command.equals("O")) {
			nextStage = new OptionsMenu();
		} else if (command.equals("Q")) {
			nextStage = null;
			System.out.println("Have fun!");
		} else {
			System.out.println("Your entry\n'" + command + "'\nwas not recognised.\nPlease try again.");
		}
		return nextStage;
	}

}
